﻿using LotControl.Application;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using System.Linq;

namespace RobotToTrayUnloader.Application
{
    public class MaterialTracking : SubscriptionBase
    {
        #region Events
        public event EventHandler<int> InfeedHasMaterialChanged;
        public event EventHandler<int> RobotHasMaterialChanged;

        public event EventHandler<HasTrayChangedDataModel> TrayIdChanged;
        public event EventHandler<TrayHasMaterialChangedDataModel> HasMaterialChanged;
        #endregion

        #region members
        MaterialTrackingApplication mMaterialTracking;
        LotControl.Application.LotControl mLotControl;

        private bool mInfeedHasMaterial;
        private bool mRobotHasMaterial;

        private int mInspectionProcessState;

        private int mInspection3DResult;

        private int mTray1ID;
        private int mTray2ID;
        private int mTray3ID;
        private int mTray4ID;
        private int mTray5ID;
        private int mTray6ID;

        private bool[] mTray1DevicePositionsHasMaterial = new bool[12];
        private bool[] mTray2DevicePositionsHasMaterial = new bool[12];
        private bool[] mTray3DevicePositionsHasMaterial = new bool[12];
        private bool[] mTray4DevicePositionsHasMaterial = new bool[12];
        private bool[] mTray5DevicePositionsHasMaterial = new bool[12];
        private bool[] mTray6DevicePositionsHasMaterial = new bool[12];
            
        private readonly TagModel mInfeedHasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.HasMaterial);
        private readonly TagModel mRobotHasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.RobotDevicePosition.HasMaterial);

        private readonly TagModel mInspectionProcessStateTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.ProcessedState);

        private readonly TagModel mInspection3DResultTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.InspectionResult);
              
        private readonly TagModel mTray1IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.ID);
        private readonly TagModel mTray2IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.ID);
        private readonly TagModel mTray3IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.ID);
        private readonly TagModel mTray4IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.ID);
        private readonly TagModel mTray5IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.ID);
        private readonly TagModel mTray6IDTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.ID);
           
        private readonly TagModel mTray1DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray1DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray1DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray1DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray1DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray1DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray1DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray1DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray1DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray1DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray1DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray1DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray1.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);

        private readonly TagModel mTray2DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray2DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray2DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray2DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray2DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray2DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray2DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray2DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray2DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray2DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray2DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray2DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray2.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);

        private readonly TagModel mTray3DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray3DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray3DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray3DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray3DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray3DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray3DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray3DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray3DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray3DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray3DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray3DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray3.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);

        private readonly TagModel mTray4DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray4DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray4DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray4DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray4DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray4DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray4DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray4DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray4DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray4DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray4DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray4DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray4.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);

        private readonly TagModel mTray5DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray5DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray5DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray5DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray5DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray5DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray5DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray5DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray5DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray5DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray5DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray5DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray5.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);

        private readonly TagModel mTray6DevicePosition1HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mTray6DevicePosition2HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mTray6DevicePosition3HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mTray6DevicePosition4HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation4.HasMaterial);
        private readonly TagModel mTray6DevicePosition5HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation5.HasMaterial);
        private readonly TagModel mTray6DevicePosition6HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation6.HasMaterial);
        private readonly TagModel mTray6DevicePosition7HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation7.HasMaterial);
        private readonly TagModel mTray6DevicePosition8HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation8.HasMaterial);
        private readonly TagModel mTray6DevicePosition9HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation9.HasMaterial);
        private readonly TagModel mTray6DevicePosition10HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation10.HasMaterial);
        private readonly TagModel mTray6DevicePosition11HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation11.HasMaterial);
        private readonly TagModel mTray6DevicePosition12HasMaterialTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.TrayLocation.Tray6.Tray.TrayDeviceLocations.DeviceLocation12.HasMaterial);
        #endregion

        #region Properties robot
        public bool InfeedHasMaterial
        {
            get => mInfeedHasMaterial;
            set
            {
                if (mInfeedHasMaterial != value)
                {
                    mInfeedHasMaterial = value;
                    InfeedHasMaterialChanged?.Invoke(this, GetInfeedPositionDeviceId());
                }
            }
        }


        public int InspectionProcessState
        {
            get => mInspectionProcessState;
            set
            {
                if (mInspectionProcessState != value)
                {
                    mInspectionProcessState = value;

                    if (value == (int)DeviceProcessState.InspectedFinal)
                    {
                        var id = GetInfeedPositionDeviceId();

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mInspectionProcessState);
                        }
                    }
                }
            }
        }

        public int Inspection3DResult
        {
            get => mInspection3DResult;
            set
            {
                if (mInspection3DResult != value)
                {
                    mInspection3DResult = value;

                    if (value != (int)ValidationResults.None)
                    {
                        var id = GetInfeedPositionDeviceId();


                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, DeviceProcessTypeResult.Inspection3DResult, (ValidationResults)mInspection3DResult);
                        }
                    }
                }
            }
        }


        public bool RobotHasMaterial
        {
            get => mRobotHasMaterial;
            set
            {
                if (mRobotHasMaterial != value)
                {
                    mRobotHasMaterial = value;
                    RobotHasMaterialChanged?.Invoke(this, GetRobotPositionDeviceId());
                }
            }
        }

        #endregion
        
        #region Properties tray1
        public int Tray1ID
        {
            get => mTray1ID;
            set
            {
                if (mTray1ID != value)
                {
                    mTray1ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray1, mTray1ID));
                }
            }
        }

        
        public bool Tray1DevicePosition1HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[0];
            set
            {
                if (mTray1DevicePositionsHasMaterial[0] != value)
                {                    
                    mTray1DevicePositionsHasMaterial[0] = value;                   
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 1, mTray1DevicePositionsHasMaterial[0], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition2HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[1];
            set
            {
                if (mTray1DevicePositionsHasMaterial[1] != value)
                {
                    mTray1DevicePositionsHasMaterial[1] = value;                    
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 2, mTray1DevicePositionsHasMaterial[1], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition3HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[2];
            set
            {
                if (mTray1DevicePositionsHasMaterial[2] != value)
                {
                    mTray1DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 3, mTray1DevicePositionsHasMaterial[2], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition4HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[3];
            set
            {
                if (mTray1DevicePositionsHasMaterial[3] != value)
                {
                    mTray1DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 4, mTray1DevicePositionsHasMaterial[3], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition5HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[4];
            set
            {
                if (mTray1DevicePositionsHasMaterial[4] != value)
                {
                    mTray1DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 5, mTray1DevicePositionsHasMaterial[4], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition6HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[5];
            set
            {
                if (mTray1DevicePositionsHasMaterial[5] != value)
                {
                    mTray1DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 6, mTray1DevicePositionsHasMaterial[5], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition7HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[6];
            set
            {
                if (mTray1DevicePositionsHasMaterial[6] != value)
                {
                    mTray1DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 7, mTray1DevicePositionsHasMaterial[6], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition8HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[7];
            set
            {
                if (mTray1DevicePositionsHasMaterial[7] != value)
                {
                    mTray1DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 8, mTray1DevicePositionsHasMaterial[7], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition9HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[8];
            set
            {
                if (mTray1DevicePositionsHasMaterial[8] != value)
                {
                    mTray1DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 9, mTray1DevicePositionsHasMaterial[8], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition10HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[9];
            set
            {
                if (mTray1DevicePositionsHasMaterial[9] != value)
                {
                    mTray1DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 10, mTray1DevicePositionsHasMaterial[9], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition11HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[10];
            set
            {
                if (mTray1DevicePositionsHasMaterial[10] != value)
                {
                    mTray1DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 11, mTray1DevicePositionsHasMaterial[10], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray1DevicePosition12HasMaterial
        {
            get => mTray1DevicePositionsHasMaterial[11];
            set
            {
                if (mTray1DevicePositionsHasMaterial[11] != value)
                {
                    mTray1DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray1, 12, mTray1DevicePositionsHasMaterial[11], GetDeviceCount(mTray1DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region Properties tray2
        public int Tray2ID
        {
            get => mTray2ID;
            set
            {
                if (mTray2ID != value)
                {
                    mTray2ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray2, mTray2ID));
                }
            }
        }

        public bool Tray2DevicePosition1HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[0];
            set
            {
                if (mTray2DevicePositionsHasMaterial[0] != value)
                {
                    mTray2DevicePositionsHasMaterial[0] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 1, mTray2DevicePositionsHasMaterial[0], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition2HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[1];
            set
            {
                if (mTray2DevicePositionsHasMaterial[1] != value)
                {
                    mTray2DevicePositionsHasMaterial[1] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 2, mTray2DevicePositionsHasMaterial[1], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition3HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[2];
            set
            {
                if (mTray2DevicePositionsHasMaterial[2] != value)
                {
                    mTray2DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 3, mTray2DevicePositionsHasMaterial[2], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition4HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[3];
            set
            {
                if (mTray2DevicePositionsHasMaterial[3] != value)
                {
                    mTray2DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 4, mTray2DevicePositionsHasMaterial[3], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition5HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[4];
            set
            {
                if (mTray2DevicePositionsHasMaterial[4] != value)
                {
                    mTray2DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 5, mTray2DevicePositionsHasMaterial[4], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition6HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[5];
            set
            {
                if (mTray2DevicePositionsHasMaterial[5] != value)
                {
                    mTray2DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 6, mTray2DevicePositionsHasMaterial[5], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition7HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[6];
            set
            {
                if (mTray2DevicePositionsHasMaterial[6] != value)
                {
                    mTray2DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 7, mTray2DevicePositionsHasMaterial[6], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition8HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[7];
            set
            {
                if (mTray2DevicePositionsHasMaterial[7] != value)
                {
                    mTray2DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 8, mTray2DevicePositionsHasMaterial[7], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition9HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[8];
            set
            {
                if (mTray2DevicePositionsHasMaterial[8] != value)
                {
                    mTray2DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 9, mTray2DevicePositionsHasMaterial[8], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition10HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[9];
            set
            {
                if (mTray2DevicePositionsHasMaterial[9] != value)
                {
                    mTray2DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 10, mTray2DevicePositionsHasMaterial[9], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition11HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[10];
            set
            {
                if (mTray2DevicePositionsHasMaterial[10] != value)
                {
                    mTray2DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 11, mTray2DevicePositionsHasMaterial[10], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray2DevicePosition12HasMaterial
        {
            get => mTray2DevicePositionsHasMaterial[11];
            set
            {
                if (mTray2DevicePositionsHasMaterial[11] != value)
                {
                    mTray2DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray2, 12, mTray2DevicePositionsHasMaterial[11], GetDeviceCount(mTray2DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region  Properties tray3
        public int Tray3ID
        {
            get => mTray3ID;
            set
            {
                if (mTray3ID != value)
                {
                    mTray3ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray3, mTray3ID));
                }
            }
        }

        public bool Tray3DevicePosition1HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[0];
            set
            {
                if (mTray3DevicePositionsHasMaterial[0] != value)
                {
                    mTray3DevicePositionsHasMaterial[0] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 1, mTray3DevicePositionsHasMaterial[0], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition2HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[1];
            set
            {
                if (mTray3DevicePositionsHasMaterial[1] != value)
                {
                    mTray3DevicePositionsHasMaterial[1] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 2, mTray3DevicePositionsHasMaterial[1], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition3HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[2];
            set
            {
                if (mTray3DevicePositionsHasMaterial[2] != value)
                {
                    mTray3DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 3, mTray3DevicePositionsHasMaterial[2], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition4HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[3];
            set
            {
                if (mTray3DevicePositionsHasMaterial[3] != value)
                {
                    mTray3DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 4, mTray3DevicePositionsHasMaterial[3], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition5HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[4];
            set
            {
                if (mTray3DevicePositionsHasMaterial[4] != value)
                {
                    mTray3DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 5, mTray3DevicePositionsHasMaterial[4], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition6HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[5];
            set
            {
                if (mTray3DevicePositionsHasMaterial[5] != value)
                {
                    mTray3DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 6, mTray3DevicePositionsHasMaterial[5], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition7HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[6];
            set
            {
                if (mTray3DevicePositionsHasMaterial[6] != value)
                {
                    mTray3DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 7, mTray3DevicePositionsHasMaterial[6], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition8HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[7];
            set
            {
                if (mTray3DevicePositionsHasMaterial[7] != value)
                {
                    mTray3DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 8, mTray3DevicePositionsHasMaterial[7], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition9HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[8];
            set
            {
                if (mTray3DevicePositionsHasMaterial[8] != value)
                {
                    mTray3DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 9, mTray3DevicePositionsHasMaterial[8], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition10HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[9];
            set
            {
                if (mTray3DevicePositionsHasMaterial[9] != value)
                {
                    mTray3DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 10, mTray3DevicePositionsHasMaterial[9], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition11HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[10];
            set
            {
                if (mTray3DevicePositionsHasMaterial[10] != value)
                {
                    mTray3DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 11, mTray3DevicePositionsHasMaterial[10], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray3DevicePosition12HasMaterial
        {
            get => mTray3DevicePositionsHasMaterial[11];
            set
            {
                if (mTray3DevicePositionsHasMaterial[11] != value)
                {
                    mTray3DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray3, 12, mTray3DevicePositionsHasMaterial[11], GetDeviceCount(mTray3DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region Properties tray4
        public int Tray4ID
        {
            get => mTray4ID;
            set
            {
                if (mTray4ID != value)
                {
                    mTray4ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray4, mTray4ID));
                }
            }
        }

        public bool Tray4DevicePosition1HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[0];
            set
            {
                if (mTray4DevicePositionsHasMaterial[0] != value)
                {
                    mTray4DevicePositionsHasMaterial[0] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 1, mTray4DevicePositionsHasMaterial[0], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition2HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[1];
            set
            {
                if (mTray4DevicePositionsHasMaterial[1] != value)
                {
                    mTray4DevicePositionsHasMaterial[1] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 2, mTray4DevicePositionsHasMaterial[1], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition3HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[2];
            set
            {
                if (mTray4DevicePositionsHasMaterial[2] != value)
                {
                    mTray4DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 3, mTray4DevicePositionsHasMaterial[2], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition4HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[3];
            set
            {
                if (mTray4DevicePositionsHasMaterial[3] != value)
                {
                    mTray4DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 4, mTray4DevicePositionsHasMaterial[3], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition5HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[4];
            set
            {
                if (mTray4DevicePositionsHasMaterial[4] != value)
                {
                    mTray4DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 5, mTray4DevicePositionsHasMaterial[4], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition6HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[5];
            set
            {
                if (mTray4DevicePositionsHasMaterial[5] != value)
                {
                    mTray4DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 6, mTray4DevicePositionsHasMaterial[5], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition7HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[6];
            set
            {
                if (mTray4DevicePositionsHasMaterial[6] != value)
                {
                    mTray4DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 7, mTray4DevicePositionsHasMaterial[6], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition8HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[7];
            set
            {
                if (mTray4DevicePositionsHasMaterial[7] != value)
                {
                    mTray4DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 8, mTray4DevicePositionsHasMaterial[7], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition9HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[8];
            set
            {
                if (mTray4DevicePositionsHasMaterial[8] != value)
                {
                    mTray4DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 9, mTray4DevicePositionsHasMaterial[8], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition10HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[9];
            set
            {
                if (mTray4DevicePositionsHasMaterial[9] != value)
                {
                    mTray4DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 10, mTray4DevicePositionsHasMaterial[9], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition11HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[10];
            set
            {
                if (mTray4DevicePositionsHasMaterial[10] != value)
                {
                    mTray4DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 11, mTray4DevicePositionsHasMaterial[10], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray4DevicePosition12HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[11];
            set
            {
                if (mTray4DevicePositionsHasMaterial[11] != value)
                {
                    mTray4DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray4, 12, mTray4DevicePositionsHasMaterial[11], GetDeviceCount(mTray4DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region Properties tray5
        public int Tray5ID
        {
            get => mTray5ID;
            set
            {
                if (mTray5ID != value)
                {
                    mTray5ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray5, mTray5ID));
                }
            }
        }

        public bool Tray5DevicePosition1HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[0];
            set
            {
                if (mTray4DevicePositionsHasMaterial[0] != value)
                {
                    mTray4DevicePositionsHasMaterial[0] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 1, mTray4DevicePositionsHasMaterial[0], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition2HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[1];
            set
            {
                if (mTray4DevicePositionsHasMaterial[1] != value)
                {
                    mTray4DevicePositionsHasMaterial[1] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 2, mTray4DevicePositionsHasMaterial[1], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition3HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[2];
            set
            {
                if (mTray4DevicePositionsHasMaterial[2] != value)
                {
                    mTray4DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 3, mTray4DevicePositionsHasMaterial[2], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition4HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[3];
            set
            {
                if (mTray4DevicePositionsHasMaterial[3] != value)
                {
                    mTray4DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 4, mTray4DevicePositionsHasMaterial[3], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition5HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[4];
            set
            {
                if (mTray4DevicePositionsHasMaterial[4] != value)
                {
                    mTray4DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 5, mTray4DevicePositionsHasMaterial[4], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition6HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[5];
            set
            {
                if (mTray4DevicePositionsHasMaterial[5] != value)
                {
                    mTray4DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 6, mTray4DevicePositionsHasMaterial[5], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition7HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[6];
            set
            {
                if (mTray4DevicePositionsHasMaterial[6] != value)
                {
                    mTray4DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 7, mTray4DevicePositionsHasMaterial[6], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition8HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[7];
            set
            {
                if (mTray4DevicePositionsHasMaterial[7] != value)
                {
                    mTray4DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 8, mTray4DevicePositionsHasMaterial[7], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition9HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[8];
            set
            {
                if (mTray4DevicePositionsHasMaterial[8] != value)
                {
                    mTray4DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 9, mTray4DevicePositionsHasMaterial[8], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition10HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[9];
            set
            {
                if (mTray4DevicePositionsHasMaterial[9] != value)
                {
                    mTray4DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 10, mTray4DevicePositionsHasMaterial[9], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition11HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[10];
            set
            {
                if (mTray4DevicePositionsHasMaterial[10] != value)
                {
                    mTray4DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 11, mTray4DevicePositionsHasMaterial[10], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray5DevicePosition12HasMaterial
        {
            get => mTray4DevicePositionsHasMaterial[11];
            set
            {
                if (mTray4DevicePositionsHasMaterial[11] != value)
                {
                    mTray4DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray5, 12, mTray4DevicePositionsHasMaterial[11], GetDeviceCount(mTray5DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region Properties tray6
        public int Tray6ID
        {
            get => mTray6ID;
            set
            {
                if (mTray6ID != value)
                {
                    mTray6ID = value;
                    TrayIdChanged?.Invoke(this, new HasTrayChangedDataModel(TrayPosition.Tray6, mTray6ID));
                }
            }
        }

        public bool Tray6DevicePosition1HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[0];
            set
            {
                if (mTray6DevicePositionsHasMaterial[0] != value)
                {
                    mTray6DevicePositionsHasMaterial[0] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 1, mTray6DevicePositionsHasMaterial[0], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition2HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[1];
            set
            {
                if (mTray6DevicePositionsHasMaterial[1] != value)
                {
                    mTray6DevicePositionsHasMaterial[1] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 2, mTray6DevicePositionsHasMaterial[1], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition3HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[2];
            set
            {
                if (mTray6DevicePositionsHasMaterial[2] != value)
                {
                    mTray6DevicePositionsHasMaterial[2] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 3, mTray6DevicePositionsHasMaterial[2], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition4HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[3];
            set
            {
                if (mTray6DevicePositionsHasMaterial[3] != value)
                {
                    mTray6DevicePositionsHasMaterial[3] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 4, mTray6DevicePositionsHasMaterial[3], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition5HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[4];
            set
            {
                if (mTray6DevicePositionsHasMaterial[4] != value)
                {
                    mTray6DevicePositionsHasMaterial[4] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 5, mTray6DevicePositionsHasMaterial[4], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition6HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[5];
            set
            {
                if (mTray6DevicePositionsHasMaterial[5] != value)
                {
                    mTray6DevicePositionsHasMaterial[5] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 6, mTray6DevicePositionsHasMaterial[5], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition7HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[6];
            set
            {
                if (mTray6DevicePositionsHasMaterial[6] != value)
                {
                    mTray6DevicePositionsHasMaterial[6] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 7, mTray6DevicePositionsHasMaterial[6], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition8HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[7];
            set
            {
                if (mTray6DevicePositionsHasMaterial[7] != value)
                {
                    mTray6DevicePositionsHasMaterial[7] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 8, mTray6DevicePositionsHasMaterial[7], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition9HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[8];
            set
            {
                if (mTray6DevicePositionsHasMaterial[8] != value)
                {
                    mTray6DevicePositionsHasMaterial[8] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 9, mTray6DevicePositionsHasMaterial[8], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition10HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[9];
            set
            {
                if (mTray6DevicePositionsHasMaterial[9] != value)
                {
                    mTray6DevicePositionsHasMaterial[9] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 10, mTray6DevicePositionsHasMaterial[9], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition11HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[10];
            set
            {
                if (mTray6DevicePositionsHasMaterial[10] != value)
                {
                    mTray6DevicePositionsHasMaterial[10] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 11, mTray6DevicePositionsHasMaterial[10], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }

        public bool Tray6DevicePosition12HasMaterial
        {
            get => mTray6DevicePositionsHasMaterial[11];
            set
            {
                if (mTray6DevicePositionsHasMaterial[11] != value)
                {
                    mTray6DevicePositionsHasMaterial[11] = value;
                    HasMaterialChanged?.Invoke(this, new TrayHasMaterialChangedDataModel(TrayPosition.Tray6, 12, mTray6DevicePositionsHasMaterial[11], GetDeviceCount(mTray6DevicePositionsHasMaterial)));
                }
            }
        }
        #endregion

        #region Constructor
        public MaterialTracking()
        {           
            mMaterialTracking = MaterialTrackingApplication.Instance;
            mLotControl = LotControl.Application.LotControl.Instance;

            TagModelPathDictionary = new()
            {
                { mInfeedHasMaterialTagModel, nameof(InfeedHasMaterial) },
                { mRobotHasMaterialTagModel, nameof(RobotHasMaterial) },
                { mInspectionProcessStateTagModel, nameof(InspectionProcessState) },
                { mInspection3DResultTagModel, nameof(Inspection3DResult) },
                { mTray1IDTagModel, nameof(Tray1ID) },
                { mTray2IDTagModel, nameof(Tray2ID) },
                { mTray3IDTagModel, nameof(Tray3ID) },
                { mTray4IDTagModel, nameof(Tray4ID) },
                { mTray5IDTagModel, nameof(Tray5ID) },
                { mTray6IDTagModel, nameof(Tray6ID) },
                { mTray1DevicePosition1HasMaterialTagModel, nameof(Tray1DevicePosition1HasMaterial) },
                { mTray1DevicePosition2HasMaterialTagModel, nameof(Tray1DevicePosition2HasMaterial) },
                { mTray1DevicePosition3HasMaterialTagModel, nameof(Tray1DevicePosition3HasMaterial) },
                { mTray1DevicePosition4HasMaterialTagModel, nameof(Tray1DevicePosition4HasMaterial) },
                { mTray1DevicePosition5HasMaterialTagModel, nameof(Tray1DevicePosition5HasMaterial) },
                { mTray1DevicePosition6HasMaterialTagModel, nameof(Tray1DevicePosition6HasMaterial) },
                { mTray1DevicePosition7HasMaterialTagModel, nameof(Tray1DevicePosition7HasMaterial) },
                { mTray1DevicePosition8HasMaterialTagModel, nameof(Tray1DevicePosition8HasMaterial) },
                { mTray1DevicePosition9HasMaterialTagModel, nameof(Tray1DevicePosition9HasMaterial) },
                { mTray1DevicePosition10HasMaterialTagModel, nameof(Tray1DevicePosition10HasMaterial) },
                { mTray1DevicePosition11HasMaterialTagModel, nameof(Tray1DevicePosition11HasMaterial) },
                { mTray1DevicePosition12HasMaterialTagModel, nameof(Tray1DevicePosition12HasMaterial) },
                { mTray2DevicePosition1HasMaterialTagModel, nameof(Tray2DevicePosition1HasMaterial) },
                { mTray2DevicePosition2HasMaterialTagModel, nameof(Tray2DevicePosition2HasMaterial) },
                { mTray2DevicePosition3HasMaterialTagModel, nameof(Tray2DevicePosition3HasMaterial) },
                { mTray2DevicePosition4HasMaterialTagModel, nameof(Tray2DevicePosition4HasMaterial) },
                { mTray2DevicePosition5HasMaterialTagModel, nameof(Tray2DevicePosition5HasMaterial) },
                { mTray2DevicePosition6HasMaterialTagModel, nameof(Tray2DevicePosition6HasMaterial) },
                { mTray2DevicePosition7HasMaterialTagModel, nameof(Tray2DevicePosition7HasMaterial) },
                { mTray2DevicePosition8HasMaterialTagModel, nameof(Tray2DevicePosition8HasMaterial) },
                { mTray2DevicePosition9HasMaterialTagModel, nameof(Tray2DevicePosition9HasMaterial) },
                { mTray2DevicePosition10HasMaterialTagModel, nameof(Tray2DevicePosition10HasMaterial) },
                { mTray2DevicePosition11HasMaterialTagModel, nameof(Tray2DevicePosition11HasMaterial) },
                { mTray2DevicePosition12HasMaterialTagModel, nameof(Tray2DevicePosition12HasMaterial) },
                { mTray3DevicePosition1HasMaterialTagModel, nameof(Tray3DevicePosition1HasMaterial) },
                { mTray3DevicePosition2HasMaterialTagModel, nameof(Tray3DevicePosition2HasMaterial) },
                { mTray3DevicePosition3HasMaterialTagModel, nameof(Tray3DevicePosition3HasMaterial) },
                { mTray3DevicePosition4HasMaterialTagModel, nameof(Tray3DevicePosition4HasMaterial) },
                { mTray3DevicePosition5HasMaterialTagModel, nameof(Tray3DevicePosition5HasMaterial) },
                { mTray3DevicePosition6HasMaterialTagModel, nameof(Tray3DevicePosition6HasMaterial) },
                { mTray3DevicePosition7HasMaterialTagModel, nameof(Tray3DevicePosition7HasMaterial) },
                { mTray3DevicePosition8HasMaterialTagModel, nameof(Tray3DevicePosition8HasMaterial) },
                { mTray3DevicePosition9HasMaterialTagModel, nameof(Tray3DevicePosition9HasMaterial) },
                { mTray3DevicePosition10HasMaterialTagModel, nameof(Tray3DevicePosition10HasMaterial) },
                { mTray3DevicePosition11HasMaterialTagModel, nameof(Tray3DevicePosition11HasMaterial) },
                { mTray3DevicePosition12HasMaterialTagModel, nameof(Tray3DevicePosition12HasMaterial) },
                { mTray4DevicePosition1HasMaterialTagModel, nameof(Tray4DevicePosition1HasMaterial) },
                { mTray4DevicePosition2HasMaterialTagModel, nameof(Tray4DevicePosition2HasMaterial) },
                { mTray4DevicePosition3HasMaterialTagModel, nameof(Tray4DevicePosition3HasMaterial) },
                { mTray4DevicePosition4HasMaterialTagModel, nameof(Tray4DevicePosition4HasMaterial) },
                { mTray4DevicePosition5HasMaterialTagModel, nameof(Tray4DevicePosition5HasMaterial) },
                { mTray4DevicePosition6HasMaterialTagModel, nameof(Tray4DevicePosition6HasMaterial) },
                { mTray4DevicePosition7HasMaterialTagModel, nameof(Tray4DevicePosition7HasMaterial) },
                { mTray4DevicePosition8HasMaterialTagModel, nameof(Tray4DevicePosition8HasMaterial) },
                { mTray4DevicePosition9HasMaterialTagModel, nameof(Tray4DevicePosition9HasMaterial) },
                { mTray4DevicePosition10HasMaterialTagModel, nameof(Tray4DevicePosition10HasMaterial) },
                { mTray4DevicePosition11HasMaterialTagModel, nameof(Tray4DevicePosition11HasMaterial) },
                { mTray4DevicePosition12HasMaterialTagModel, nameof(Tray4DevicePosition12HasMaterial) },
                { mTray5DevicePosition1HasMaterialTagModel, nameof(Tray5DevicePosition1HasMaterial) },
                { mTray5DevicePosition2HasMaterialTagModel, nameof(Tray5DevicePosition2HasMaterial) },
                { mTray5DevicePosition3HasMaterialTagModel, nameof(Tray5DevicePosition3HasMaterial) },
                { mTray5DevicePosition4HasMaterialTagModel, nameof(Tray5DevicePosition4HasMaterial) },
                { mTray5DevicePosition5HasMaterialTagModel, nameof(Tray5DevicePosition5HasMaterial) },
                { mTray5DevicePosition6HasMaterialTagModel, nameof(Tray5DevicePosition6HasMaterial) },
                { mTray5DevicePosition7HasMaterialTagModel, nameof(Tray5DevicePosition7HasMaterial) },
                { mTray5DevicePosition8HasMaterialTagModel, nameof(Tray5DevicePosition8HasMaterial) },
                { mTray5DevicePosition9HasMaterialTagModel, nameof(Tray5DevicePosition9HasMaterial) },
                { mTray5DevicePosition10HasMaterialTagModel, nameof(Tray5DevicePosition10HasMaterial) },
                { mTray5DevicePosition11HasMaterialTagModel, nameof(Tray5DevicePosition11HasMaterial) },
                { mTray5DevicePosition12HasMaterialTagModel, nameof(Tray5DevicePosition12HasMaterial) },
                { mTray6DevicePosition1HasMaterialTagModel, nameof(Tray6DevicePosition1HasMaterial) },
                { mTray6DevicePosition2HasMaterialTagModel, nameof(Tray6DevicePosition2HasMaterial) },
                { mTray6DevicePosition3HasMaterialTagModel, nameof(Tray6DevicePosition3HasMaterial) },
                { mTray6DevicePosition4HasMaterialTagModel, nameof(Tray6DevicePosition4HasMaterial) },
                { mTray6DevicePosition5HasMaterialTagModel, nameof(Tray6DevicePosition5HasMaterial) },
                { mTray6DevicePosition6HasMaterialTagModel, nameof(Tray6DevicePosition6HasMaterial) },
                { mTray6DevicePosition7HasMaterialTagModel, nameof(Tray6DevicePosition7HasMaterial) },
                { mTray6DevicePosition8HasMaterialTagModel, nameof(Tray6DevicePosition8HasMaterial) },
                { mTray6DevicePosition9HasMaterialTagModel, nameof(Tray6DevicePosition9HasMaterial) },
                { mTray6DevicePosition10HasMaterialTagModel, nameof(Tray6DevicePosition10HasMaterial) },
                { mTray6DevicePosition11HasMaterialTagModel, nameof(Tray6DevicePosition11HasMaterial) },
                { mTray6DevicePosition12HasMaterialTagModel, nameof(Tray6DevicePosition12HasMaterial) },
            };
        }
        #endregion

        public int GetInfeedPositionDeviceId()
        {
            return (int)mPlcService.ReadValue(RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.InfeedDevicePosition.Device.ID));
        }

        public int GetRobotPositionDeviceId()
        {
            return (int)mPlcService.ReadValue(RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderMaterialTracking.RobotDevicePosition.Device.ID));
        }

        private int GetDeviceCount(bool[] trayDevicePositionsHasMaterial)
        {
            return trayDevicePositionsHasMaterial.Where(item => item).Count();
        }
    }
}
