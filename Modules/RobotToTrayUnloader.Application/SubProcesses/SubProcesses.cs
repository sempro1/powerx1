﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class SubProcesses
    {
        public Inspection Inspection { get; } = new Inspection();
        public Robot Robot { get; } = new Robot();
    }
}
