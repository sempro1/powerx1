﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
   public class InspectionDataModel
    {
        public double InfeedPosition { get; set; }
        public double OutfeedPosition { get; set; }
        public double StartInspectionPosition { get; set; }
        public double Velocity { get; set; }
        public bool NoAOI { get; set; }

    }
}
