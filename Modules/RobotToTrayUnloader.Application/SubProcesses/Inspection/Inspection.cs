﻿using MachineManager;
using MachineManager.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class Inspection : SubscriptionBase
    {
        private readonly TagModel mStatusTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPc.Status);

        private readonly TagModel mInfeedPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPlc.Inspection.InfeedPosition);
        private readonly TagModel mOutfeedPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPlc.Inspection.OutfeedPosition);
        private readonly TagModel mStartInspectionPositionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPlc.Inspection.StartInspectionPosition);
        private readonly TagModel mVelocityTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPlc.Inspection.Velocity);
        private readonly TagModel mNoAOITagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.InspectionProcessPcInterface.ToPlc.Inspection.NoAoi);
        private InspectionDataModel mSettings;

        private int mStatus;
        public event EventHandler<InspectionDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;
        public InspectionDataModel Settings => mSettings;

        public int Status
        {
            get => mStatus;
            private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public double InfeedPosition
        {
            get => (float)mSettings.InfeedPosition;
            set
            {
                if (mSettings.InfeedPosition != value)
                {
                    mSettings.InfeedPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double OutfeedPosition
        {
            get => (float)mSettings.OutfeedPosition;
            set
            {
                if (mSettings.OutfeedPosition != value)
                {
                    mSettings.OutfeedPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StartInspectionPosition
        {
            get => (float)mSettings.StartInspectionPosition;
            set
            {
                if (mSettings.StartInspectionPosition != value)
                {
                    mSettings.StartInspectionPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Velocity
        {
            get => (float)mSettings.Velocity;
            set
            {
                if (mSettings.Velocity != value)
                {
                    mSettings.Velocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public bool NoAOI
        {
            get => mSettings.NoAOI;
            set
            {
                if (mSettings.NoAOI != value)
                {
                    mSettings.NoAOI = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }
        public async void UpdateSettings(InspectionDataModel settings)
        {
            await PlcService.Instance.WriteValueAsync(mInfeedPositionTagModel, (float)settings.InfeedPosition);
            await PlcService.Instance.WriteValueAsync(mOutfeedPositionTagModel, (float)settings.OutfeedPosition);
            await PlcService.Instance.WriteValueAsync(mStartInspectionPositionTagModel, (float)settings.StartInspectionPosition);
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, (float)settings.Velocity);
            await PlcService.Instance.WriteValueAsync(mNoAOITagModel, settings.NoAOI);
        }

        public Inspection()
        {
            mSettings = new InspectionDataModel();

            TagModelPathDictionary = new()
            {
                 { mStatusTagModel, nameof(Status)},
                 { mInfeedPositionTagModel, nameof(InfeedPosition) },
                 { mOutfeedPositionTagModel, nameof(OutfeedPosition) },
                 { mStartInspectionPositionTagModel, nameof(StartInspectionPosition) },
                 { mVelocityTagModel, nameof(Velocity) },
                 { mNoAOITagModel, nameof(NoAOI) }
            };
        }
    }
}
