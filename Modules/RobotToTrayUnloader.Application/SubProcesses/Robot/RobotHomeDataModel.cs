﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotHomeDataModel
    {
        public PositionDataModel HomePosition { get; set; }
        public RobotHomeDataModel()
        {
            HomePosition = new PositionDataModel();
        }
    }
   
}
