﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotDataModel
    {      
      
        public int BaseCount { get; set; }
        public int DefaultVelocityPercentage { get; set; }
        public int ProductPresentVacuumDelay { get; set; }
        public int ProductNotPresentVacuumDelay { get; set; }
        public int TrayType1 { get; set; }
        public int TrayType2 { get; set; }
        public int TrayType3 { get; set; }
        public int TrayType4 { get; set; }
        public int TrayType5 { get; set; }
        public int TrayType6 { get; set; }
               
    }
}
