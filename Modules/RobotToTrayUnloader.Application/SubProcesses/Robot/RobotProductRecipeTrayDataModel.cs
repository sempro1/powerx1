﻿namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotProductRecipeTrayDataModel
    {
        public double PocketOffsetX { get; set; }
        public double PocketOffsetY { get; set; }
        public double TrayHeight { get; set; }
        public double PocketDepth { get; set; }
        public double TrayToFirstPocketOffsetX { get; set; }
        public double TrayToFirstPocketOffsetY { get; set; }
        public double DeviceSizeZ { get; set; }
    }
}
