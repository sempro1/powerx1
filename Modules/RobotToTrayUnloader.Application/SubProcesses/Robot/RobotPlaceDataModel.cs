﻿
namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class RobotPlaceDataModel
    {
        public double PlaceOffsetZ { get; set; }
        public VelocityPercentageDataModel PlaceVelocityPercentage { get; set; }
        public RobotPlaceDataModel()
        {
            PlaceVelocityPercentage = new VelocityPercentageDataModel();
        }
    }   
}
