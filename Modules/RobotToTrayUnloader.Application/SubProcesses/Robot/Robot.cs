﻿using MachineManager;
using MachineManager.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace RobotToTrayUnloader.Application.SubProcesses
{
    public class Robot : SubscriptionBase
    {
        public event EventHandler<RobotDataModel> SettingsChanged;
        public event EventHandler<RobotPickDataModel> SettingsPickChanged;
        public event EventHandler<RobotPlaceDataModel> SettingsPlaceChanged;
        public event EventHandler<RobotHomeDataModel> SettingsHomeChanged;
        public event EventHandler<int> StatusChanged;

        private readonly TagModel mRobotStatusTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPc.Status);

        private readonly TagModel mPickPositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.Position.X);
        private readonly TagModel mPickPositionYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.Position.Y);
        private readonly TagModel mPickPositionZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.Position.Z);
        private readonly TagModel mPickPositionRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.Position.R);
        private readonly TagModel mPickOffsetZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.PickOffSetZ);
        private readonly TagModel mPickApproachVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.VelocityPercentage.ApproachVelocityPercentage);
        private readonly TagModel mPickClearVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Pick.VelocityPercentage.ClearVelocityPercentage);

        private readonly TagModel mPlacePocketOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.PocketOffSetX);
        private readonly TagModel mPlacePocketOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.PocketOffsetY);
        private readonly TagModel mPlaceTrayHeightTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.TrayHeight);
        private readonly TagModel mPlacePocketDepthTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.PocketDepth);
        private readonly TagModel mPlaceTrayToFirstPocketOffsetXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.TrayToFirstPocketOffsetX);
        private readonly TagModel mPlaceTrayToFirstPocketOffsetYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.TrayToFirstPocketOffsetY);
        private readonly TagModel mDeviceSizeZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.DeviceSizeZ);
        private readonly TagModel mPlaceOffsetZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.PlaceOffsetZ);
        private readonly TagModel mPlaceApproachVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.VelocityPercentage.ApproachVelocityPercentage);
        private readonly TagModel mPlaceClearVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Place.VelocityPercentage.ClearVelocityPercentage);

        private readonly TagModel mHomePositionXTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Home.Position.X);
        private readonly TagModel mHomePositionYTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Home.Position.Y);
        private readonly TagModel mHomePositionZTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Home.Position.Z);
        private readonly TagModel mHomePositionRTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.Home.Position.R);

        private readonly TagModel mBaseCountTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.BaseCount);
        private readonly TagModel mDefaultVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Robot.DefaultVelocityPercentage);

        private readonly TagModel mProductPresentVacuumDelayTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Vacuum.ProductPresentVacuumDelay);
        private readonly TagModel mProductNotPresentVacuumDelayTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Vacuum.ProductNotPresentVacuumDelay);

        private readonly TagModel mTrayType1TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType1);
        private readonly TagModel mTrayType2TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType2);
        private readonly TagModel mTrayType3TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType3);
        private readonly TagModel mTrayType4TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType4);
        private readonly TagModel mTrayType5TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType5);
        private readonly TagModel mTrayType6TagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotProcessPcInterface.ToPlc.RobotSettings.Trays.TrayType6);
        private RobotDataModel mSettings;
        private RobotPickDataModel mPickSettings;
        private RobotPlaceDataModel mPlaceSettings;
        private RobotHomeDataModel mHomeSettings;
        private RobotProductRecipeTrayDataModel mProductRecipeSettings;
      

        private int mRobotStatus;

        public RobotDataModel Settings => mSettings;
        public RobotPickDataModel SettingsPick => mPickSettings;
        public RobotPlaceDataModel SettingsPlace => mPlaceSettings;
        public RobotHomeDataModel SettingsHome => mHomeSettings;
        public RobotProductRecipeTrayDataModel settingsProductRecipe => mProductRecipeSettings;

        public int Status
        {
            get => mRobotStatus;
            private set
            {
                if (mRobotStatus != value)
                {
                    mRobotStatus = value;
                    StatusChanged?.Invoke(this, mRobotStatus);
                }
            }
        }

        public double PickPositionX
        {
            get => (float)mPickSettings.PickPosition.X;
            set
            {
                if (mPickSettings.PickPosition.X != value)
                {
                    mPickSettings.PickPosition.X = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionY
        {
            get => (float)mPickSettings.PickPosition.Y;
            set
            {
                if (mPickSettings.PickPosition.Y != value)
                {
                    mPickSettings.PickPosition.Y = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionZ
        {
            get => (float)mPickSettings.PickPosition.Z;
            set
            {
                if (mPickSettings.PickPosition.Z != value)
                {
                    mPickSettings.PickPosition.Z = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickPositionR
        {
            get => (float)mPickSettings.PickPosition.R;
            set
            {
                if (mPickSettings.PickPosition.R != value)
                {
                    mPickSettings.PickPosition.R = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public double PickOffsetZ
        {
            get => (float)mPickSettings.PickOffsetZ;
            set
            {
                if (mPickSettings.PickOffsetZ != value)
                {
                    mPickSettings.PickOffsetZ = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }

        public int PickApproachVelocityPercentage
        {
            get => mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage;
            set
            {
                if (mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage != value)
                {
                    mPickSettings.PickVelocityPercentage.ApproachVelocityPercentage = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }
        public int PickClearVelocityPercentage
        {
            get => mPickSettings.PickVelocityPercentage.ClearVelocityPercentage;
            set
            {
                if (mPickSettings.PickVelocityPercentage.ClearVelocityPercentage != value)
                {
                    mPickSettings.PickVelocityPercentage.ClearVelocityPercentage = value;
                    SettingsPickChanged?.Invoke(this, mPickSettings);
                }
            }
        }
        public double PlacePocketOffsetX
        {
            get => (float)mProductRecipeSettings.PocketOffsetX;
            set
            {
                if (mProductRecipeSettings.PocketOffsetX != value)
                {
                    mProductRecipeSettings.PocketOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlacePocketOffsetY
        {
            get => (float)mProductRecipeSettings.PocketOffsetY;
            set
            {
                if (mProductRecipeSettings.PocketOffsetY != value)
                {
                    mProductRecipeSettings.PocketOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double TrayHeight
        {
            get => (float)mProductRecipeSettings.TrayHeight;
            set
            {
                if (mProductRecipeSettings.TrayHeight != value)
                {
                    mProductRecipeSettings.TrayHeight = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PocketDepth
        {
            get => (float)mProductRecipeSettings.PocketDepth;
            set
            {
                if (mProductRecipeSettings.PocketDepth != value)
                {
                    mProductRecipeSettings.PocketDepth = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceTrayToFirstPocketOffsetX
        {
            get => (float)mProductRecipeSettings.TrayToFirstPocketOffsetX;
            set
            {
                if (mProductRecipeSettings.TrayToFirstPocketOffsetX != value)
                {
                    mProductRecipeSettings.TrayToFirstPocketOffsetX = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceTrayToFirstPocketOffsetY
        {
            get => (float)mProductRecipeSettings.TrayToFirstPocketOffsetY;
            set
            {
                if (mProductRecipeSettings.TrayToFirstPocketOffsetY != value)
                {
                    mProductRecipeSettings.TrayToFirstPocketOffsetY = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double DeviceSizeZ
        {
            get => (float)mProductRecipeSettings.DeviceSizeZ;
            set
            {
                if (mProductRecipeSettings.DeviceSizeZ != value)
                {
                    mProductRecipeSettings.DeviceSizeZ = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }
        public double PlaceOffsetZ
        {
            get => (float)mPlaceSettings.PlaceOffsetZ;
            set
            {
                if (mPlaceSettings.PlaceOffsetZ != value)
                {
                    mPlaceSettings.PlaceOffsetZ = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }

        public int PlaceApproachVelocityPercentage
        {
            get => mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage;
            set
            {
                if (mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage != value)
                {
                    mPlaceSettings.PlaceVelocityPercentage.ApproachVelocityPercentage = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }
        public int PlaceClearVelocityPercentage
        {
            get => mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage;
            set
            {
                if (mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage != value)
                {
                    mPlaceSettings.PlaceVelocityPercentage.ClearVelocityPercentage = value;
                    SettingsPlaceChanged?.Invoke(this, mPlaceSettings);
                }
            }
        }

        public double HomePositionX
        {
            get => (float)mHomeSettings.HomePosition.X;
            set
            {
                if (mHomeSettings.HomePosition.X != value)
                {
                    mHomeSettings.HomePosition.X = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionY
        {
            get => (float)mHomeSettings.HomePosition.Y;
            set
            {
                if (mHomeSettings.HomePosition.Y != value)
                {
                    mHomeSettings.HomePosition.Y = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionZ
        {
            get => (float)mHomeSettings.HomePosition.Z;
            set
            {
                if (mHomeSettings.HomePosition.Z != value)
                {
                    mHomeSettings.HomePosition.Z = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public double HomePositionR
        {
            get => (float)mHomeSettings.HomePosition.R;
            set
            {
                if (mHomeSettings.HomePosition.R != value)
                {
                    mHomeSettings.HomePosition.R = value;
                    SettingsHomeChanged?.Invoke(this, mHomeSettings);
                }
            }
        }

        public int BaseCount
        {
            get => (int)mSettings.BaseCount;
            set
            {
                if (mSettings.BaseCount != value)
                {
                    mSettings.BaseCount = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int DefaultVelocityPercentage
        {
            get => (int)mSettings.DefaultVelocityPercentage;
            set 
            {
                if (mSettings.DefaultVelocityPercentage != value)
                {
                    mSettings.DefaultVelocityPercentage = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int ProductPresentVacuumDelay
        {
            get => (int)mSettings.ProductPresentVacuumDelay;
            set
            {
                if (mSettings.ProductPresentVacuumDelay != value)
                {
                    mSettings.ProductPresentVacuumDelay = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int ProductNotPresentVacuumDelay
        {
            get => (int)mSettings.ProductNotPresentVacuumDelay;
            set
            {
                if (mSettings.ProductNotPresentVacuumDelay != value)
                {
                    mSettings.ProductNotPresentVacuumDelay = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType1
        {
            get => (int)mSettings.TrayType1;
            set
            {
                if (mSettings.TrayType1 != value)
                {
                    mSettings.TrayType1 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType2
        {
            get => (int)mSettings.TrayType2;
            set
            {
                if (mSettings.TrayType2 != value)
                {
                    mSettings.TrayType2 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType3
        {
            get => (int)mSettings.TrayType3;
            set
            {
                if (mSettings.TrayType3 != value)
                {
                    mSettings.TrayType3 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType4
        {
            get => (int)mSettings.TrayType4;
            set
            {
                if (mSettings.TrayType4 != value)
                {
                    mSettings.TrayType4 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType5
        {
            get => (int)mSettings.TrayType5;
            set
            {
                if (mSettings.TrayType5 != value)
                {
                    mSettings.TrayType5 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public int TrayType6
        {
            get => (int)mSettings.TrayType6;
            set
            {
                if (mSettings.TrayType6 != value)
                {
                    mSettings.TrayType6 = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public async void UpdateRobotPickSettings(RobotPickDataModel settings)
        {
            
            if (settings.PickPosition.X != PickPositionX)
            {
                await PlcService.Instance.WriteValueAsync(mPickPositionXTagModel, (float)settings.PickPosition.X);
            }
            if (settings.PickPosition.Y != PickPositionY)
            {
                await PlcService.Instance.WriteValueAsync(mPickPositionYTagModel, (float)settings.PickPosition.Y);
            }
            if (settings.PickPosition.Z != PickPositionZ)
            {
                await PlcService.Instance.WriteValueAsync(mPickPositionZTagModel, (float)settings.PickPosition.Z);
            }
            if (settings.PickPosition.R != PickPositionR)
            {
                await PlcService.Instance.WriteValueAsync(mPickPositionRTagModel, (float)settings.PickPosition.R);
            }
            if (settings.PickOffsetZ != PickOffsetZ)
            {
                await PlcService.Instance.WriteValueAsync(mPickOffsetZTagModel, (float)settings.PickOffsetZ);
            }
            if (settings.PickVelocityPercentage.ApproachVelocityPercentage != PickApproachVelocityPercentage)
            {
                await PlcService.Instance.WriteValueAsync(mPickApproachVelocityPercentageTagModel, (short)settings.PickVelocityPercentage.ApproachVelocityPercentage);
            }
            if (settings.PickVelocityPercentage.ClearVelocityPercentage != PickClearVelocityPercentage)
            {
                await PlcService.Instance.WriteValueAsync(mPickClearVelocityPercentageTagModel, (short)settings.PickVelocityPercentage.ClearVelocityPercentage);
            }
        }

        public async void UpdateRobotPlaceSettings(RobotPlaceDataModel settings)
        {
            if (settings.PlaceOffsetZ != PlaceOffsetZ)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceOffsetZTagModel, (float)settings.PlaceOffsetZ);
            }
            if (settings.PlaceVelocityPercentage.ApproachVelocityPercentage != PlaceApproachVelocityPercentage)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceApproachVelocityPercentageTagModel, (short)settings.PlaceVelocityPercentage.ApproachVelocityPercentage);
            }
            if (settings.PlaceVelocityPercentage.ClearVelocityPercentage != PlaceClearVelocityPercentage)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceClearVelocityPercentageTagModel, (short)settings.PlaceVelocityPercentage.ClearVelocityPercentage);
            }        
        }

        public async void UpdateRobotHomeSettings(RobotHomeDataModel settings)
        {
            if (settings.HomePosition.X != HomePositionX)
            {
                await PlcService.Instance.WriteValueAsync(mHomePositionXTagModel, (float)settings.HomePosition.X);
            }
            if (settings.HomePosition.Y != HomePositionY)
            {
                await PlcService.Instance.WriteValueAsync(mHomePositionYTagModel, (float)settings.HomePosition.Y);
            }
            if (settings.HomePosition.Z != HomePositionZ)
            {
                await PlcService.Instance.WriteValueAsync(mHomePositionZTagModel, (float)settings.HomePosition.Z);
            }
            if (settings.HomePosition.R != HomePositionZ)
            {
                await PlcService.Instance.WriteValueAsync(mHomePositionRTagModel, (float)settings.HomePosition.R);
            }
        }

        public async void UpdateRobotProductRecipeSettings(RobotProductRecipeTrayDataModel settings)
        {
            if (settings.PocketOffsetX != PlacePocketOffsetX)
            {
                await PlcService.Instance.WriteValueAsync(mPlacePocketOffsetXTagModel, (float)settings.PocketOffsetX);
            }
            if (settings.PocketOffsetY != PlacePocketOffsetY)
            {
                await PlcService.Instance.WriteValueAsync(mPlacePocketOffsetYTagModel, (float)settings.PocketOffsetY);
            }
            if (settings.TrayHeight != TrayHeight)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceTrayHeightTagModel, (float)settings.TrayHeight);
            }
            if (settings.PocketDepth != PocketDepth)
            {
                await PlcService.Instance.WriteValueAsync(mPlacePocketDepthTagModel, (float)settings.PocketDepth);
            }
            if (settings.TrayToFirstPocketOffsetX != PlaceTrayToFirstPocketOffsetX)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceTrayToFirstPocketOffsetXTagModel, (float)settings.TrayToFirstPocketOffsetX);
            }
            if (settings.TrayToFirstPocketOffsetY != PlaceTrayToFirstPocketOffsetY)
            {
                await PlcService.Instance.WriteValueAsync(mPlaceTrayToFirstPocketOffsetYTagModel, (float)settings.TrayToFirstPocketOffsetY);
            }
            if (settings.DeviceSizeZ != DeviceSizeZ)
            {
                await PlcService.Instance.WriteValueAsync(mDeviceSizeZTagModel, (float)settings.DeviceSizeZ);
            }
        }

        public async void UpdateSettings(RobotDataModel settings)
        {             
            if (settings.ProductPresentVacuumDelay != ProductPresentVacuumDelay)
            {
                await PlcService.Instance.WriteValueAsync(mProductPresentVacuumDelayTagModel, (short)settings.ProductPresentVacuumDelay);
            }
            if (settings.ProductNotPresentVacuumDelay != ProductNotPresentVacuumDelay)
            {
                await PlcService.Instance.WriteValueAsync(mProductNotPresentVacuumDelayTagModel, (short)settings.ProductNotPresentVacuumDelay);
            }
            if (settings.BaseCount != BaseCount)
            {
                await PlcService.Instance.WriteValueAsync(mBaseCountTagModel, (short)settings.BaseCount);
            }
            if (settings.DefaultVelocityPercentage != DefaultVelocityPercentage)
            {
                await PlcService.Instance.WriteValueAsync(mDefaultVelocityPercentageTagModel, (short)settings.DefaultVelocityPercentage);
            }
            if (settings.TrayType1 != TrayType1)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType1TagModel, (short)settings.TrayType1);
            }
            if (settings.TrayType2 != TrayType2)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType2TagModel, (short)settings.TrayType2);
            }
            if (settings.TrayType3 != TrayType3)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType3TagModel, (short)settings.TrayType3);
            }
            if (settings.TrayType4 != TrayType4)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType4TagModel, (short)settings.TrayType4);
            }
            if (settings.TrayType5 != TrayType5)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType5TagModel, (short)settings.TrayType5);
            }
            if (settings.TrayType6 != TrayType6)
            {
                await PlcService.Instance.WriteValueAsync(mTrayType6TagModel, (short)settings.TrayType6);
            }
        }
        public Robot()
        {
            mSettings = new RobotDataModel();
            mPickSettings = new RobotPickDataModel();
            mPlaceSettings = new RobotPlaceDataModel();
            mHomeSettings = new RobotHomeDataModel();
            mProductRecipeSettings = new RobotProductRecipeTrayDataModel();

             TagModelPathDictionary = new()
            {
                 { mRobotStatusTagModel, nameof(Status)},
                 { mPickPositionXTagModel, nameof(PickPositionX)},
                 { mPickPositionYTagModel, nameof(PickPositionY)},
                 { mPickPositionZTagModel, nameof(PickPositionZ)},
                 { mPickPositionRTagModel, nameof(PickPositionR)},
                 { mPickOffsetZTagModel, nameof(PickOffsetZ)},
                 { mPickApproachVelocityPercentageTagModel, nameof(PickApproachVelocityPercentage)},
                 { mPickClearVelocityPercentageTagModel, nameof(PickClearVelocityPercentage)},
                 { mPlaceOffsetZTagModel, nameof(PlaceOffsetZ)},
                 { mPlaceApproachVelocityPercentageTagModel, nameof(PlaceApproachVelocityPercentage)},
                 { mPlaceClearVelocityPercentageTagModel, nameof(PlaceClearVelocityPercentage)},
                 { mHomePositionXTagModel, nameof(HomePositionX)},
                 { mHomePositionYTagModel, nameof(HomePositionY)},
                 { mHomePositionZTagModel, nameof(HomePositionZ)},
                 { mHomePositionRTagModel, nameof(HomePositionR)},
                 { mBaseCountTagModel, nameof(BaseCount)},
                 { mDefaultVelocityPercentageTagModel, nameof(DefaultVelocityPercentage)},
                 { mProductPresentVacuumDelayTagModel, nameof(ProductPresentVacuumDelay)},
                 { mProductNotPresentVacuumDelayTagModel, nameof(ProductNotPresentVacuumDelay)},
                 { mTrayType1TagModel, nameof(TrayType1)},
                 { mTrayType2TagModel, nameof(TrayType2)},
                 { mTrayType3TagModel, nameof(TrayType3)},
                 { mTrayType4TagModel, nameof(TrayType4)},
                 { mTrayType5TagModel, nameof(TrayType5)},
                 { mTrayType6TagModel, nameof(TrayType6)},
            };
        }
    }
}
