﻿using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using System;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System.Collections.Generic;
using RobotTrayUnloader.Application;
using RecipeManager;
using X1.Recipe.Lib;
using RobotToTrayUnloader.Application.SubProcesses;

namespace RobotToTrayUnloader.Application
{
    public class RobotToTrayUnloader : SubscriptionBase
    {
        public event EventHandler<bool> HasErrorChanged;
        public event EventHandler<int> StateChanged;
        public event EventHandler<int> StatusChanged;
        public event EventHandler<bool> SimulationChanged;

        public event EventHandler<RobotToTrayUnloaderDataModel> SettingsChanged;

        private ILog mLog = LogClient.Get();
        private PlcService mPLCService = PlcService.Instance;

        private bool mHasError;
        private int mState;
        private int mStatus;

        private static RobotToTrayUnloader mRobotTrayUnloader;
        private bool mIsRunningProduction;
        private bool mInSimulation;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        private readonly TagModel mRobotTrayUnloaderStatusTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPc.Status);
        private readonly TagModel mRobotTrayUnloaderStateTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPc.State);
        private readonly TagModel mRobotTrayUnloaderIsRunningProductionTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPc.IsRunningProduction);
        private readonly TagModel mRobotTrayUnloaderHasErrorTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPc.HasError);
        private readonly TagModel mRobotTrayUnloaderInSimulationTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPc.InSimulation);

        private readonly TagModel mInitializeCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.InitializeCommand);
        private readonly TagModel mStartCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.StartCommand);
        private readonly TagModel mStopCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.StopCommand);
        private readonly TagModel mInspectCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.InspectCommand);
        private readonly TagModel mInspectToInfeedCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.InspectToInfeedCommand);
        private readonly TagModel mPickCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.PickCommand);
        private readonly TagModel mPlaceCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.PlaceCommand);
        private readonly TagModel mHomeCommandTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.HomeCommand);
        private readonly TagModel mEnableSimulationTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.EnableSimulation);
        private readonly TagModel mVelocityPercentageTagModel = RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotToTrayUnloaderPcInterface.ToPlc.VelocityPercentage);

        private RobotToTrayUnloaderDataModel mSettings;
        public static RobotToTrayUnloader Instance => mRobotTrayUnloader ??= new RobotToTrayUnloader();

        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();

        public MaterialTracking MaterialTracking { get; } = new MaterialTracking();

        public RobotToTrayUnloaderDataModel Settings
        {
            get => mSettings;
            set
            {
                mSettings = value;
                SettingsChanged?.Invoke(this, value);
            }
        }

        public bool IsRunningProduction
        {
            get => mIsRunningProduction;
            set
            {
                if (mIsRunningProduction != value)
                {
                    mIsRunningProduction = value;
                }
            }
        }

        public bool HasError
        {
            get => mHasError;
            set
            {
                if (mHasError != value)
                {
                    mHasError = value;
                    HasErrorChanged?.Invoke(this, mHasError);
                }
            }
        }
        public int State
        {
            get => mState;
            set
            {
                if (mState != value)
                {
                    mState = value;
                    StateChanged?.Invoke(this, mState);
                }
            }
        }

        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool InSimulation
        {
            get => mInSimulation;
            set
            {
                if (mInSimulation != value)
                {
                    mInSimulation = value;
                    SimulationChanged?.Invoke(this, mInSimulation);
                }
            }
        }

        public RobotToTrayUnloader()
        {
            Settings = new RobotToTrayUnloaderDataModel();
            Settings.DataModelChanged += SettingsChanged;

            mRecipeManager.ProductRecipeChanged += RecipeManager_ProductRecipeChanged;
            mRecipeManager.MachineRecipeLoaded += RecipeManager_MachineRecipeLoaded;

             TagModelPathDictionary = new()
            {
                {mRobotTrayUnloaderStatusTagModel, nameof(Status) },
                {mRobotTrayUnloaderStateTagModel, nameof(State) },
                {mRobotTrayUnloaderIsRunningProductionTagModel, nameof(IsRunningProduction) },
                {mRobotTrayUnloaderHasErrorTagModel, nameof(HasError) },
                {mRobotTrayUnloaderInSimulationTagModel, nameof(InSimulation) },
                {mVelocityPercentageTagModel, GetNameOf(() => Settings.VelocityPercentage)},
                {Resources.ScanningStage.StatusTagModel, GetNameOf (() => Resources.ScanningStage.Status) }
            };

            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Inspection), SubProcesses.Inspection.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Robot), SubProcesses.Robot.TagModelPathDictionary);        

            AddTagModelDictionaryOfProperty(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);

            AddSubscription();
            InitializeSettings();

            mPlcService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }

        private void InitializeSettings()
        {
            RecipeManager_ProductRecipeChanged(this, mRecipeManager.GetCurrentRecipe());
            RecipeManager_MachineRecipeLoaded(this, mRecipeManager.GetMachineRecipe());
        }

        private void RecipeManager_ProductRecipeChanged(object sender, ProductRecipe e)
        {
            if (mPlcService.ConnectionState != ConnectionState.Online)
                return;

            var inspectionModel = new InspectionDataModel();
            inspectionModel.InfeedPosition = e.Inspection.InfeedPosition;
            inspectionModel.OutfeedPosition = e.Inspection.OutfeedPosition;
            inspectionModel.StartInspectionPosition = e.Inspection.StartInspectionPosition;
            inspectionModel.Velocity = e.Inspection.Velocity;
            inspectionModel.NoAOI = e.Inspection.NoAOI;
            SubProcesses.Inspection.UpdateSettings(inspectionModel);

            var trayDataModel = new RobotProductRecipeTrayDataModel();
            trayDataModel.PocketOffsetX = e.Tray.PocketOffsetX;
            trayDataModel.PocketOffsetY = e.Tray.PocketOffsetY;
            trayDataModel.TrayHeight = e.Tray.TrayHeight;
            trayDataModel.PocketDepth = e.Tray.PocketDepth;
            trayDataModel.TrayToFirstPocketOffsetX = e.Tray.TrayToFirstPocketOffsetX;
            trayDataModel.TrayToFirstPocketOffsetY = e.Tray.TrayToFirstPocketOffsetY;
            trayDataModel.DeviceSizeZ = e.Device.DeviceSizeZ;
            SubProcesses.Robot.UpdateRobotProductRecipeSettings(trayDataModel);

            var settingsDataModel = new RobotDataModel();
            settingsDataModel.BaseCount = e.Robot.BaseCount;

            if (e.Robot.Locations.Count > 5)
            {
                settingsDataModel.TrayType1 = (int)e.Robot.Locations[0];
                settingsDataModel.TrayType2 = (int)e.Robot.Locations[1];
                settingsDataModel.TrayType3 = (int)e.Robot.Locations[2];
                settingsDataModel.TrayType4 = (int)e.Robot.Locations[3];
                settingsDataModel.TrayType5 = (int)e.Robot.Locations[4];
                settingsDataModel.TrayType6 = (int)e.Robot.Locations[5];
            }
            settingsDataModel.DefaultVelocityPercentage = e.Robot.DefaultVelocityPercentage;
            settingsDataModel.ProductNotPresentVacuumDelay = 25;
            settingsDataModel.ProductPresentVacuumDelay = 25;
            SubProcesses.Robot.UpdateSettings(settingsDataModel);

            var placeDataModel = new RobotPlaceDataModel();
            placeDataModel.PlaceOffsetZ = e.RobotProcess.Place.PlaceOffsetZ;
            placeDataModel.PlaceVelocityPercentage.ApproachVelocityPercentage = e.RobotProcess.Place.VelocityPercentage.ApproachVelocityPercentage;
            placeDataModel.PlaceVelocityPercentage.ClearVelocityPercentage = e.RobotProcess.Place.VelocityPercentage.ClearVelocityPercentage;
            SubProcesses.Robot.UpdateRobotPlaceSettings(placeDataModel);

            var pickDataModel = new RobotPickDataModel();
            pickDataModel.PickPosition.X = e.RobotProcess.Pick.Position.X;
            pickDataModel.PickPosition.Y = e.RobotProcess.Pick.Position.Y;
            pickDataModel.PickPosition.Z = e.RobotProcess.Pick.Position.Z;
            pickDataModel.PickPosition.R = e.RobotProcess.Pick.Position.R;
            pickDataModel.PickOffsetZ = e.RobotProcess.Pick.PickOffsetZ;
            pickDataModel.PickVelocityPercentage.ApproachVelocityPercentage = e.RobotProcess.Pick.VelocityPercentage.ApproachVelocityPercentage;
            pickDataModel.PickVelocityPercentage.ClearVelocityPercentage = e.RobotProcess.Pick.VelocityPercentage.ClearVelocityPercentage;

            SubProcesses.Robot.UpdateRobotPickSettings(pickDataModel);
        }

        private void RecipeManager_MachineRecipeLoaded(object sender, X1.Recipe.Lib.Machine.MachineRecipe e)
        {
            var homeDataModel = new RobotHomeDataModel();
            homeDataModel.HomePosition.X = e.RobotTrayUnloader.Home.Position.X;
            homeDataModel.HomePosition.Y = e.RobotTrayUnloader.Home.Position.Y;
            homeDataModel.HomePosition.Z = e.RobotTrayUnloader.Home.Position.Z;
            homeDataModel.HomePosition.R = e.RobotTrayUnloader.Home.Position.R;
            SubProcesses.Robot.UpdateRobotHomeSettings(homeDataModel);
        }


        public void Initialize()
        {
            mLog.Debug($"Initialize: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mInitializeCommandTagModel, true);
        }

        public void Start()
        {
            mLog.Debug($"Start: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mStartCommandTagModel, true);
        }

        public void Stop()
        {
            mLog.Debug($"Stop: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mStopCommandTagModel, true);
        }

        public void Inspect()
        {
            mLog.Debug($"Inspect: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mInspectCommandTagModel, true);
        }

        public void InspectToInfeed()
        {
            mLog.Debug($"InspectToInfeed: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mInspectToInfeedCommandTagModel, true);
        }

        public void Pick()
        {
            mLog.Debug($"Pick: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mPickCommandTagModel, true);
        }

        public void Place()
        {
            mLog.Debug($"Place: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mPlaceCommandTagModel, true);
        }

        public void Home()
        {
            mLog.Debug($"Home: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mHomeCommandTagModel, true);
        }

        public void EnableSimulation()
        {
            mLog.Debug($"ExecuteEnableSimulation: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mEnableSimulationTagModel, true);
        }
        public void DisableSimulation()
        {
            mLog.Debug($"ExecuteDisableSimulation: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mEnableSimulationTagModel, false);
        }
        public void SetVelocityPercentage(int percentage)
        {
            mLog.Debug($"VelocityPercentage: RobotToTrayUnlaoder");
            mPLCService.WriteValue(mVelocityPercentageTagModel, (short)percentage);
        }
    }
}
