﻿using MachineManager.Lib;
using RobotToTrayUnloader.PLCInterfaces.OpcUa;
using System;

namespace RobotToTrayUnloader.Application.Resources
{
    public class ScanningStage
    {
        private int mScanningStageStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => RobotToTrayUnloaderOpcUa.GetTagModel(x => x.RobotToTrayUnloader.RobotTrayUnloaderScanningStagePcInterface.ToPc.Status);

        public int Status
        {
            get => mScanningStageStatus;
            private set
            {
                if (mScanningStageStatus != value)
                {
                    mScanningStageStatus = value;
                    StatusChanged?.Invoke(this, mScanningStageStatus);
                }
            }
        }
    }
}
