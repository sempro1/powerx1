﻿using System;

namespace RobotTrayUnloader.Application
{
   public class RobotToTrayUnloaderDataModel
    { 
        private int mVelocityPercentage;

        public event EventHandler<RobotToTrayUnloaderDataModel> DataModelChanged;
        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                mVelocityPercentage = value;
                DataModelChanged?.Invoke(this, this);
            }
        }
    }
}
