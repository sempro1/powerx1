﻿using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
   public class Lift
    {
        private int mLiftStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLiftPcInterface.ToPc.Status);

        public int Status
        {
            get => mLiftStatus;
            private set
            {
                if (mLiftStatus != value)
                {
                    mLiftStatus = value;
                    StatusChanged?.Invoke(this, mLiftStatus);
                }
            }
        }
    }
}
