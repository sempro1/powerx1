﻿using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
    public class LeadFramePusher
    {
        private int mLeadFramePusherStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderLeadFramePusherPcInterface.ToPc.Status);
        public int Status
        {
            get => mLeadFramePusherStatus;
            private set
            {
                if (mLeadFramePusherStatus != value)
                {
                    mLeadFramePusherStatus = value;
                    StatusChanged?.Invoke(this, mLeadFramePusherStatus);
                }
            }
        }
    }
}
