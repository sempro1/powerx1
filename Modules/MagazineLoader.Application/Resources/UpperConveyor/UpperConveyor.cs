﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.Resources
{
   public class UpperConveyor
    {
        private int mUpperConveyorStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUpperConveyorPcInterface.ToPc.Status);
        public int Status
        {
            get => mUpperConveyorStatus;
            private set
            {
                if (mUpperConveyorStatus != value)
                {
                    mUpperConveyorStatus = value;
                    StatusChanged?.Invoke(this, mUpperConveyorStatus);
                }
            }
        }
    }
}
