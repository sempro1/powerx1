﻿namespace MagazineLoader.Application.Resources
{
    public class Resources
    {
        public UpperConveyor UpperConveyor { get; } = new UpperConveyor();
        public UpperHatch UpperHatch { get; } = new UpperHatch();
        public Lift Lift { get; } = new Lift();
        public LeadFramePusher LeadFramePusher { get; } = new LeadFramePusher();
        public LowerConveyor LowerConveyor { get; } = new LowerConveyor();
        public LowerHatch LowerHatch { get; } = new LowerHatch();
    }
}
