﻿using LotControl.Application;
using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using MaterialTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;

namespace MagazineLoader.Application
{
    public class MaterialTracking: SubscriptionBase
    {
        public event EventHandler<bool> UpperConveyorHasMaterialChanged;
        public event EventHandler<bool> LowerConveyorHasMaterialChanged;
        public event EventHandler<bool> MagazineLiftHasMaterialChanged;
        public event EventHandler<bool> OutfeedHasMaterialChanged;
        public event EventHandler<int> OutfeedDeviceIdChanged;
        public event EventHandler<bool> MagazineLiftHasSlotsChanged;

        private MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;

        private bool mUpperConveyorHasMaterial;
        private bool mLowerConveyorHasMaterial;
        private bool mMagazineLiftHasMaterial;
        private bool mOutfeedHasMaterial;
        private int mOutfeedDeviceId;

        private bool mMagazineSlot0HasMaterial;
        private bool mMagazineSlot1HasMaterial;
        private bool mMagazineSlot2HasMaterial;
        private bool mMagazineSlot3HasMaterial;
        private bool mMagazineSlot4HasMaterial;
        private bool mMagazineSlot5HasMaterial;
        private bool mMagazineSlot6HasMaterial;
        private bool mMagazineSlot7HasMaterial;
        private bool mMagazineSlot8HasMaterial;
        private bool mMagazineSlot9HasMaterial;
        private bool mMagazineSlot10HasMaterial;
        private bool mMagazineSlot11HasMaterial;


        private readonly TagModel mUpperConveyorHasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.UpperConveyorMagazinePosition.HasMaterial);
        private readonly TagModel mLowerConveyorHasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LowerConveyorMagazinePosition.HasMaterial);
        private readonly TagModel mMagazineLiftHasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.HasMaterial);
        private readonly TagModel mOutfeedHasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.OutfeedDevicePosition.HasMaterial);
        private readonly TagModel mOutfeedDeviceIdTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.OutfeedDevicePosition.Device.ID);

        private readonly TagModel mMagazineSlot0HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot0);
        private readonly TagModel mMagazineSlot1HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot1);
        private readonly TagModel mMagazineSlot2HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot2);
        private readonly TagModel mMagazineSlot3HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot3);
        private readonly TagModel mMagazineSlot4HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot4);
        private readonly TagModel mMagazineSlot5HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot5);
        private readonly TagModel mMagazineSlot6HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot6);
        private readonly TagModel mMagazineSlot7HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot7);
        private readonly TagModel mMagazineSlot8HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot8);
        private readonly TagModel mMagazineSlot9HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot9);
        private readonly TagModel mMagazineSlot10HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot10);
        private readonly TagModel mMagazineSlot11HasMaterialTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MaterialTracking.LiftMagazinePosition.Magazine.Slot.Slot11);

        public bool UpperConveyorHasMaterial
        {
            get => mUpperConveyorHasMaterial;
            set
            {
                if (mUpperConveyorHasMaterial != value)
                {
                    mUpperConveyorHasMaterial = value;
                    UpperConveyorHasMaterialChanged?.Invoke(this, mUpperConveyorHasMaterial);
                }
            }
        }

        public bool LowerConveyorHasMaterial
        {
            get => mLowerConveyorHasMaterial;
            set
            {
                if (mLowerConveyorHasMaterial != value)
                {
                    mLowerConveyorHasMaterial = value;
                    LowerConveyorHasMaterialChanged?.Invoke(this, mLowerConveyorHasMaterial);
                }
            }
        }

        public bool MagazineLiftHasMaterial
        {
            get => mMagazineLiftHasMaterial;
            set
            {
                if (mMagazineLiftHasMaterial != value)
                {
                    mMagazineLiftHasMaterial = value;
                    MagazineLiftHasMaterialChanged?.Invoke(this, mMagazineLiftHasMaterial);
                }
            }
        }

        public bool OutfeedHasMaterial
        {
            get => mOutfeedHasMaterial;
            set
            {
                if (mOutfeedHasMaterial != value)
                {
                    mOutfeedHasMaterial = value;
                    OutfeedHasMaterialChanged?.Invoke(this, mOutfeedHasMaterial);
                }
            }
        }

        public int OutfeedDeviceId
        {
            get => mOutfeedDeviceId;
            set
            {
                if (mOutfeedDeviceId != value)
                {
                    mOutfeedDeviceId = value;
                    OutfeedDeviceIdChanged?.Invoke(this, mOutfeedDeviceId);

                    if (mOutfeedDeviceId != 0)
                    {
                        
                        mMaterialTracking.NewDevice(mOutfeedDeviceId, mOutfeedDeviceId, mLotControl.CurrentLotId);
                    }
                }
            }
        }        

        public bool MagazineSlot0HasMaterial
        {
            get => mMagazineSlot0HasMaterial;
            set
            {
                if (mMagazineSlot0HasMaterial != value)
                {
                    mMagazineSlot0HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot0HasMaterial);
                }
            }
        }
        public bool MagazineSlot1HasMaterial
        {
            get => mMagazineSlot1HasMaterial;
            set
            {
                if (mMagazineSlot1HasMaterial != value)
                {
                    mMagazineSlot1HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot1HasMaterial);
                }
            }
        }
        public bool MagazineSlot2HasMaterial
        {
            get => mMagazineSlot2HasMaterial;
            set
            {
                if (mMagazineSlot2HasMaterial != value)
                {
                    mMagazineSlot2HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot2HasMaterial);
                }
            }
        }

        public bool MagazineSlot3HasMaterial
        {
            get => mMagazineSlot3HasMaterial;
            set
            {
                if (mMagazineSlot3HasMaterial != value)
                {
                    mMagazineSlot3HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot3HasMaterial);
                }
            }
        }

        public bool MagazineSlot4HasMaterial
        {
            get => mMagazineSlot4HasMaterial;
            set
            {
                if (mMagazineSlot4HasMaterial != value)
                {
                    mMagazineSlot4HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot4HasMaterial);
                }
            }
        }

        public bool MagazineSlot5HasMaterial
        {
            get => mMagazineSlot5HasMaterial;
            set
            {
                if (mMagazineSlot5HasMaterial != value)
                {
                    mMagazineSlot5HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot5HasMaterial);
                }
            }
        }

        public bool MagazineSlot6HasMaterial
        {
            get => mMagazineSlot6HasMaterial;
            set
            {
                if (mMagazineSlot6HasMaterial != value)
                {
                    mMagazineSlot6HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot6HasMaterial);
                }
            }
        }

        public bool MagazineSlot7HasMaterial
        {
            get => mMagazineSlot7HasMaterial;
            set
            {
                if (mMagazineSlot7HasMaterial != value)
                {
                    mMagazineSlot7HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot7HasMaterial);
                }
            }
        }

        public bool MagazineSlot8HasMaterial
        {
            get => mMagazineSlot8HasMaterial;
            set
            {
                if (mMagazineSlot8HasMaterial != value)
                {
                    mMagazineSlot8HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot8HasMaterial);
                }
            }
        }

        public bool MagazineSlot9HasMaterial
        {
            get => mMagazineSlot9HasMaterial;
            set
            {
                if (mMagazineSlot9HasMaterial != value)
                {
                    mMagazineSlot9HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot9HasMaterial);
                }
            }
        }

        public bool MagazineSlot10HasMaterial
        {
            get => mMagazineSlot10HasMaterial;
            set
            {
                if (mMagazineSlot10HasMaterial != value)
                {
                    mMagazineSlot10HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot10HasMaterial);
                }
            }
        }

        public bool MagazineSlot11HasMaterial
        {
            get => mMagazineSlot11HasMaterial;
            set
            {
                if (mMagazineSlot11HasMaterial != value)
                {
                    mMagazineSlot11HasMaterial = value;
                    MagazineLiftHasSlotsChanged?.Invoke(this, mMagazineSlot11HasMaterial);
                }
            }
        }

        public MaterialTracking()
        {
            TagModelPathDictionary = new()
            {
                { mUpperConveyorHasMaterialTagModel , nameof(UpperConveyorHasMaterial) },
                { mLowerConveyorHasMaterialTagModel , nameof(LowerConveyorHasMaterial) },
                { mMagazineLiftHasMaterialTagModel  , nameof(MagazineLiftHasMaterial) },
                { mOutfeedHasMaterialTagModel , nameof(OutfeedHasMaterial) },
                { mOutfeedDeviceIdTagModel, nameof(OutfeedDeviceId) },
                { mMagazineSlot0HasMaterialTagModel , nameof(MagazineSlot0HasMaterial) },
                { mMagazineSlot1HasMaterialTagModel , nameof(MagazineSlot1HasMaterial) },
                { mMagazineSlot2HasMaterialTagModel , nameof(MagazineSlot2HasMaterial) },
                { mMagazineSlot3HasMaterialTagModel , nameof(MagazineSlot3HasMaterial) },
                { mMagazineSlot4HasMaterialTagModel , nameof(MagazineSlot4HasMaterial) },
                { mMagazineSlot5HasMaterialTagModel  , nameof(MagazineSlot5HasMaterial) },
                { mMagazineSlot6HasMaterialTagModel  , nameof(MagazineSlot6HasMaterial) },
                { mMagazineSlot7HasMaterialTagModel  , nameof(MagazineSlot7HasMaterial) },
                { mMagazineSlot8HasMaterialTagModel  , nameof(MagazineSlot8HasMaterial) },
                { mMagazineSlot9HasMaterialTagModel  , nameof(MagazineSlot9HasMaterial) },
                { mMagazineSlot10HasMaterialTagModel  , nameof(MagazineSlot10HasMaterial) },
                { mMagazineSlot11HasMaterialTagModel  , nameof(MagazineSlot11HasMaterial) }
            };
        }
    }
}
