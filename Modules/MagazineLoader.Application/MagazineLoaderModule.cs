﻿using System;
using System.Collections.Generic;
using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using MagazineLoader.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using RecipeManager;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace MagazineLoader.Application
{
    public class MagazineLoaderModule : SubscriptionBase
    {
        public event EventHandler<bool> HasErrorChanged;
        public event EventHandler<int> StateChanged;
        public event EventHandler<int> StatusChanged;
        public event EventHandler<bool> SimulationChanged;

        public event EventHandler<MagazineLoaderDataModel> SettingsChanged;

        private ILog mLog = LogClient.Get();
        private PlcService mPLCService = PlcService.Instance;
        private EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;

        private bool mHasError;
        private int mState;
        private int mStatus;

        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        private static MagazineLoaderModule mMagazineLoader;
        private bool mIsRunningProduction;
        private bool mInSimulation;

        private readonly TagModel mMagazineLoaderStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPc.Status);
        private readonly TagModel mMagazineLoaderStateTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPc.State);
        private readonly TagModel mIsRunningProductionTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPc.IsRunningProduction);
        private readonly TagModel mInSimulationCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPc.InSimulation);

        private readonly TagModel mInitializeCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.InitializeCommand);
        private readonly TagModel mStartCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.StartCommand);
        private readonly TagModel mStopCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.StopCommand);
        private readonly TagModel mLoadMagazineCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.LoadMagazineCommand);
        private readonly TagModel mProvideMaterialCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.ProvideLeadFramesCommand);
        private readonly TagModel mUnloadMagazineCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.UnloadMagazineCommand);
        private readonly TagModel mVelocityPercentageTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.VelocityPercentage);
        private readonly TagModel mSimulationEnabledCommandTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderModulePcInterface.ToPlc.EnableSimulation);

        public static MagazineLoaderModule Instance => mMagazineLoader ??= new MagazineLoaderModule();

        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();
        public MaterialTracking MaterialTracking { get; } = new MaterialTracking();
        public MagazineLoaderDataModel Settings { get; }

        public EventHandler EventHandler { get; } = new();

        public bool IsRunningProduction
        {
            get => mIsRunningProduction;
            set
            {
                if (mIsRunningProduction != value)
                {
                    mIsRunningProduction = value;
                }
            }
        }

        public bool HasError
        {
            get => mHasError;
            set
            {
                if (mHasError != value)
                {
                    mHasError = value;
                    HasErrorChanged?.Invoke(this, mHasError);
                }
            }
        }
        public int State
        {
            get => mState;
            set
            {
                if (mState != value)
                {
                    mState = value;
                    StateChanged?.Invoke(this, mState);
                }
            }
        }

        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool InSimulation
        {
            get => mInSimulation;
            set
            {
                if (mInSimulation != value)
                {
                    mInSimulation = value;
                    SimulationChanged?.Invoke(this, mInSimulation);
                }
            }
        }

        public MagazineLoaderModule()
        {
            Settings = new MagazineLoaderDataModel();
            Settings.DataModelChanged += SettingsChanged;

            InitializeSettings();
            mRecipeManager.MachineRecipeLoaded += RecipeManager_MachineRecipeLoaded;
            mRecipeManager.ProductRecipeChanged += RecipeManager_ProductRecipeChanged;

            TagModelPathDictionary = new Dictionary<ITagModel, string>
            {
                {mMagazineLoaderStatusTagModel, nameof(Status) },
                {mMagazineLoaderStateTagModel, nameof(State) },
                {mIsRunningProductionTagModel, nameof(IsRunningProduction) },
                {mInSimulationCommandTagModel, nameof(InSimulation) },
                //Settings
                {mVelocityPercentageTagModel, GetNameOf(() => Settings.VelocityPercentage)},
                //Resources
                {Resources.UpperConveyor.StatusTagModel, GetNameOf(() => Resources.UpperConveyor.Status)},
                {Resources.UpperHatch.StatusTagModel, GetNameOf(() => Resources.UpperHatch.Status)},
                {Resources.Lift.StatusTagModel, GetNameOf(() => Resources.Lift.Status)},
                {Resources.LeadFramePusher.StatusTagModel, GetNameOf(() => Resources.LeadFramePusher.Status)},
                {Resources.LowerHatch.StatusTagModel, GetNameOf(() => Resources.LowerHatch.Status)},
                {Resources.LowerConveyor.StatusTagModel, GetNameOf(() => Resources.LowerConveyor.Status)}
             };

            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.LoadMagazine), SubProcesses.LoadMagazine.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.ProvideMaterial), SubProcesses.ProvideMaterial.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.UnloadMagazine), SubProcesses.UnloadMagazine.TagModelPathDictionary);

            AddTagModelDictionaryOfProperty(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(nameof(EventHandler), EventHandler.TagModelPathDictionary);

            AddSubscription();

            mEventManager.RegisterEvents(MagazineLoaderEvents.Events);

            mPlcService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }

        private void InitializeSettings()
        {          
            RecipeManager_MachineRecipeLoaded(this, mRecipeManager.GetMachineRecipe());          
            RecipeManager_ProductRecipeChanged(this, mRecipeManager.GetCurrentRecipe());
        }

        public void Initialize()
        {
            mLog.Debug($"Initialize: MagazineLoaderModule");
            mPLCService.WriteValue(mInitializeCommandTagModel, true);
        }

        public void Start()
        {
            mLog.Debug($"Start: MagazineLoaderModule ");
            mPLCService.WriteValue(mStartCommandTagModel, true);
        }

        public void Stop()
        {
            mLog.Debug($"Stop: MagazineLoaderModule ");
            mPLCService.WriteValue(mStopCommandTagModel, true);
        }

        public void ExecuteLoadMagazine()
        {
            mPLCService.WriteValue(mLoadMagazineCommandTagModel, true);
        }

        public void ExecuteProvideMaterial()
        {
            mPLCService.WriteValue(mProvideMaterialCommandTagModel, true);
        }

        public void ExecuteUnloadMagazine()
        {
            mPLCService.WriteValue(mUnloadMagazineCommandTagModel, true);
        }

        public void SetVelocityPercentage(int percentage)
        {
            mPLCService.WriteValue(mVelocityPercentageTagModel, (short) percentage);
        }

        public void EnableSimulation()
        {
            mPLCService.WriteValue(mSimulationEnabledCommandTagModel, true);
        }

        public void DisableSimulation()
        {
            mPLCService.WriteValue(mSimulationEnabledCommandTagModel, false);
        }

        private void RecipeManager_MachineRecipeLoaded(object sender, MachineRecipe e)
        {
            SubProcesses.LoadMagazine.UpdateMachineRecipeSettingsOnPLC(e.MagazineLoader.LoadMagazine.ConveyorUpperZPosition, 
                e.MagazineLoader.LoadMagazine.StartPickOffsetZ, 
                e.MagazineLoader.LoadMagazine.EndPickOffsetZ, 
                e.MagazineLoader.LoadMagazine.PickY, 
                e.MagazineLoader.LoadMagazine.MagazineLiftSafePositionY);

            SubProcesses.ProvideMaterial.UpdateMachineRecipeSettingsOnPLC(e.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionY, e.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionZ);            
            SubProcesses.UnloadMagazine.UpdateMachineRecipeSettingsOnPLC(e.MagazineLoader.UnloadMagazine.PlaceYPosition, e.MagazineLoader.UnloadMagazine.StartPlaceOffsetZPosition, 
                e.MagazineLoader.UnloadMagazine.PlaceYPosition,
                e.MagazineLoader.UnloadMagazine.MagazineLiftSafePositionY);
        }

        private void RecipeManager_ProductRecipeChanged(object sender, ProductRecipe e)
        {
            SubProcesses.LoadMagazine.UpdateProductRecipeSettingsOnPLC(e.Magazine.SlotCount);
            SubProcesses.ProvideMaterial.UpdateProductRecipeSettingsOnPLC(e.Magazine.DevicesPerSlot, e.Magazine.BottomSlotOffset, e.Magazine.SlotOffset, e.Magazine.MagazineWidth);            
        }    
    }
}
