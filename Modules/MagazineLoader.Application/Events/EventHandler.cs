﻿using EventHandler.Application;
using MagazineLoader.Lib;

namespace MagazineLoader.Application
{
    public class EventHandler: EventHandlerSubScriptionBase
    {
        public bool LowerConveyorFullWarningSet { set => SetEventPropertyValue(value); }
        public bool LowerConveyorFullErrorSet { set => SetEventPropertyValue(value); }
        public bool MagazineOrientationWarningIsSet { set => SetEventPropertyValue(value); }
        public bool MagazineOrientationErrorIsSet { set => SetEventPropertyValue(value); }
        public bool MagazineNotPresentSet { set => SetEventPropertyValue(value); }
        public bool NoProductAtTransferSet { set => SetEventPropertyValue(value); }
        public bool MagazineLoaderInSimulationSet { set => SetEventPropertyValue(value); }

        public EventHandler()
        {
            mEventDictionary = new()
            {
                { nameof(MagazineNotPresentSet), MagazineLoaderEvents.MagazineNotPresent },
                { nameof(NoProductAtTransferSet), MagazineLoaderEvents.NoProductAtTransfer },
                { nameof(LowerConveyorFullWarningSet), MagazineLoaderEvents.LowerConveyorFullWarning },
                { nameof(LowerConveyorFullErrorSet), MagazineLoaderEvents.LowerConveyorFullError },
                { nameof(MagazineOrientationWarningIsSet), MagazineLoaderEvents.MagazineOrientationWarning },
                { nameof(MagazineOrientationErrorIsSet), MagazineLoaderEvents.MagazineOrientationError },
                { nameof(MagazineLoaderInSimulationSet), MagazineLoaderEvents.MagazineLoaderInSimulation },
            };

            RegisterTagModelsInDictionary();
        }
    }
}
