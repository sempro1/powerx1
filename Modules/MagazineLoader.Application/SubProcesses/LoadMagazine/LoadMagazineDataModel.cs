﻿namespace MagazineLoader.Application.SubProcesses
{
    public class LoadMagazineDataModel
    {
        public double ConveyorUpperZPosition { get; set; }
        public double StartPickOffsetZ { get; set; }
        public double EndPickOffsetZ { get; set; }
        public double PickY { get; set; }
        public double MagazinePositionLiftSafePositionY { get; set; }
        public short SlotCount { get; set; }        
    }
}
