﻿namespace MagazineLoader.Application.SubProcesses
{
   public class UnloadMagazineDataModel
    {
        public double LowerConveyorZ { get; set; }
        public double StartPlaceOffsetZ { get; set; }
        public double PlaceY { get; set; }
        public double MagazinePositionLiftSafePosition { get; set; }         
    }
}
