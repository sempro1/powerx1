﻿using MachineManager;
using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.OpcUa;
using System;

namespace MagazineLoader.Application.SubProcesses
{
    public class UnloadMagazine: SubscriptionBase
    {
        private readonly TagModel mStatusTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazinePCInterface.ToPc.Status);

        private readonly TagModel mLowerConveyorZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazinePCInterface.ToPlc.Unload.LowerConveyorZ);
        private readonly TagModel mStartPlaceOffsetZTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazinePCInterface.ToPlc.Unload.StartPlaceOffsetZ);
        private readonly TagModel mPlaceYTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazinePCInterface.ToPlc.Unload.PlaceY);
        private readonly TagModel mMagazineLiftSafePositionTagModel = MagazineLoaderOpcUA.GetTagModel(x => x.MagazineLoader.MagazineLoaderUnloadMagazinePCInterface.ToPlc.Unload.LiftSafePositionY);       
        private UnloadMagazineDataModel mSettings;
        private int mStatus;

        public event EventHandler<UnloadMagazineDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;
        public UnloadMagazineDataModel Settings
        {
            get => mSettings;
            set
            {
                mSettings = value;
                SettingsChanged?.Invoke(this, value);
            }
        }
        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public double LowerConveyorZ
        {
            get => (float)mSettings.LowerConveyorZ;
            set
            {
                if (mSettings.LowerConveyorZ != value)
                {
                   mSettings.LowerConveyorZ = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double StartPlaceOffsetZ
        {
            get => (float)mSettings.StartPlaceOffsetZ;
            set
            {
                if (mSettings.StartPlaceOffsetZ != value)
                {
                    mSettings.StartPlaceOffsetZ = Convert.ToDouble(value);
                   SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double PlaceY
        {
            get => (float)mSettings.PlaceY;
            set
            {
                if (mSettings.PlaceY != value)
                {
                    mSettings.PlaceY = Convert.ToDouble(value);
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MagazinePositionLiftSafePosition
        {
            get => mSettings.MagazinePositionLiftSafePosition;
            set
            {
                if (mSettings.MagazinePositionLiftSafePosition != value)
                {
                    mSettings.MagazinePositionLiftSafePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public void UpdateSettings(UnloadMagazineDataModel settings)
        {
            UpdateMachineRecipeSettingsOnPLC(settings.LowerConveyorZ, settings.StartPlaceOffsetZ, settings.PlaceY, settings.MagazinePositionLiftSafePosition);            
        }

        public async void UpdateMachineRecipeSettingsOnPLC(double lowerConveyorZ, double startPlaceOffsetZ, double placY, double magazineLiftSafePosition)
        {
            Settings.StartPlaceOffsetZ = startPlaceOffsetZ;

            await PlcService.Instance.WriteValueAsync(mLowerConveyorZTagModel, (float)lowerConveyorZ);
            await PlcService.Instance.WriteValueAsync(mStartPlaceOffsetZTagModel, (float)startPlaceOffsetZ);
            await PlcService.Instance.WriteValueAsync(mPlaceYTagModel, (float)placY);
            await PlcService.Instance.WriteValueAsync(mMagazineLiftSafePositionTagModel, (float)magazineLiftSafePosition);

        }
        

        public UnloadMagazine()
        {
            Settings = new UnloadMagazineDataModel();

            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)},
                { mLowerConveyorZTagModel, nameof(LowerConveyorZ)},
                { mStartPlaceOffsetZTagModel, nameof(StartPlaceOffsetZ)},
                { mPlaceYTagModel, nameof(PlaceY)},
                { mMagazineLiftSafePositionTagModel, nameof(MagazinePositionLiftSafePosition)}
            };
        }
    }
}
