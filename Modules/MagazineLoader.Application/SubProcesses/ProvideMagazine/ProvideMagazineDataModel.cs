﻿namespace MagazineLoader.Application.SubProcesses
{
   public class ProvideMagazineDataModel
    {
        public double MagazineSlotOffset { get; set; }
        public double MagazineBottomSlotOffset { get; set; }
        public short DeviceCountPerLeadFrame { get; set; }
        public double MagazineLiftStartPositionZ { get; set; }
        public double MagazineLiftStartPositionY { get; set; }
        public double MagazineWidth { get; set; }
    }
}
