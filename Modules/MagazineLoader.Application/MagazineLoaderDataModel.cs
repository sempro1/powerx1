﻿using System;

namespace MagazineLoader.Application
{
    public class MagazineLoaderDataModel
    {
        private int mVelocityPercentage;

        public event EventHandler<MagazineLoaderDataModel> DataModelChanged;
        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                mVelocityPercentage = value;
                DataModelChanged?.Invoke(this, this);
            }
        }
    }
}
