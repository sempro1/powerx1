﻿using LotControl.Application;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application
{
   public class MaterialTracking: SubscriptionBase
    {
        public event EventHandler<int> InfeedHasMaterialChanged;
        public event EventHandler<int> TrimmingHasMaterialChanged;
        public event EventHandler<int> DambarHasMaterialChanged;
        public event EventHandler<int> WasteBinHasMaterialChanged;
        public event EventHandler<int> FormingHasMaterialChanged;

        public event EventHandler<int> IndexerPosition0HasMaterialChanged;
        public event EventHandler<int> IndexerPosition1HasMaterialChanged;
        public event EventHandler<int> IndexerPosition2HasMaterialChanged;
        public event EventHandler<int> IndexerPosition3HasMaterialChanged;
        public event EventHandler<int> IndexerPosition4HasMaterialChanged;

        MaterialTrackingApplication mMaterialTracking;
        LotControl.Application.LotControl mLotControl;

        private bool mInfeedHasMaterial;
        private bool mTrimmingHasMaterial;
        private bool mDambarHasMaterial;
        private bool mWasteBinHasMaterial;
        private bool mFormingHasMaterial;

        private bool mIndexerPosition0HasMaterial;
        private bool mIndexerPosition1HasMaterial;
        private bool mIndexerPosition2HasMaterial;
        private bool mIndexerPosition3HasMaterial;
        private bool mIndexerPosition4HasMaterial;

        private int mTrimmingProcessState;
        private int mFormingProcessState;
        private int mInspectedDambarProcessState;

        private int mInspectionDambarResult;

        private readonly TagModel mInfeedHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation0.HasMaterial);
        private readonly TagModel mTrimmingHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mDambarHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mWasteBinHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mFormingHasMaterialTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation4.HasMaterial);

        private readonly TagModel mIndexPosition0HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation0.HasMaterial);
        private readonly TagModel mIndexPosition1HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation1.HasMaterial);
        private readonly TagModel mIndexPosition2HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation2.HasMaterial);
        private readonly TagModel mIndexPosition3HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation3.HasMaterial);
        private readonly TagModel mIndexPosition4HasMaterial = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation4.HasMaterial);

        private readonly TagModel mTrimmingProcessStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation1.Device.ProcessedState);
        private readonly TagModel mFormingProcessStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation4.Device.ProcessedState);
        private readonly TagModel mInspectedDambarProcessStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation3.Device.ProcessedState);

        private readonly TagModel mDambarInspectionResultTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation3.Device.DambarInspectionResult);
      
        public bool InfeedHasMaterial
        {
            get => mInfeedHasMaterial;
            set
            {
                if (mInfeedHasMaterial != value)
                {
                    mInfeedHasMaterial = value;

                    if (mInfeedHasMaterial)
                        InfeedHasMaterialChanged?.Invoke(this, GetPressPositionDeviceId(0));
                    else
                        InfeedHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool TrimmingHasMaterial
        {
            get => mTrimmingHasMaterial;
            set
            {
                if (mTrimmingHasMaterial != value)
                {
                    mTrimmingHasMaterial = value;
                    if (mTrimmingHasMaterial)
                        TrimmingHasMaterialChanged?.Invoke(this, GetPressPositionDeviceId(1));
                    else
                        TrimmingHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool DambarHasMaterial
        {
            get => mDambarHasMaterial;
            set
            {
                if (mDambarHasMaterial != value)
                {
                    mDambarHasMaterial = value;
                    if (mDambarHasMaterial)
                        DambarHasMaterialChanged?.Invoke(this, GetPressPositionDeviceId(2));
                    else
                        DambarHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool WasteBinHasMaterial
        {
            get => mWasteBinHasMaterial;
            set
            {
                if (mWasteBinHasMaterial != value)
                {
                    mWasteBinHasMaterial = value;
                    if (mWasteBinHasMaterial)
                        WasteBinHasMaterialChanged?.Invoke(this, GetPressPositionDeviceId(3));
                    else
                        WasteBinHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool FormingHasMaterial
        {
            get => mFormingHasMaterial;
            set
            {
                if (mFormingHasMaterial != value)
                {
                    mFormingHasMaterial = value;
                    if (mFormingHasMaterial)
                        FormingHasMaterialChanged?.Invoke(this, GetPressPositionDeviceId(4));
                    else
                        FormingHasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition0HasMaterial
        {
            get => mIndexerPosition0HasMaterial;
            set
            {
                if (mIndexerPosition0HasMaterial != value)
                {
                    mIndexerPosition0HasMaterial = value;
                    if (mIndexerPosition0HasMaterial)
                        IndexerPosition0HasMaterialChanged?.Invoke(this, GetIndexerPositionDeviceId(0));
                    else
                        IndexerPosition0HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition1HasMaterial
        {
            get => mIndexerPosition1HasMaterial;
            set
            {
                if (mIndexerPosition1HasMaterial != value)
                {
                    mIndexerPosition1HasMaterial = value;
                    if (mIndexerPosition1HasMaterial)
                        IndexerPosition1HasMaterialChanged?.Invoke(this, GetIndexerPositionDeviceId(1));
                    else
                        IndexerPosition1HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition2HasMaterial
        {
            get => mIndexerPosition2HasMaterial;
            set
            {
                if (mIndexerPosition2HasMaterial != value)
                {
                    mIndexerPosition2HasMaterial = value;
                    if (mIndexerPosition2HasMaterial)
                        IndexerPosition2HasMaterialChanged?.Invoke(this, GetIndexerPositionDeviceId(2));
                    else
                        IndexerPosition2HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition3HasMaterial
        {
            get => mIndexerPosition3HasMaterial;
            set
            {
                if (mIndexerPosition3HasMaterial != value)
                {
                    mIndexerPosition3HasMaterial = value;
                    if (mIndexerPosition3HasMaterial)
                        IndexerPosition3HasMaterialChanged?.Invoke(this, GetIndexerPositionDeviceId(3));
                    else
                        IndexerPosition3HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }

        public bool IndexerPosition4HasMaterial
        {
            get => mIndexerPosition4HasMaterial;
            set
            {
                if (mIndexerPosition4HasMaterial != value)
                {
                    mIndexerPosition4HasMaterial = value;
                    if (mIndexerPosition4HasMaterial)
                        IndexerPosition4HasMaterialChanged?.Invoke(this, GetIndexerPositionDeviceId(4));
                    else
                        IndexerPosition4HasMaterialChanged?.Invoke(this, 0);
                }
            }
        }
        public int TrimmingProcessState
        {
            get => mTrimmingProcessState;
            set
            {
                if (mTrimmingProcessState != value)
                {
                    mTrimmingProcessState = value;

                    if (value == (int)DeviceProcessState.Trimmed)
                    {
                        var id = GetPressPositionDeviceId((int)DeviceLocation.Trim);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mTrimmingProcessState);
                        }
                    }
                }
            }
        }

        public int FormingProcessState
        {
            get => mFormingProcessState;
            set
            {
                if (mFormingProcessState != value)
                {
                    mFormingProcessState = value;

                    if (value == (int)DeviceProcessState.Formed)
                    {
                        var id = GetPressPositionDeviceId((int)DeviceLocation.Form);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mFormingProcessState);
                        }
                    }
                }
            }
        }

        public int InspectedDambarProcessState
        {
            get => mInspectedDambarProcessState;
            set
            {
                if (mInspectedDambarProcessState != value)
                {
                    mInspectedDambarProcessState = value;

                    if (value == (int)DeviceProcessState.InspectedDambar)
                    {
                        var id = GetPressPositionDeviceId((int)DeviceLocation.Dambar);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, (DeviceProcessState)mInspectedDambarProcessState);
                        }
                    }
                }
            }
        }

        public int InspectionDambarResult
        {
            get => mInspectionDambarResult;
            set
            {
                if (mInspectionDambarResult != value)
                {
                    mInspectionDambarResult = value;

                    if (value != (int)ValidationResults.None)
                    {
                        var id = GetPressPositionDeviceId((int)DeviceLocation.Dambar);

                        if (id != 0)
                        {
                            mMaterialTracking.UpdateDevice(id, mLotControl.CurrentLotId, DeviceProcessTypeResult.InspectionDambarResult, (ValidationResults) mInspectionDambarResult);
                        }
                    }
                }
            }
        }


        public MaterialTracking()
        {
            mMaterialTracking = MaterialTrackingApplication.Instance;
            mLotControl = LotControl.Application.LotControl.Instance;

            TagModelPathDictionary = new()
            {
                {mInfeedHasMaterialTagModel , nameof(InfeedHasMaterial) },
                {mTrimmingHasMaterialTagModel , nameof(TrimmingHasMaterial) },
                {mDambarHasMaterialTagModel , nameof(DambarHasMaterial) },
                {mWasteBinHasMaterialTagModel , nameof(WasteBinHasMaterial) },
                {mFormingHasMaterialTagModel , nameof(FormingHasMaterial) },
                {mIndexPosition0HasMaterial  , nameof(IndexerPosition0HasMaterial) },
                {mIndexPosition1HasMaterial  , nameof(IndexerPosition1HasMaterial) },
                {mIndexPosition2HasMaterial  , nameof(IndexerPosition2HasMaterial) },
                {mIndexPosition3HasMaterial  , nameof(IndexerPosition3HasMaterial) },
                {mIndexPosition4HasMaterial  , nameof(IndexerPosition4HasMaterial) },
                {mTrimmingProcessStateTagModel, nameof(TrimmingProcessState) },
                {mFormingProcessStateTagModel, nameof(FormingProcessState) },
                {mInspectedDambarProcessStateTagModel, nameof(InspectedDambarProcessState) },
                {mDambarInspectionResultTagModel, nameof(InspectionDambarResult) }
            };
        }
        public int GetIndexerPositionDeviceId(int position)
        {
            TagModel tagModel;
            switch (position)
            {
                case 0:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation0.Device.ID);
                    break;
                case 1:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation1.Device.ID);
                    break;
                case 2:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation2.Device.ID);
                    break;
                case 3:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation3.Device.ID);
                    break;
                case 4:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleIndexerDeviceLocations.DeviceLocation4.Device.ID);
                    break;
                default:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation0.Device.ID);
                    break;
            }

            return (int)mPlcService.ReadValue(tagModel);
        }

        public int GetPressPositionDeviceId(int position)
        {
            TagModel tagModel;
            switch (position)
            {
                case 0:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation0.Device.ID);
                    break;
                case 1:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation1.Device.ID);
                    break;
                case 2:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation2.Device.ID);
                    break;
                case 3:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation3.Device.ID);
                    break;
                case 4:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation4.Device.ID);
                    break;
                default:
                    tagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleMaterialTracking.PressModuleDeviceLocations.DeviceLocation0.Device.ID);
                    break;
            }

            return (int)mPlcService.ReadValue(tagModel);
        }

        
    }
}
