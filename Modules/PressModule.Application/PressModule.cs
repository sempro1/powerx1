﻿using log4net;
using Logging;
using MachineManager;
using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using RecipeManager;
using System;
using System.Collections.Generic;
using X1.Recipe.Lib;

namespace PressModule.Application
{
   public class PressModule: SubscriptionBase
    {
        public event EventHandler<bool> HasErrorChanged;
        public event EventHandler<int> StateChanged;
        public event EventHandler<int> StatusChanged;
        public event EventHandler<bool> SimulationChanged;

        public event EventHandler<PressModuleDataModel> SettingsChanged;

        private ILog mLog = LogClient.Get();
        private PlcService mPLCService = PlcService.Instance;

        private bool mHasError;
        private int mState;
        private int mStatus;

        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        private static PressModule mPressmodule;
        private bool mIsRunningProduction;
        private bool mInSimulation;

        private readonly TagModel mPressModuleStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPc.Status);
        private readonly TagModel mPressModuleStateTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPc.State);
        private readonly TagModel mPressModuleIsRunningProductionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPc.IsRunningProduction);
        private readonly TagModel mPressModuleHasErrorTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPc.HasError);
        private readonly TagModel mPressModuleInSimulationTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPc.InSimulation);

        private readonly TagModel mInitializeCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.InitializeCommand);
        private readonly TagModel mStartCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.StartCommand);
        private readonly TagModel mStopCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.StopCommand);
        private readonly TagModel mInfeedCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.InfeedCommand);
        private readonly TagModel mTrimCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.TrimCommand);
        private readonly TagModel mFormCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.FormCommand);
        private readonly TagModel mIndexPickCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.IndexPickCommand);
        private readonly TagModel mIndexPlaceCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.IndexPlaceCommand);
        private readonly TagModel mDambarCommandTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.DambarCommand);
        private readonly TagModel mEnableSimulationTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.EnableSimulation);
        private readonly TagModel mVelocityPercentageTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModulePcInterface.ToPlc.VelocityPercentage);

        private PressModuleDataModel mSettings;
        public static PressModule Instance => mPressmodule ?? (mPressmodule = new PressModule());
        public SubProcesses.SubProcesses SubProcesses { get; } = new SubProcesses.SubProcesses();
        public Resources.Resources Resources { get; } = new Resources.Resources();
        public MaterialTracking MaterialTracking { get; } = new MaterialTracking();

        public PressModuleDataModel Settings
        {
            get => mSettings;
            set
            {
                mSettings = value;
                SettingsChanged?.Invoke(this, value);
            }
        }

        public bool IsRunningProduction
        {
            get => mIsRunningProduction;
            set
            {
                if (mIsRunningProduction != value)
                {
                    mIsRunningProduction = value;
                }
            }
        }

        public bool HasError
        {
            get => mHasError;
            set
            {
                if (mHasError != value)
                {
                    mHasError = value;
                    HasErrorChanged?.Invoke(this, mHasError);
                }
            }
        }
        public int State
        {
            get => mState;
            set
            {
                if (mState != value)
                {
                    mState = value;
                    StateChanged?.Invoke(this, mState);
                }
            }
        }

        public int Status
        {
            get => mStatus;
            set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public bool InSimulation
        {
            get => mInSimulation;
            set
            {
                if (mInSimulation != value)
                {
                    mInSimulation = value;
                    SimulationChanged?.Invoke(this, mInSimulation);
                }
            }
        }

        public PressModule()
        {
            Settings = new PressModuleDataModel();
            Settings.DataModelChanged += SettingsChanged;

            InitializeSettings();
            mRecipeManager.ProductRecipeChanged += RecipeManager_ProductRecipeChanged;

            TagModelPathDictionary = new ()
            {
                {mPressModuleStatusTagModel, nameof(Status) },
                {mPressModuleStateTagModel, nameof(State) },
                {mPressModuleIsRunningProductionTagModel, nameof(IsRunningProduction) },
                {mPressModuleHasErrorTagModel, nameof(HasError) },
                {mPressModuleInSimulationTagModel, nameof(InSimulation) },
                {mVelocityPercentageTagModel, GetNameOf(() => Settings.VelocityPercentage)},
                {Resources.Trim.StatusTagModel, GetNameOf(() => Resources.Trim.Status) },
                {Resources.Form.StatusTagModel, GetNameOf(() => Resources.Form.Status) },
                {Resources.Indexer.StatusTagModel, GetNameOf(() => Resources.Indexer.Status) },
                {Resources.VacuumCleaner.StatusTagModel, GetNameOf(() => Resources.VacuumCleaner.Status) }
            };

            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Infeed), SubProcesses.Infeed.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.IndexHandler), SubProcesses.IndexHandler.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Trimming), SubProcesses.Trimming.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Dambar), SubProcesses.Dambar.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.Forming), SubProcesses.Forming.TagModelPathDictionary);
            AddTagModelDictionaryOfProperty(GetNameOf(() => SubProcesses.WasteHandler), SubProcesses.WasteHandler.TagModelPathDictionary);

            AddTagModelDictionaryOfProperty(nameof(MaterialTracking), MaterialTracking.TagModelPathDictionary);

            AddSubscription();
            mPLCService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            // On reconnect
            if (e == ConnectionState.Online)
            {
                InitializeSettings();
            }
        }

        private void InitializeSettings()
        {           
            RecipeManager_ProductRecipeChanged(this, mRecipeManager.GetCurrentRecipe());
        }

        private void RecipeManager_ProductRecipeChanged(object sender, ProductRecipe e)
        {
            SubProcesses.Trimming.UpdateProductRecipeSettingsOnPLC(e.Trim.TopPosition, e.Trim.SensorPosition, e.Trim.ClosePosition, e.Trim.Velocity, e.Trim.RemovePosition, e.Trim.MaintenancePosition);
            SubProcesses.Forming.UpdateProductRecipeSettingsOnPLC(e.Form.TopPosition, e.Form.SensorPosition, e.Form.ClosePosition, e.Form.Velocity, e.Form.RemovePosition, e.Form.MaintenancePosition);
            SubProcesses.WasteHandler.UpdateProductRecipeSettingsOnPLC(e.WasteHandlerBin.AlmostFullCount, e.WasteHandlerBin.MaximumWasteCount);
        }

        public void Initialize()
        {
            mLog.Debug($"Initialize: PressModule");
            mPLCService.WriteValue(mInitializeCommandTagModel, true);
        }

        public void Start()
        {
            mLog.Debug($"Start: PressModule");
            mPLCService.WriteValue(mStartCommandTagModel, true);
        }

        public void Stop()
        {
            mLog.Debug($"Stop: PressModule");
            mPLCService.WriteValue(mStopCommandTagModel, true);
        }
               
        public void ExecuteTrim()
        {
            mLog.Debug($"ExecuteTrim: PressModule");
            mPLCService.WriteValue(mTrimCommandTagModel, true);
        }
        public void ExecuteForm()
        {
            mLog.Debug($"ExecuteForm: PressModule");
            mPLCService.WriteValue(mFormCommandTagModel, true);
        }
        public void ExecuteIndexerPick()
        {
            mLog.Debug($"ExecuteIndexerPick: PressModule");
            mPLCService.WriteValue(mIndexPickCommandTagModel, true);
        }
        public void ExecuteIndexerPlace()
        {
            mLog.Debug($"ExecuteIndexerPlace: PressModule");
            mPLCService.WriteValue(mIndexPlaceCommandTagModel, true);
        }
        public void ExecuteDambar()
        {
            mLog.Debug($"ExecuteDambar: PressModule");
            mPLCService.WriteValue(mDambarCommandTagModel, true);
        }
        public void EnableSimulation()
        {
            mLog.Debug($"ExecuteEnableSimulation: PressModule");
            mPLCService.WriteValue(mEnableSimulationTagModel, true);
        }
        public void DisableSimulation()
        {
            mLog.Debug($"ExecuteDisableSimulation: PressModule");
            mPLCService.WriteValue(mEnableSimulationTagModel, false);
        }
        public void SetVelocityPercentage(int percentage)
        {
            mLog.Debug($"VelocityPercentage: PressModule");
            mPLCService.WriteValue(mVelocityPercentageTagModel, (short)percentage);
        }
    }
}
