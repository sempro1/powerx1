﻿using MachineManager;
using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses
{
    public class Dambar : SubscriptionBase
    {
        public event EventHandler<int> StatusChanged;

        private readonly TagModel mDambarStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleDambarPcInterface.ToPc.Status);
        private int mDambarStatus;
        public int Status
        {
            get => mDambarStatus;
                private set
            {
                if (mDambarStatus != value)
                {
                    mDambarStatus = value;
                    StatusChanged?.Invoke(this, mDambarStatus);
                }
            }
        }

        public Dambar()
        {
            TagModelPathDictionary = new()
            {
                { mDambarStatusTagModel, nameof(Status)}
            };

            AddSubscription();
        }
    }
}
