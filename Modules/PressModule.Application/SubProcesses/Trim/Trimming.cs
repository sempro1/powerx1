﻿using MachineManager;
using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses
{
    public class Trimming : SubscriptionBase
    {
        private readonly TagModel mTrimmingStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPc.Status);

        private readonly TagModel mTopPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.TopPosition);
        private readonly TagModel mSensorPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.SensorPosition);
        private readonly TagModel mClosePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.ClosePosition);
        private readonly TagModel mRemovePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.RemovePosition);
        private readonly TagModel mMaintenancePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.MaintenacePosition);
        private readonly TagModel mVelocityTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimmingPcInterface.ToPlc.Trimming.Velocity);
        private TrimmingDataModel mSettings;

        private int mTrimmingStatus;
        public event EventHandler<TrimmingDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;
        public TrimmingDataModel Settings => mSettings;
        public int Status
        {
            get => mTrimmingStatus;
            private set
            {
                if (mTrimmingStatus != value)
                {
                    mTrimmingStatus = value;
                    StatusChanged?.Invoke(this, mTrimmingStatus);
                }
            }
        }

        public double TopPosition
        {
            get => (float)mSettings.TopPosition;
            set
            {
                if (mSettings.TopPosition != value)
                {
                    mSettings.TopPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double SensorPosition
        {
            get => (float)mSettings.SensorPosition;
            set
            {
                if (mSettings.SensorPosition != value)
                {
                    mSettings.SensorPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double ClosePosition
        {
            get => (float)mSettings.ClosePosition;
            set
            {
                if (mSettings.ClosePosition != value)
                {
                    mSettings.ClosePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double RemovePosition
        {
            get => (float)mSettings.RemovePosition;
            set
            {
                if (mSettings.RemovePosition != value)
                {
                    mSettings.RemovePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MaintenancePosition
        {
            get => (float)mSettings.MaintenancePosition;
            set
            {
                if (mSettings.MaintenancePosition != value)
                {
                    mSettings.MaintenancePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Velocity
        {
            get => (float)mSettings.Velocity;
            set
            {
                if (mSettings.Velocity != value)
                {
                    mSettings.Velocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public  void UpdateSettings(TrimmingDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(Settings.TopPosition, settings.SensorPosition, settings.ClosePosition, settings.RemovePosition, settings.MaintenancePosition, settings.Velocity);
        }

        public async void UpdateProductRecipeSettingsOnPLC(double topPosition, double sensorPosition, double closePosition, double removePosition, double maintenancePosition, double velocity)
        {
            await PlcService.Instance.WriteValueAsync(mTopPositionTagModel, (float)topPosition);
            await PlcService.Instance.WriteValueAsync(mSensorPositionTagModel, (float)sensorPosition);
            await PlcService.Instance.WriteValueAsync(mClosePositionTagModel, (float)closePosition);
            await PlcService.Instance.WriteValueAsync(mRemovePositionTagModel, (float)removePosition);
            await PlcService.Instance.WriteValueAsync(mMaintenancePositionTagModel, (float)maintenancePosition);
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, (float)velocity);
        }
        public Trimming()
        {
            mSettings = new TrimmingDataModel();

            TagModelPathDictionary = new()
            {
                 { mTrimmingStatusTagModel, nameof(Status) },
                 { mTopPositionTagModel, nameof(TopPosition) },
                 { mSensorPositionTagModel, nameof(SensorPosition) },
                 { mClosePositionTagModel, nameof(ClosePosition) },
                 { mRemovePositionTagModel, nameof(RemovePosition) },
                 { mMaintenancePositionTagModel, nameof(MaintenancePosition) },
                 { mVelocityTagModel, nameof(Velocity) }
             };         
        }        
    }
}
