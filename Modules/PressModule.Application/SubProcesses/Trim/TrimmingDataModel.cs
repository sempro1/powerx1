﻿namespace PressModule.Application.SubProcesses
{
   public class TrimmingDataModel
    {
        public double TopPosition { get; set; }
        public double SensorPosition { get; set; }
        public double ClosePosition { get; set; }
        public double RemovePosition { get; set; }
        public double MaintenancePosition { get; set; }
        public double Velocity { get; set; }
    }
}
