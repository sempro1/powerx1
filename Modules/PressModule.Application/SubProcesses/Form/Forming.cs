﻿using MachineManager;
using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses
{
    public class Forming : SubscriptionBase
    {
        public event EventHandler<FormingDataModel> SettingsChanged;
        public event EventHandler<int> StatusChanged;

        private readonly TagModel mFormingStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPc.Status);

        private readonly TagModel mTopPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.TopPosition);
        private readonly TagModel mSensorPositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.SensorPosition);
        private readonly TagModel mClosePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.ClosePosition);
        private readonly TagModel mRemovePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.RemovePosition);
        private readonly TagModel mMaintenancePositionTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.MaintenacePosition);
        private readonly TagModel mVelocityTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormingPcInterface.ToPlc.Forming.Velocity);
        private FormingDataModel mSettings;

        private int mFormingStatus;

        public FormingDataModel Settings => mSettings;

        public int Status
        {
            get => mFormingStatus;
            private set
            {
                if (mFormingStatus != value)
                {
                    mFormingStatus = value;
                    StatusChanged?.Invoke(this, mFormingStatus);
                }
            }
        }
        
        public double TopPosition
        {
            get => (float)mSettings.TopPosition;
            set
            {
                if (mSettings.TopPosition != value)
                {
                    mSettings.TopPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double SensorPosition
        {
            get => (float)mSettings.SensorPosition;
            set
            {
                if (mSettings.SensorPosition != value)
                {
                    mSettings.SensorPosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double ClosePosition
        {
            get => (float)mSettings.ClosePosition;
            set
            {
                if (mSettings.ClosePosition != value)
                {
                    mSettings.ClosePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double RemovePosition
        {
            get => (float)mSettings.RemovePosition;
            set
            {
                if (mSettings.RemovePosition != value)
                {
                    mSettings.RemovePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double MaintenancePosition
        {
            get => (float)mSettings.MaintenancePosition;
            set
            {
                if (mSettings.MaintenancePosition != value)
                {
                    mSettings.MaintenancePosition = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public double Velocity
        {
            get => (float)mSettings.Velocity;
            set
            {
                if (mSettings.Velocity != value)
                {
                    mSettings.Velocity = value;
                    SettingsChanged?.Invoke(this, mSettings);
                }
            }
        }

        public void UpdateSettings(FormingDataModel settings)
        {
            UpdateProductRecipeSettingsOnPLC(Settings.TopPosition, settings.SensorPosition, settings.ClosePosition, settings.RemovePosition, settings.MaintenancePosition, settings.Velocity);
        }

        public async void UpdateProductRecipeSettingsOnPLC(double topPosition, double sensorPosition, double closePosition, double removePosition, double maintenancePosition, double velocity)
        {
            await PlcService.Instance.WriteValueAsync(mTopPositionTagModel, (float)topPosition);
            await PlcService.Instance.WriteValueAsync(mSensorPositionTagModel, (float)sensorPosition);
            await PlcService.Instance.WriteValueAsync(mClosePositionTagModel, (float)closePosition);
            await PlcService.Instance.WriteValueAsync(mRemovePositionTagModel, (float)removePosition);
            await PlcService.Instance.WriteValueAsync(mMaintenancePositionTagModel, (float)maintenancePosition);
            await PlcService.Instance.WriteValueAsync(mVelocityTagModel, (float)velocity);
        }
        public Forming()
        {
            mSettings = new FormingDataModel();

            TagModelPathDictionary = new()
            {
                 { mFormingStatusTagModel, nameof(Status) },
                 { mTopPositionTagModel, nameof(TopPosition) },
                 { mSensorPositionTagModel, nameof(SensorPosition) },
                 { mClosePositionTagModel, nameof(ClosePosition) },
                 { mRemovePositionTagModel, nameof(RemovePosition) },
                 { mMaintenancePositionTagModel, nameof(MaintenancePosition) },
                 { mVelocityTagModel, nameof(Velocity) }
             };                        
        }        
    }
}
