﻿using MachineManager;
using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;
using System.Collections.Generic;

namespace PressModule.Application.SubProcesses.Infeed
{
    public class Infeed : SubscriptionBase
    {

        private readonly TagModel mStatusTagModel = PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleInfeedPcInterface.ToPc.Status);
        private int mStatus;

        public event EventHandler<int> StatusChanged;
        public int Status
        {
            get => mStatus;
                private set
            {
                if (mStatus != value)
                {
                    mStatus = value;
                    StatusChanged?.Invoke(this, mStatus);
                }
            }
        }

        public Infeed()
        {
            TagModelPathDictionary = new()
            {
                { mStatusTagModel, nameof(Status)}
            };
        }
    }
}
