﻿namespace PressModule.Application.SubProcesses
{
    public class SubProcesses
    {
        public Infeed.Infeed Infeed { get; } = new Infeed.Infeed();
        public Trimming Trimming { get; } = new Trimming();
        public Forming Forming { get; } = new Forming();
        public IndexHandler IndexHandler { get; } = new IndexHandler();
        public Dambar Dambar { get; } = new Dambar();
        public WasteHandler WasteHandler { get; } = new WasteHandler();
    }
}
