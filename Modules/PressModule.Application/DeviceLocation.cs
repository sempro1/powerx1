﻿
namespace PressModule.Application
{
    public enum DeviceLocation
    {
        Infeed,
        Trim,
        WasteBin,
        Dambar,
        Form,
        Outfeed,
    }
}
