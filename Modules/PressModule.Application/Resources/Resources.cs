﻿namespace PressModule.Application.Resources
{
   public class Resources
    {
        public Trim Trim { get; } = new Trim();
        public Form Form { get; } = new Form();
        public Indexer Indexer { get; } = new Indexer();
        public VacuumCleaner VacuumCleaner { get; } = new VacuumCleaner();
    }
}
