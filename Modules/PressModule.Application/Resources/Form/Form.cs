﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class Form
    {
        private int mFormStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleFormPcInterface.ToPc.Status);

        public int Status
        {
            get => mFormStatus;
            private set
            {
                if (mFormStatus != value)
                {
                    mFormStatus = value;
                    StatusChanged?.Invoke(this, mFormStatus);
                }
            }
        }
    }
}
