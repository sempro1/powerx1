﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class VacuumCleaner
    {
        private int mVacuumCleanerStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleVacuumCleanerPcInterface.ToPc.Status);

        public int Status
        {
            get => mVacuumCleanerStatus;
            private set
            {
                if (mVacuumCleanerStatus != value)
                {
                    mVacuumCleanerStatus = value;
                    StatusChanged?.Invoke(this, mVacuumCleanerStatus);
                }
            }
        }
    }
}
