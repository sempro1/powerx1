﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class Trim
    {
        private int mTrimStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleTrimPcInterface.ToPc.Status);

        public int Status
        {
            get => mTrimStatus;
            private set
            {
                if (mTrimStatus != value)
                {
                    mTrimStatus = value;
                    StatusChanged?.Invoke(this, mTrimStatus);
                }
            }
        }
    }
}
