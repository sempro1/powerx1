﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.OpcUa;
using System;

namespace PressModule.Application.Resources
{
   public class Indexer
    {
        private int mIndexerStatus;

        public event EventHandler<int> StatusChanged;
        public TagModel StatusTagModel => PressModuleOpcUa.GetTagModel(x => x.PressModule.PressModuleIndexerPcInterface.ToPc.Status);

        public int Status
        {
            get => mIndexerStatus;
            private set
            {
                if (mIndexerStatus != value)
                {
                    mIndexerStatus = value;
                    StatusChanged?.Invoke(this, mIndexerStatus);
                }
            }
        }
    }
}
