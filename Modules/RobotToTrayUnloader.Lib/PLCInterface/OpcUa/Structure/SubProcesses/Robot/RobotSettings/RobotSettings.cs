﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class RobotSettings
    {
        [Name("robot")]
        public Robot Robot { get; set; }

        [Name("vacuum")]
        public Vacuum Vacuum { get; set; }

        [Name("trays")]
        public Trays Trays { get; set; }
    }
}
