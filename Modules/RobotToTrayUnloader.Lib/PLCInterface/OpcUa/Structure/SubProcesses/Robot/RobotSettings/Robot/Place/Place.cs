﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class Place
    {
        [Name("pocketOffsetX")]
        public double PocketOffSetX { get; set; }

        [Name("pocketOffsetY")]
        public double PocketOffsetY { get; set; }

        [Name("trayHeight")]
        public double TrayHeight { get; set; }

        [Name("pocketDepth")]
        public double PocketDepth { get; set; }

        [Name("trayToFirstPocketOffsetX")]
        public double TrayToFirstPocketOffsetX { get; set; }

        [Name("trayToFirstPocketOffsetY")]
        public double TrayToFirstPocketOffsetY { get; set; }

        [Name("deviceSizeZ")]
        public double DeviceSizeZ { get; set; }

        [Name("placeOffsetZ")]
        public double PlaceOffsetZ { get; set; }

        [Name("velocityPercentage")]
        public VelocityPercentage VelocityPercentage { get; set; }
    }

}
