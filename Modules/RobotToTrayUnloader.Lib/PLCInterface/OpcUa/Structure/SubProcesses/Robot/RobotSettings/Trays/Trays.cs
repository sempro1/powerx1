﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class Trays
    {
        [Name("trayType1")]
        public int TrayType1 { get; set; }

        [Name("trayType2")]
        public int TrayType2 { get; set; }

        [Name("trayType3")]
        public int TrayType3 { get; set; }

        [Name("trayType4")]
        public int TrayType4 { get; set; }

        [Name("trayType5")]
        public int TrayType5 { get; set; }

        [Name("trayType6")]
        public int TrayType6 { get; set; }
    }
}
