﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
    public class VelocityPercentage
    {
       [Name("approachVelocityPercentage")]
       public int ApproachVelocityPercentage { get; set; }

       [Name("clearVelocityPercentage")]
       public int ClearVelocityPercentage { get; set; }
    }
}