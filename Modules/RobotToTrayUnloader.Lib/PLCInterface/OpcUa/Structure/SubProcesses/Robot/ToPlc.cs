﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Robot
{
   public class ToPlc
    {
        [Name("robotSettings")]
        public RobotSettings RobotSettings { get; set; }
    }
}
