﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class Inspection
    {        
            [Name("infeedPosition")]
            public double InfeedPosition { get; set; }

            [Name("outfeedPosition")]
            public double OutfeedPosition { get; set; }

            [Name("startInspectionPosition")]
            public double StartInspectionPosition { get; set; }

            [Name("velocity")]
            public double Velocity { get; set; }

            [Name("noAOI")]
            public bool NoAoi { get; set; }
                
    }
}
