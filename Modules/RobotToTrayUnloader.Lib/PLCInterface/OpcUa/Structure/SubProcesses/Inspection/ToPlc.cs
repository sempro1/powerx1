﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Inspection
{
    public class ToPlc
    {
        [Name("inspection")]
        public Inspection Inspection { get; set; }

    }
}
