﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.ScanningStage
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
