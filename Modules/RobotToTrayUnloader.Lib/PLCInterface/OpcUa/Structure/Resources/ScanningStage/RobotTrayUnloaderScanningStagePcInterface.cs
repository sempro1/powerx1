﻿using MachineManager.Lib;
using System;
using System.Collections.Generic;
namespace PressModule.PLCInterfaces.ScanningStage
{
    public class RobotTrayUnloaderScanningStagePcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
