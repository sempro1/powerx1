﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Main
{
   public class RobotToTrayUnloaderPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
