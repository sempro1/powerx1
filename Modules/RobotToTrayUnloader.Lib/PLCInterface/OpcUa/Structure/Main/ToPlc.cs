﻿using MachineManager.Lib;

namespace RobotToTrayUnloader.PLCInterface.Main
{
   public class ToPlc
    {
        [Name("initializeCommand")]
        public bool InitializeCommand { get; set; }

        [Name("startCommand")]
        public bool StartCommand { get; set; }

        [Name("stopCommand")]
        public bool StopCommand { get; set; }

        [Name("inspectCommand")]
        public bool InspectCommand { get; set; }

        [Name("inspectToInfeedCommand")]
        public bool InspectToInfeedCommand { get; set; }

        [Name("pickCommand")]
        public bool PickCommand { get; set; }

        [Name("placeCommand")]
        public bool PlaceCommand { get; set; }

        [Name("homeCommand")]
        public bool HomeCommand { get; set; }

        [Name("enableSimulation")]
        public bool EnableSimulation { get; set; }

        [Name("velocityPercentage")]
        public int VelocityPercentage { get; set; }
    }
}
