﻿
using MachineManager;

namespace RobotToTrayUnloader.PLCInterfaces.OpcUa
{
    public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceForModule(nameof(RobotToTrayUnloader));
        public RobotToTrayUnloader RobotToTrayUnloader { get; set; }
    }
}
