﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace RobotTrayUnloader.PLCInterface
{
   public class RobotToTrayUnloaderMaterialTracking
    {
        [Name("infeedDevicePosition")]
        public DeviceLocation InfeedDevicePosition { get; set; }

        [Name("robotDevicePosition")]
        public DeviceLocation RobotDevicePosition { get; set; }
        [Name("trayLocation")]
        public TrayLocation TrayLocation { get; set; }

    }
}
