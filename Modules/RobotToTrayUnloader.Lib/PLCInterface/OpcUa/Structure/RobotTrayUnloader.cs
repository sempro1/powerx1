﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.ScanningStage;
using RobotToTrayUnloader.PLCInterface.Inspection;
using RobotToTrayUnloader.PLCInterface.Main;
using RobotToTrayUnloader.PLCInterface.Robot;
using RobotTrayUnloader.PLCInterface;

namespace RobotToTrayUnloader.PLCInterfaces.OpcUa
{
    public class RobotToTrayUnloader
    {
        [Name("robotToTrayUnloaderPcInterface")]
        public RobotToTrayUnloaderPcInterface RobotToTrayUnloaderPcInterface { get; set; }

        [Name("inspectionProcessPcInterface")]
        public InspectionProcessPcInterface InspectionProcessPcInterface { get; set; }

        [Name("robotProcessPcInterface")]
        public RobotProcessPcInterface RobotProcessPcInterface { get; set; }

        [Name("robotTrayUnloaderScanningStagePcInterface")]
        public RobotTrayUnloaderScanningStagePcInterface RobotTrayUnloaderScanningStagePcInterface { get; set; }

        [Name("robotToTrayUnloaderMaterialTracking")]
        public RobotToTrayUnloaderMaterialTracking RobotToTrayUnloaderMaterialTracking { get; set; }

    }
}
