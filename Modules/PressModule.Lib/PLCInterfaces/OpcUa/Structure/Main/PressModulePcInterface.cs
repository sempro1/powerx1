﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Main
{
   public class PressModulePcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
