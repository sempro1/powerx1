﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Main
{
    public class ToPlc
    {
        [Name("initializeCommand")]
        public bool InitializeCommand { get; set; }

        [Name("startCommand")]
        public bool StartCommand { get; set; }

        [Name("stopCommand")]
        public bool StopCommand { get; set; }

        [Name("infeedCommand")]
        public bool InfeedCommand { get; set; }

        [Name("trimCommand")]
        public bool TrimCommand { get; set; }   
        public bool FormCommand { get; set; }

        [Name("indexPickCommand")]
        public bool IndexPickCommand { get; set; }

        [Name("indexPlaceCommand")]
        public bool IndexPlaceCommand { get; set; }

        [Name("dambarCommand")]
        public bool DambarCommand { get; set; }

        [Name("enableSimulation")]
        public bool EnableSimulation { get; set; }

        [Name("velocityPercentage")]
        public int VelocityPercentage { get; set; }
    }
}
