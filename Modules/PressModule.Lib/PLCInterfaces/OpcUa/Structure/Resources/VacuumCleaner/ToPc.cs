﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.VacuumCleaner
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
