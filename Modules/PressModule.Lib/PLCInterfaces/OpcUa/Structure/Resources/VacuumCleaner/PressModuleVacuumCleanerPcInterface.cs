﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.VacuumCleaner
{
   public class PressModuleVacuumCleanerPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
