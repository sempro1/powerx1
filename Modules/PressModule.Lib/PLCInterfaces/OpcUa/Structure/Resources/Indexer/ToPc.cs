﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
