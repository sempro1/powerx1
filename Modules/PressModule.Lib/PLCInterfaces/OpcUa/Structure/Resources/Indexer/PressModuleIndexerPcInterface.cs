﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Indexer
{
   public class PressModuleIndexerPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
