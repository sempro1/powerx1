﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Form
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
