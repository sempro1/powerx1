﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trim
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
