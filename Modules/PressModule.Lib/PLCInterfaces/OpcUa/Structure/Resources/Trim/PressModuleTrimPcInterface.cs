﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trim
{
   public class PressModuleTrimPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
