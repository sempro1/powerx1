﻿
namespace PressModule.PLCInterfaces.MaterialTracking
{
    public class PressModuleMaterialTracking
    {
        public PressModuleDeviceLocations PressModuleDeviceLocations { get; set;}
        public PressModuleIndexerDeviceLocations PressModuleIndexerDeviceLocations { get; set; }
    }
}
