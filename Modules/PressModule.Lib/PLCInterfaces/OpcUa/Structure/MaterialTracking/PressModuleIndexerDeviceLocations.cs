﻿using MachineManager.Lib;
using MaterialTracking.Lib;

namespace PressModule.PLCInterfaces.MaterialTracking
{
   public class PressModuleIndexerDeviceLocations
    {
        [Name("[0]")]
        public DeviceLocation DeviceLocation0 { get; set; }
        [Name("[1]")]
        public DeviceLocation DeviceLocation1 { get; set; }
        [Name("[2]")]
        public DeviceLocation DeviceLocation2 { get; set; }
        [Name("[3]")]
        public DeviceLocation DeviceLocation3 { get; set; }
        [Name("[4]")]
        public DeviceLocation DeviceLocation4 { get; set; }
    }
}
