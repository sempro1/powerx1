﻿using MachineManager;

namespace PressModule.PLCInterfaces.OpcUa
{
   public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceForModule(nameof(PressModule));
        public PressModule PressModule { get; set; }
    }
}
