﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.WasteHandler
{
   public class WasteHandler
    {
        [Name("almostMaximumWasteCount")]
        public int AlmostMaximumWasteCount { get; set; }

        [Name("maximumWasteCount")]
        public int MaximumWasteCount { get; set; }
    }
}
