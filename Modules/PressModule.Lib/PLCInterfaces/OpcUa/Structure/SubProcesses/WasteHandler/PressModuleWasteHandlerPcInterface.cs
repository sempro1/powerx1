﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.WasteHandler
{
    public class PressModuleWasteHandlerPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
                
    }
}
