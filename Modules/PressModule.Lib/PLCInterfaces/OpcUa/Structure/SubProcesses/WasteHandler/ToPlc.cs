﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.WasteHandler
{
    public class ToPlc
    {
        [Name("wasteHandler")]
        public WasteHandler WasteHandler { get; set; }

        [Name("resetCounter")]
        public bool ResetCounter { get; set; }
    }
}
