﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Infeed
{ 
   public class PressModuleInfeedPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
