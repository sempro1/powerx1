﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.IndexHandler
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
