﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
   public class ToPlc
    {
        [Name("forming")]
        public Forming Forming { get; set; }
    }
}
