﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
    public class PressModuleFormingPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }

    }
}
