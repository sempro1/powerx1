﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
   public class Forming
    {
        [Name("topPosition")]
        public double TopPosition { get; set; }
        [Name("sensorPosition")]
        public double SensorPosition { get; set; }
        [Name("closePosition")]
        public double ClosePosition { get; set; }
        [Name("removePosition")]
        public double RemovePosition { get; set; }
        [Name("maintenancePosition")]
        public double MaintenacePosition { get; set; }
        [Name("velocity")]
        public double Velocity { get; set; }
    }
}
