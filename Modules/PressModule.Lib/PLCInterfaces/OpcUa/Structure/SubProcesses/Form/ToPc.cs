﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Forming
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
