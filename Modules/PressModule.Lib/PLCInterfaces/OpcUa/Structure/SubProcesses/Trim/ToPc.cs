﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trimming
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
