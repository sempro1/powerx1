﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Trimming
{
   public class ToPlc
    {
        [Name("trimming")]
        public Trimming Trimming { get; set; }
    }
}
