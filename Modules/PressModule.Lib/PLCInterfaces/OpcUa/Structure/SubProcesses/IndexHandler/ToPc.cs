﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Infeed
{
   public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
