﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.IndexHandler
{ 
   public class PressModuleIndexHandlerPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
