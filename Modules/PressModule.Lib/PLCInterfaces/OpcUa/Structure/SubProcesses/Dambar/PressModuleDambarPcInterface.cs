﻿using MachineManager.Lib;

namespace PressModule.PLCInterfaces.Dambar
{ 
   public class PressModuleDambarPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }
    }
}
