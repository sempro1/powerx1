﻿using MachineManager.Lib;
using PressModule.PLCInterfaces.Main;
using PressModule.PLCInterfaces.Infeed;
using PressModule.PLCInterfaces.Trimming;
using PressModule.PLCInterfaces.Forming;
using PressModule.PLCInterfaces.IndexHandler;
using PressModule.PLCInterfaces.Dambar;
using PressModule.PLCInterfaces.WasteHandler;
using PressModule.PLCInterfaces.MaterialTracking;
using PressModule.PLCInterfaces.Trim;
using PressModule.PLCInterfaces.Form;
using PressModule.PLCInterfaces.Indexer;
using PressModule.PLCInterfaces.VacuumCleaner;

namespace PressModule.PLCInterfaces
{
    public class PressModule
    {
        [Name("pressModulePcInterface")]
        public PressModulePcInterface PressModulePcInterface { get; set; }

        [Name("pressModuleInfeedPcInterface")]
        public PressModuleInfeedPcInterface PressModuleInfeedPcInterface { get; set; }

        [Name("pressModuleTrimmingPcInterface")]
        public PressModuleTrimmingPcInterface PressModuleTrimmingPcInterface { get; set; }

        [Name("pressModuleFormingPcInterface")]
        public PressModuleFormingPcInterface PressModuleFormingPcInterface { get; set; }

        [Name("pressModuleIndexHandlerPcInterface")]
        public PressModuleIndexHandlerPcInterface PressModuleIndexHandlerPcInterface { get; set; }

        [Name("pressModuleDambarPcInterface")]
        public PressModuleDambarPcInterface PressModuleDambarPcInterface { get; set; }

        [Name("pressModuleWasteHandlerPcInterface")]
        public PressModuleWasteHandlerPcInterface PressModuleWasteHandlerPcInterface { get; set; }

        [Name("pressModuleTrimPcInterface")]
        public PressModuleTrimPcInterface PressModuleTrimPcInterface { get; set; }

        [Name("pressModuleFormPcInterface")]
        public PressModuleFormPcInterface PressModuleFormPcInterface { get; set; }

        [Name("pressModuleIndexerPcInterface")]
        public PressModuleIndexerPcInterface PressModuleIndexerPcInterface { get; set; }

        [Name("pressModuleVacuumCleanerPcInterface")]
        public PressModuleVacuumCleanerPcInterface PressModuleVacuumCleanerPcInterface { get; set; }
        public PressModuleMaterialTracking PressModuleMaterialTracking { get; set; }
    }
}
