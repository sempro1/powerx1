﻿using System.Collections.Generic;
using EventHandler.Lib;

namespace MagazineLoader.Lib
{
    public class MagazineLoaderEvents
    {
        private const string cModuleName = "MagazineLoader";
        private const string cLoadMagazineProcessName = "Load magazine";
        private const string cProvideMaterialProcessName = "Provide material";
        private const string cUnloadMagazineProcessName = "Unload magazine";
             
        //Load
        private const string cLoadMagazinePLCEvents = "instLoadMagazineEvents.";
        private const string cMagazineNotPresentInstance = cLoadMagazinePLCEvents + "instMagazineNotPresentError";
        private const string cMagazineNotPresentAtUpperConveyorInstance = cLoadMagazinePLCEvents + "instNoMoreMagazinesAtUpperConveyor";
        private const string cMagazineOrientationWarningInstance = cLoadMagazinePLCEvents + "instMagazineOrientationNotOkWarning";
        private const string cMagazineOrientationErrorInstance = cLoadMagazinePLCEvents + "instMagazineOrientationNotOkError";
        private const string cUpperConveyorAlmostEmptyInstance = cLoadMagazinePLCEvents + "instUpperConveyorAlmostEmpty";
        private const string cNoMoreMagazinesAtUpperConveyorInstance = cLoadMagazinePLCEvents + "instNoMoreMagazinesAtUpperConveyor";
        

        //Provide
        private const string cProvideMaterialPLCEvents = "instProvideMaterialEvents.";
        private const string cNoProductAtTransferInstance = cProvideMaterialPLCEvents + "instNoLeadFrameAtOutputAssist";

        //Unload
        private const string cUnloadMagazinePLCEvents = "instUnloadMagazineEvents.";
        private const string cLowerConveyorFullWarningInstance = cUnloadMagazinePLCEvents + "instLowerConveyorFullWarning";
        private const string cLowerConveyorFullErrorInstance = cUnloadMagazinePLCEvents + "instLowerConveyorFullError";

        public static Event MagazineNotPresent = new(100, "Magazine not present at lift", EventType.Error, cLoadMagazineProcessName, cModuleName, cMagazineNotPresentInstance, $"Events/{cModuleName}/MagazineNotPresent");
        public static Event NoProductAtTransfer = new(101, "No product at transfer detected", EventType.Assist, cProvideMaterialProcessName, cModuleName, cNoProductAtTransferInstance, $"Events/{cModuleName}/NoProductAtTransfer");
        public static Event LowerConveyorFullError = new(102, "Lower conveyor full", EventType.Error, cUnloadMagazineProcessName, cModuleName, cLowerConveyorFullWarningInstance, $"Events/{cModuleName}/LowerConveyorFullError");
        public static Event LowerConveyorFullWarning = new(103, "Lower conveyor almost full", EventType.Warning, cUnloadMagazineProcessName, cModuleName, cLowerConveyorFullErrorInstance, $"Events/{cModuleName}/LowerConveyorFullWarning");
        public static Event MagazineOrientationWarning = new(104, "Magazine orientation is not ok", EventType.Warning, cLoadMagazineProcessName, cModuleName, cMagazineOrientationWarningInstance, $"Events/{cModuleName}/MagazineOrientationWarning");
        public static Event MagazineOrientationError = new(105, "Magazine orientation is not ok", EventType.Error, cLoadMagazineProcessName, cModuleName, cMagazineOrientationErrorInstance, $"Events/{cModuleName}/MagazineOrientationError");
        public static Event MagazineLoaderInSimulation = new(106, "Magazine module in simulation", EventType.Info, cLoadMagazineProcessName, cModuleName, cMagazineNotPresentAtUpperConveyorInstance, $"Events/{cModuleName}/MagazineNotPresentAtUpperConveyor");
        public static Event UpperConveyorAlmostEmpty = new(108, "Upper conveyor is almost empty", EventType.Info, cLoadMagazineProcessName, cModuleName, cUpperConveyorAlmostEmptyInstance, $"Events/{cModuleName}/UpperConveyorAlmostEmpty");
        public static Event NoMoreMagazinesAtUpperConveyor = new(109, "Magazine not present at upper conveyor", EventType.Info, cLoadMagazineProcessName, cModuleName, cNoMoreMagazinesAtUpperConveyorInstance, $"Events/{cModuleName}/NoMoreMagazinesAtUpperConveyor");
        public static IEnumerable<Event> Events = new List<Event>
        {
            MagazineNotPresent,
            NoProductAtTransfer,
            LowerConveyorFullError,
            LowerConveyorFullWarning,
            MagazineOrientationWarning,
            MagazineOrientationError,
            MagazineLoaderInSimulation,
        };
    }
}
