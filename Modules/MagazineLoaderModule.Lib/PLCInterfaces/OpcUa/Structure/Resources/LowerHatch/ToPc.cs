﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerHatch
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
