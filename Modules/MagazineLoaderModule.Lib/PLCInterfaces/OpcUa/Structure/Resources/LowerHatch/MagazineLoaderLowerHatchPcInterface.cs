﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerHatch
{
    public class MagazineLoaderLowerHatchPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
