﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LeadFramePusher
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
