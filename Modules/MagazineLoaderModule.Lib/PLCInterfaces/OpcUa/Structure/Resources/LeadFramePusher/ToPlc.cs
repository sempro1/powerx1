﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LeadFramePusher
{
    public class ToPlc
    {
        [Name("pushCommand")]
        public bool PushCommand { get; set; }

        [Name("retrieveCommand")]
        public bool RetrieveCommand { get; set; }
    }
}
