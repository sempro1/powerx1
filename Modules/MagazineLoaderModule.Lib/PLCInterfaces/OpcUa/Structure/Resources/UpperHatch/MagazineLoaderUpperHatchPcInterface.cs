﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperHatch
{
    public class MagazineLoaderUpperHatchPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
