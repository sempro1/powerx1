﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperHatch
{
    public class ToPlc
    {
        [Name("openCommand")]
        public bool OpenCommand { get; set; }

        [Name("closeCommand")]
        public bool CloseCommand { get; set; }
    }
}
