﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperHatch
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
