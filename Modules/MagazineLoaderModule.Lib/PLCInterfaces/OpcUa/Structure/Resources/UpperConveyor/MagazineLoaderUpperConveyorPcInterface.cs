﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperConveyor
{
    public class MagazineLoaderUpperConveyorPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
