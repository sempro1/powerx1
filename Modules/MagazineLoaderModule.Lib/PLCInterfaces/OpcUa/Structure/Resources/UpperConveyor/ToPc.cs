﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.UpperConveyor
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
