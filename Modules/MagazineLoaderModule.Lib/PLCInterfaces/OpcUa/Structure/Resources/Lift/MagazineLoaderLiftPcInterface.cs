﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Lift
{
    public class MagazineLoaderLiftPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
