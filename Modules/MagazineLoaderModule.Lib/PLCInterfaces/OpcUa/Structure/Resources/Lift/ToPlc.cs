﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Lift
{
    public class ToPlc
    {
        public bool GoToCommand { get; set; }
        public double SetPointY { get; set; }
        public double SetPointZ { get; set; }       
        public bool ClampCommand { get; set; }   
        
        [Name("unClampCommand")]
        public bool UnClampCommand { get; set; }
    }
}
