﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Lift
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
