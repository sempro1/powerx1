﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerConveyor
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}
