﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LowerConveyor
{
    public class MagazineLoaderLowerConveyorPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
