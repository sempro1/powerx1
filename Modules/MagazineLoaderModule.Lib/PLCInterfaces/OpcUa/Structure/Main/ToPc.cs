﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class ToPc
    {
        public int Status { get; set; }
        public int State { get; set; }
        [Name("isRunningProduction")]
        public bool IsRunningProduction { get; set; }
        [Name("inSimulation")]
        public bool InSimulation { get; set; }
        [Name("inDryRun")]
        public bool InDryRun { get; set; }
    }
}