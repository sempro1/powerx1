﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class ToPlc
    {
        public bool InitializeCommand { get; set; }

        public bool StartCommand { get; set; }

        public bool StopCommand { get; set; }

        public bool LoadMagazineCommand { get; set; }

        public bool ProvideLeadFramesCommand { get; set; }

        [Name("unloadMagazineCommand")]
        public bool UnloadMagazineCommand { get; set; }

        [Name("enableSimulation")]
        public bool EnableSimulation { get; set; }

        [Name("velocityPercentage")]
        public int VelocityPercentage { get; set; }
    }
}