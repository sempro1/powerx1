﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class MagazineLoaderModulePcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set;}

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}