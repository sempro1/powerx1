﻿using MachineManager;

namespace MagazineLoader.PLCInterfaces.OpcUa
{
    public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceForModule(nameof(MagazineLoader));
        public MagazineLoader MagazineLoader { get; set; }
    }
}
