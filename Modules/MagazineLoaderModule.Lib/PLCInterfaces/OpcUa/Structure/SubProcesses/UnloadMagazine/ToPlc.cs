﻿using MachineManager;

namespace MagazineLoader.PLCInterfaces.UnloadMagazine
{
    public class ToPlc
    {
        public Unload Unload { get; set; }
    }
}