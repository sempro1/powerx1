﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LoadMagazine
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}