﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LoadMagazine
{
    public class Load
    {
        [Name("upperConveyorZ")]
        public double UpperConveyorZ { get; set; }

        [Name("slotcount")]
        public int SlotCount { get; set; }

        [Name("startPickOffsetZ")]
        public double StartPickOffsetZ { get; set; }

        [Name("endPickOffsetZ")]
        public double EndPickOffsetZ { get; set; }

        [Name("pickY")]
        public double PickY { get; set; }

        [Name("magazinePositionLiftSafePositionY")]
        public double MagazinePositionLiftSafePositionY { get; set; }
       
    }
}