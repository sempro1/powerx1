﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.LoadMagazine
{
    public class MagazineLoaderLoadMagazinePCInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}