﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.ProvideMaterial
{
    public class Provide
    {
        [Name("magazineSlotOffset")]
        public double MagazineSlotOffset { get; set; }

        [Name("magazineBottumSlotOffset")]
        public double MagazineBottomSlotOffset { get; set; }

        [Name("deviceCountPerLeadFrame")]
        public double DeviceCountPerLeadFrame { get; set; }

        [Name("magazineLiftStartPositionZ")]
        public double MagazineLiftStartPositionZ { get; set; }

        [Name("magazineLiftStartPositionY")]
        public double MagazineLiftStartPositionY { get; set; }

        [Name("magazineWidth")]
        public double MagazineWidth { get; set; }
    }
}