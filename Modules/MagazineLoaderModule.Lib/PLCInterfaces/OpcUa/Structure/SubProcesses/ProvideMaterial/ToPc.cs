﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.ProvideMaterial
{
    public class ToPc
    {
        [Name("status")]
        public int Status { get; set; }
    }
}