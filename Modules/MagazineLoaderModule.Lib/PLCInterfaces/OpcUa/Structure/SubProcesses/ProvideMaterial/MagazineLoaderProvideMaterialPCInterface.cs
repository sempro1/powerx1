﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.ProvideMaterial
{
    public class MagazineLoaderProvideMaterialPCInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}