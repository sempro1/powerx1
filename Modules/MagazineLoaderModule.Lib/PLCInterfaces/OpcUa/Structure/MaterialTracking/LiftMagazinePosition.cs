﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class LiftMagazinePosition
    {
        [Name("hasMaterial")]
        public bool HasMaterial { get; set; }

        [Name("magazine")]
        public Magazine Magazine { get; set; }
    }
}