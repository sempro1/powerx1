﻿using MachineManager.Lib;

namespace MagazineLoader.PLCInterfaces.Main
{
    public class UpperConveyorMagazinePosition
    {
        [Name("hasMaterial")]
        public bool HasMaterial { get; set; }
    }
}