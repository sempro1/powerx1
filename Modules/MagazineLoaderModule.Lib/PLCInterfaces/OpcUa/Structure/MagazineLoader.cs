﻿using MachineManager.Lib;
using MagazineLoader.PLCInterfaces.LeadFramePusher;
using MagazineLoader.PLCInterfaces.Lift;
using MagazineLoader.PLCInterfaces.LoadMagazine;
using MagazineLoader.PLCInterfaces.LowerConveyor;
using MagazineLoader.PLCInterfaces.LowerHatch;
using MagazineLoader.PLCInterfaces.Main;
using MagazineLoader.PLCInterfaces.ProvideMaterial;
using MagazineLoader.PLCInterfaces.UnloadMagazine;
using MagazineLoader.PLCInterfaces.UpperConveyor;
using MagazineLoader.PLCInterfaces.UpperHatch;

namespace MagazineLoader.PLCInterfaces
{
    public class MagazineLoader
    {
        [Name("magazineLoaderModulePcInterface")]
        public MagazineLoaderModulePcInterface MagazineLoaderModulePcInterface { get; set; }

        [Name("magazineLoaderLoadMagazinePCInterface")]
        public MagazineLoaderLoadMagazinePCInterface MagazineLoaderLoadMagazinePCInterface { get; set; }

        [Name("magazineLoaderProvideMaterialPCInterface")]
        public MagazineLoaderProvideMaterialPCInterface MagazineLoaderProvideMaterialPCInterface { get; set; }

        [Name("magazineLoaderUnloadMagazinePCInterface")]
        public MagazineLoaderUnloadMagazinePCInterface MagazineLoaderUnloadMagazinePCInterface { get; set; }

        [Name("magazineLoaderUpperConveyorPcInterface")]
        public MagazineLoaderUpperConveyorPcInterface MagazineLoaderUpperConveyorPcInterface { get; set; }

        [Name("magazineLoaderUpperHatchPcInterface")]
        public MagazineLoaderUpperHatchPcInterface MagazineLoaderUpperHatchPcInterface { get; set; }

        [Name("magazineLoaderLiftPcInterface")]
        public MagazineLoaderLiftPcInterface MagazineLoaderLiftPcInterface { get; set; }

        [Name("magazineLoaderLeadFramePusherPcInterface")]
        public MagazineLoaderLeadFramePusherPcInterface MagazineLoaderLeadFramePusherPcInterface { get; set; }

        [Name("magazineLoaderLowerConveyorPcInterface")]
        public MagazineLoaderLowerConveyorPcInterface MagazineLoaderLowerConveyorPcInterface { get; set; }

        [Name("magazineLoaderLowerHatchPcInterface")]
        public MagazineLoaderLowerHatchPcInterface MagazineLoaderLowerHatchPcInterface { get; set; }

        [Name("magazineLoaderMaterialTracking")]
        public MagazineLoaderMaterialTracking MaterialTracking { get; set; }
    }
}
