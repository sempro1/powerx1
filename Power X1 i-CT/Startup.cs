using LineControl;
using log4net;
using Logging;
using LotControl;
using MagazineLoader;
using MaterialTracking;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Power_X1_iCT;
using Power_X1_ICT.Data;
using PressModule.Updater;
using RobotToTrayUnloader.Updater;
using System;

namespace Power_X1_ICT
{
    public class Startup
    {
        private MagazineLoader.Application.MagazineLoaderModule mMagazineLoaderModule;
        private PressModule.Application.PressModule mPressModule;
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloaderModule;

        private readonly ILog mLog = LogClient.Get();
        public Startup(IConfiguration configuration)
        {
            mLog.Info($"Startup: {DateTime.Now}");
            Configuration = configuration;
            mMagazineLoaderModule = MagazineLoader.Application.MagazineLoaderModule.Instance;
            mPressModule = PressModule.Application.PressModule.Instance;
            mRobotToTrayUnloaderModule = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddRazorPages();
            services.AddSignalR();
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });
            services.AddSingleton<LineControlUpdater>();
            services.AddSingleton<MagazineLoaderUpdater>();
            services.AddSingleton<PressModuleUpdater>();
            services.AddSingleton<RobotToTrayUnloaderUpdater>();
            services.AddSingleton<MaterialTrackingUpdater>();
            services.AddSingleton<EventManagerUpdater>();
            services.AddSingleton<LotControlUpdater>();
            //log4net.Util.LogLog.InternalDebugging = true;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
            //Re-outcomment below code to not show exceptions on published weppage
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            app.UseMigrationsEndPoint();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            //var cookiePolicyOptions = new CookiePolicyOptions
            //{
            //    MinimumSameSitePolicy = SameSiteMode.Strict,
            //};
            //app.UseCookiePolicy(cookiePolicyOptions);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapHub<PowerX1iCTHub>(PowerX1iCTHub.Pattern);
            });

            app.Use(async (context, next) =>
            {
                if (next != null)
                {
                    await next.Invoke();
                }
            });
        }
    }
}
