﻿using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;

namespace Power_X1_iCT.Model
{
    public class InspectionSettingsModel
    {
        private Inspection mInspection = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Inspection;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double InfeedPosition { get; set; }
        public double OutfeedPosition { get; set; }
        public double StartInspectionPosition { get; set; }
        public double Velocity { get; set; }
        public bool NoAOI { get; set; }

        public InspectionSettingsModel()
        {
            Inspection_SettingsChanged(this, mInspection.Settings);
            mInspection.SettingsChanged += Inspection_SettingsChanged;
        }

        private void Inspection_SettingsChanged(object sender, RobotToTrayUnloader.Application.SubProcesses.InspectionDataModel e)
        {
            InfeedPosition = e.InfeedPosition;
            OutfeedPosition = e.OutfeedPosition;
            StartInspectionPosition = e.StartInspectionPosition;
            Velocity = e.Velocity;
            NoAOI = e.NoAOI;
        }
        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.Inspection.InfeedPosition = InfeedPosition;
            productRecipe.Inspection.OutfeedPosition = OutfeedPosition;
            productRecipe.Inspection.StartInspectionPosition = StartInspectionPosition;
            productRecipe.Inspection.Velocity = Velocity;
            productRecipe.Inspection.NoAOI = NoAOI;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();

        }
        public void Download()
        {
            var dataModel = new RobotToTrayUnloader.Application.SubProcesses.InspectionDataModel();
            dataModel.InfeedPosition = InfeedPosition;
            dataModel.OutfeedPosition = OutfeedPosition;
            dataModel.StartInspectionPosition = StartInspectionPosition;
            dataModel.Velocity = Velocity;
            dataModel.NoAOI = NoAOI;

            mInspection.UpdateSettings(dataModel);
        }
    }
}
