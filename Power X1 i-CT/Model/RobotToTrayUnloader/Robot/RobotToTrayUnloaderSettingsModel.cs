﻿using RobotTrayUnloader.Application;

namespace Power_X1_iCT.Model
{
    public class RobotToTrayUnloaderSettingsModel
    {
        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;
        private int mVelocityPercentage;

        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                if (mVelocityPercentage != value)
                {
                    mVelocityPercentage = value;
                }
            }
        }

        public RobotToTrayUnloaderSettingsModel()
        {
            RobotToTrayUnloader_SettingsChanged(this, mRobotToTrayUnloader.Settings);
            mRobotToTrayUnloader.SettingsChanged += RobotToTrayUnloader_SettingsChanged;
        }

        private void RobotToTrayUnloader_SettingsChanged(object sender, RobotToTrayUnloaderDataModel e)
        {
            VelocityPercentage = e.VelocityPercentage;
        }

        public void SetVelocityPercentage(int percentage)
        {
            mRobotToTrayUnloader.SetVelocityPercentage(percentage);
        }
    }
}
