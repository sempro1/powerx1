﻿using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;

namespace Power_X1_iCT.Model
{
    public class RobotPickSettingsModel
    {
        private Robot mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Robot;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }
        public double PositionR { get; set; }
        public double PickOffsetZ { get; set; }
        public int PickApproachVelocityPercentage { get; set; }
        public int PickClearVelocityPercentage { get; set; }
        

        public RobotPickSettingsModel()
        {
            RobotPick_settingsChanged(this, mRobot.SettingsPick);
            mRobot.SettingsPickChanged += RobotPick_settingsChanged;
        }

        private void RobotPick_settingsChanged(object sender, RobotPickDataModel e)
        {
            PositionX = e.PickPosition.X;
            PositionY = e.PickPosition.Y;
            PositionZ = e.PickPosition.Z;
            PositionR = e.PickPosition.R;
            PickOffsetZ = e.PickOffsetZ;
            PickApproachVelocityPercentage = e.PickVelocityPercentage.ApproachVelocityPercentage;
            PickClearVelocityPercentage = e.PickVelocityPercentage.ClearVelocityPercentage;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.RobotProcess.Pick.Position.X = PositionX;
            productRecipe.RobotProcess.Pick.Position.Y = PositionY;
            productRecipe.RobotProcess.Pick.Position.Z = PositionZ;
            productRecipe.RobotProcess.Pick.Position.R = PositionR;
            productRecipe.RobotProcess.Pick.PickOffsetZ = PickOffsetZ;
            productRecipe.RobotProcess.Pick.VelocityPercentage.ApproachVelocityPercentage = PickApproachVelocityPercentage;
            productRecipe.RobotProcess.Pick.VelocityPercentage.ClearVelocityPercentage = PickClearVelocityPercentage;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotPickDataModel();
            dataModel.PickPosition.X = PositionX;
            dataModel.PickPosition.Y = PositionY;
            dataModel.PickPosition.Z = PositionZ;
            dataModel.PickPosition.R = PositionR;
            dataModel.PickOffsetZ = PickOffsetZ;
            dataModel.PickVelocityPercentage.ApproachVelocityPercentage = PickApproachVelocityPercentage;
            dataModel.PickVelocityPercentage.ClearVelocityPercentage = PickClearVelocityPercentage;           

            mRobot.UpdateRobotPickSettings(dataModel);
        }

    }
}
