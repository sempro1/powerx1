﻿using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;

namespace Power_X1_iCT.Model
{
    public class RobotPlaceSettingsModel
    {
        private Robot mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Robot;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public double PlaceOffsetZ { get; set; }
        public int PlaceApproachVelocityPercentage { get; set; }
        public int PlaceClearVelocityPercentage { get; set; }        

        public RobotPlaceSettingsModel()
        {
            RobotPlace_SettingChanged(this, mRobot.SettingsPlace);
            mRobot.SettingsPlaceChanged += RobotPlace_SettingChanged;
        }

        public void RobotPlace_SettingChanged(object send, RobotPlaceDataModel e)
        {     
            PlaceOffsetZ = e.PlaceOffsetZ;
            PlaceApproachVelocityPercentage = e.PlaceVelocityPercentage.ApproachVelocityPercentage;
            PlaceClearVelocityPercentage = e.PlaceVelocityPercentage.ClearVelocityPercentage;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.RobotProcess.Place.PlaceOffsetZ = PlaceOffsetZ;
            productRecipe.RobotProcess.Place.VelocityPercentage.ApproachVelocityPercentage = PlaceApproachVelocityPercentage;
            productRecipe.RobotProcess.Place.VelocityPercentage.ClearVelocityPercentage = PlaceClearVelocityPercentage;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotPlaceDataModel();
            dataModel.PlaceOffsetZ = PlaceOffsetZ;
            dataModel.PlaceVelocityPercentage.ApproachVelocityPercentage = PlaceApproachVelocityPercentage;
            dataModel.PlaceVelocityPercentage.ClearVelocityPercentage = PlaceClearVelocityPercentage;

            mRobot.UpdateRobotPlaceSettings(dataModel);
        }

    }
}
