﻿using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;

namespace Power_X1_iCT.Model
{
    public class RobotHomeSettingsModel
    {
        private Robot mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Robot;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }
        public double PositionR { get; set; }

        public RobotHomeSettingsModel()
        {
            RobotHome_settingsChanged(this, mRobot.SettingsHome);
            mRobot.SettingsHomeChanged += RobotHome_settingsChanged;
        }

        private void RobotHome_settingsChanged(object sender, RobotHomeDataModel e)
        {
            PositionX = e.HomePosition.X;
            PositionY = e.HomePosition.Y;
            PositionZ = e.HomePosition.Z;
            PositionR = e.HomePosition.R;
        }

        public void Save()
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe();
            machineRecipe.RobotTrayUnloader.Home.Position.X = PositionX;
            machineRecipe.RobotTrayUnloader.Home.Position.Y = PositionY; 
            machineRecipe.RobotTrayUnloader.Home.Position.Z = PositionZ; 
            machineRecipe.RobotTrayUnloader.Home.Position.R = PositionR;

            mRecipeManager.SaveRecipe(machineRecipe);
            Download();
        }

        public void Download()
        {
            var dataModel = new RobotHomeDataModel();
            dataModel.HomePosition.X = PositionX;
            dataModel.HomePosition.Y = PositionY;
            dataModel.HomePosition.Z = PositionZ;
            dataModel.HomePosition.R = PositionR;

            mRobot.UpdateRobotHomeSettings(dataModel);
        }
    }
}
