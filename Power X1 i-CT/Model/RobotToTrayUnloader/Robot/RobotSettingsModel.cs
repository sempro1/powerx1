﻿using RecipeManager;
using RobotToTrayUnloader.Application.SubProcesses;

namespace Power_X1_iCT.Model
{
    public class RobotSettingsModel
    {
        private Robot mRobot = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance.SubProcesses.Robot;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public int BaseCount { get; set; }
        public int DefaultVelocityPercentage { get; set; }

        public RobotSettingsModel()
        {
            Robot_settingsChanged(this, mRobot.Settings);
            mRobot.SettingsChanged += Robot_settingsChanged;
        }

        private void Robot_settingsChanged(object sender, RobotToTrayUnloader.Application.SubProcesses.RobotDataModel e)
        {
            BaseCount = e.BaseCount;
            DefaultVelocityPercentage = e.DefaultVelocityPercentage;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.Robot.BaseCount = BaseCount;
            productRecipe.Robot.DefaultVelocityPercentage = DefaultVelocityPercentage;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new RobotDataModel();
            dataModel.BaseCount = BaseCount;
            dataModel.DefaultVelocityPercentage = DefaultVelocityPercentage;

            mRobot.UpdateSettings(dataModel);
        }
    }
}
