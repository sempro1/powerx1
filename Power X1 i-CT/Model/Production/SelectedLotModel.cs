﻿using MaterialTracking.Lib;

namespace Power_X1_iCT.Model
{
    public class SelectedLotModel
    {
        public string LotName { get; set; } = string.Empty;
        public string State { get; set; } = LotProcessState.Planned.ToString();
        public string Opened { get; set; }
        public string Closed { get; set; }
        public int DeviceCount { get; set; }
        public int LeadFrameCount { get; set; }
    }
}
