﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class AddLotModel
    {
        [Required]
        public string AddLotName { get; set; } = string.Empty;
    }
}
