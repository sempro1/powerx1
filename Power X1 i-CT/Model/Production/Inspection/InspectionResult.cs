﻿namespace Power_X1_iCT.Model
{
    public class InspectionResult
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public double? LowerLimit { get; set; } 
        public double? UpperLimit { get; set; } 
        public bool Result { get; set; }

        public InspectionResult(string name, double value, string unit)
        {
            Name = name;
            Value = value.ToString("N3");
            Unit = unit;
            Result = true;
        }

        public InspectionResult(string name, double value, string unit, double lowerLimit, double upperLimit)
        {
            Name = name;
            Value = value.ToString("N3");
            Unit = unit;
            LowerLimit = lowerLimit;
            UpperLimit = upperLimit;
            Result = value <= upperLimit && value >= LowerLimit;
        }
        public InspectionResult(string name, int value, string unit, int lowerLimit, int upperLimit)
        {
            Name = name;
            Value = value.ToString();
            Unit = unit;
            LowerLimit = lowerLimit;
            UpperLimit = upperLimit;
            Result = value <= upperLimit && value >= LowerLimit;
        }
    }
}