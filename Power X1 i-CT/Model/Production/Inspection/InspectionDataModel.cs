﻿using System.Collections.Generic;

namespace Power_X1_iCT.Model
{
    public class InspectionDataModel
    {
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public bool Result { get; set; }        
        public byte[] Image { get; set; }
        public string VisionApplication { get; set; } = string.Empty;
        public List<InspectionResult> InspectionResults { get; set; } = new();
        public InspectionDataModel(string name)
        {
            Name = name;
        }
    }

}
