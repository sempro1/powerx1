﻿using MaterialTracking.Lib;

namespace Power_X1_iCT.Model
{
    public class SelectedDeviceModel
    {
        public string LotName { get; set; } = string.Empty;
        public int Id { get; set; }
        public string State { get; set; } = DeviceProcessState.New.ToString();
        public string Created { get; set; }
        public string DambarInspection { get; set; }
        public string Inspection { get; set; }
        public string Processed { get; set; }
    }
}
