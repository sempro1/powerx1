﻿using MaterialTracking.Lib;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Power_X1_iCT.Model
{
    public class ForceLoggingProcessModel
    {
        public string Name { get; set; } = string.Empty;

        public double ProductPosition { get; set; }
        public List<DataSet> DataSets { get; set; } = new();
        public double ProductThickness { get; internal set; }

        public ForceLoggingProcessModel(string name)
        {
            Name = name;
        }

        public void AddDataSet (DataSet dataSet)
        {
            DataSets.Add(dataSet);
        }
    }

    public class DataSet
    {
        public string Name { get; set; } = string.Empty;
        public List<DataPoint> Data { get; set; } = new();

        public DataSet(string name)
        {
            Name = name;
        }       
    }

    public class DataPoint
    {
        public string x { get; set; }
        public string y { get; set; }

        public DataPoint(string aX, string aY)
        {
            x = aX;
            y = aY;
        }
    }
}