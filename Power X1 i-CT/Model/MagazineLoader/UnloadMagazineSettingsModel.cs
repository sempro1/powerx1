﻿using MagazineLoader.Application.SubProcesses;
using RecipeManager;
using System;

namespace Power_X1_iCT.Model
{
    public class UnloadMagazineSettingsModel
    {
        private UnloadMagazine mUnloadMagazine = MagazineLoader.Application.MagazineLoaderModule.Instance.SubProcesses.UnloadMagazine;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public double LowerConveyorZ { get; set; }
        public double StartPlaceOffsetZ { get; set; }
        public double PlaceY { get; set; }
        public double LiftSafePositionY { get; set; }      

        public UnloadMagazineSettingsModel()
        {
            UnloadMagazine_SettingsChanged(this, mUnloadMagazine.Settings);
            mUnloadMagazine.SettingsChanged += UnloadMagazine_SettingsChanged;
        }

        private void UnloadMagazine_SettingsChanged(Object sender, UnloadMagazineDataModel e)
        {
            LowerConveyorZ = e.LowerConveyorZ;
            StartPlaceOffsetZ = e.StartPlaceOffsetZ;
            PlaceY = e.PlaceY;
            LiftSafePositionY = e.MagazinePositionLiftSafePosition;            
        }

        public void Save()
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe();
            machineRecipe.MagazineLoader.UnloadMagazine.LowerConveyorZPosition = LowerConveyorZ;
            machineRecipe.MagazineLoader.UnloadMagazine.StartPlaceOffsetZPosition = StartPlaceOffsetZ;
            machineRecipe.MagazineLoader.UnloadMagazine.PlaceYPosition = PlaceY;
            machineRecipe.MagazineLoader.UnloadMagazine.MagazineLiftSafePositionY = LiftSafePositionY;
            mRecipeManager.SaveRecipe(machineRecipe);            

            Download();
        }

        public void Download()
        {
            var dataModel = new UnloadMagazineDataModel();
            dataModel.LowerConveyorZ = LowerConveyorZ;
            dataModel.StartPlaceOffsetZ = StartPlaceOffsetZ;
            dataModel.PlaceY = PlaceY;
            dataModel.MagazinePositionLiftSafePosition = LiftSafePositionY;

            mUnloadMagazine.UpdateSettings(dataModel);

        }
    }
}
