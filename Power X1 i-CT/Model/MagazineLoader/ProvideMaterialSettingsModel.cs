﻿using MagazineLoader.Application.SubProcesses;
using RecipeManager;
using System;
using X1.Recipe.Lib.Machine;

namespace Power_X1_iCT.Model
{
    public class ProvideMaterialSettingsModel
    {
        private MagazineLoader.Application.SubProcesses.ProvideMaterial mProvideMagazine = MagazineLoader.Application.MagazineLoaderModule.Instance.SubProcesses.ProvideMaterial;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double MagazineSlotOffset { get; set; }
        public double MagazineBottomSlotOffset { get; set; }
        public int DeviceCountPerLeadFrame { get; set; }
        public double MagazineLiftStartPositionZ { get; set; }
        public double MagazineLiftStartPositionY { get; set; }
        public double MagazineWidth { get; set; }

        public ProvideMaterialSettingsModel()
        {
            ProvideMagazine_SettingsChanged(this, mProvideMagazine.Settings);
            mProvideMagazine.SettingsChanged += ProvideMagazine_SettingsChanged;
        }

        private void ProvideMagazine_SettingsChanged(object sender, ProvideMagazineDataModel e)
        {
            MagazineSlotOffset = e.MagazineSlotOffset;
            MagazineBottomSlotOffset = e.MagazineBottomSlotOffset;
            DeviceCountPerLeadFrame = e.DeviceCountPerLeadFrame;
            MagazineLiftStartPositionZ = e.MagazineLiftStartPositionZ;
            MagazineLiftStartPositionY = e.MagazineLiftStartPositionY;
            MagazineWidth = e.MagazineWidth;
        }

        public void Save()
        {
            var machineRecipe = mRecipeManager.GetMachineRecipe();
            machineRecipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionY = MagazineLiftStartPositionY;
            machineRecipe.MagazineLoader.ProvideMaterial.MagazineLiftStartPositionZ = MagazineLiftStartPositionZ;
            mRecipeManager.SaveRecipe(machineRecipe);

            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.Magazine.SlotOffset = MagazineSlotOffset;
            productRecipe.Magazine.BottomSlotOffset = MagazineBottomSlotOffset;
            productRecipe.Magazine.DevicesPerSlot = DeviceCountPerLeadFrame;
            productRecipe.Magazine.MagazineWidth = MagazineWidth;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new ProvideMagazineDataModel();
            dataModel.MagazineSlotOffset = MagazineSlotOffset;
            dataModel.MagazineBottomSlotOffset = MagazineBottomSlotOffset;
            dataModel.DeviceCountPerLeadFrame = Convert.ToInt16(DeviceCountPerLeadFrame);
            dataModel.MagazineLiftStartPositionZ = MagazineLiftStartPositionZ;
            dataModel.MagazineLiftStartPositionY = MagazineLiftStartPositionY;
            dataModel.MagazineWidth = MagazineWidth;

            mProvideMagazine.UpdateSettings(dataModel);
        }
    }
}
