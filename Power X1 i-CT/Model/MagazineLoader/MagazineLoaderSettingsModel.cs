﻿using MagazineLoader.Application;


namespace Power_X1_iCT.Model
{
    public class MagazineLoaderSettingsModel
    {
        private MagazineLoaderModule mMagazineLoader = MagazineLoaderModule.Instance;
        private int mVelocityPercentage;

        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                if (mVelocityPercentage != value)
                {
                    mVelocityPercentage = value;
                }
            }
        }

        public MagazineLoaderSettingsModel()
        {
            MagazineLoader_SettingsChanged(this, mMagazineLoader.Settings);
            mMagazineLoader.SettingsChanged += MagazineLoader_SettingsChanged;
        }

        private void MagazineLoader_SettingsChanged(object sender, MagazineLoaderDataModel e)
        {
            VelocityPercentage = e.VelocityPercentage;
        }

        public void SetVelocityPercentage(int percentage)
        {
            mMagazineLoader.SetVelocityPercentage(percentage);
        }
    }
}
