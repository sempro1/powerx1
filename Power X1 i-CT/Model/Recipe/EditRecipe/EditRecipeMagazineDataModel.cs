﻿using Microsoft.AspNetCore.Mvc;
using System;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class EditRecipeMagazineDataModel
    {
        public uint SlotCount { get; set; }
        public string SlotOffset { get; set; }
        public uint DevicesPerSlot { get; set; }
        public string BottomSlotOffset { get; set; }
        public string Width { get; set; }
        public EditRecipeMagazineDataModel(Magazine magazine)
        {
            SlotCount = Convert.ToUInt32(magazine.SlotCount);
            SlotOffset = magazine.SlotOffset.ToString();
            DevicesPerSlot = Convert.ToUInt32(magazine.DevicesPerSlot);
            BottomSlotOffset = magazine.BottomSlotOffset.ToString("F2");
            Width = magazine.MagazineWidth.ToString("F2");
        }

        public EditRecipeMagazineDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Magazine ConvertToMagazineRecipe()
        {
            var output = new Magazine();

            output.BottomSlotOffset = Convert.ToDouble(BottomSlotOffset);
            output.SlotCount = Convert.ToInt32(SlotCount);
            output.SlotOffset = Convert.ToDouble(SlotOffset);
            output.DevicesPerSlot = Convert.ToInt32(DevicesPerSlot);
            output.MagazineWidth = Convert.ToDouble(Width);

            return output;
        }
    }
}