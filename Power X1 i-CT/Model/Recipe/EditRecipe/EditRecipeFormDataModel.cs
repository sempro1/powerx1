﻿using Microsoft.AspNetCore.Mvc;
using System;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class EditRecipeFormDataModel
    {
        public string TopPosition { get; set; }
        public string SensorPosition { get; set; }
        public string ClosePosition { get; set; }
        public string Velocity { get; set; }
        public string RemovePosition { get; set; }
        public string MaintenancePosition { get; set; }

        public EditRecipeFormDataModel(Form form)
        {
            TopPosition = form.TopPosition.ToString("F2");
            SensorPosition = form.SensorPosition.ToString("F2");
            ClosePosition = form.ClosePosition.ToString("F2");
            Velocity = form.Velocity.ToString("F0");
            RemovePosition = form.RemovePosition.ToString("F2");
            MaintenancePosition = form.MaintenancePosition.ToString("F2");
        }

        public  EditRecipeFormDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Form ConvertToFormRecipe()
        {
            var output = new Form();

            output.TopPosition = Convert.ToDouble(TopPosition);
            output.SensorPosition = Convert.ToDouble(SensorPosition);
            output.ClosePosition = Convert.ToDouble(ClosePosition);
            output.Velocity = Convert.ToInt32(Velocity);
            output.RemovePosition = Convert.ToDouble(RemovePosition);
            output.MaintenancePosition = Convert.ToDouble(MaintenancePosition);

            return output;
        }

    }
}
