﻿using System;
using System.Collections.Generic;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    public class EditRecipeRobotTrayDataModel
    {
        public string Count { get; set; }
        public string Tray1 { get; set; } = "Disabled";
        public string Tray2 { get; set; } = "Disabled";      
        public string Tray3 { get; set; } = "Disabled";
        public string Tray4 { get; set; } = "Disabled";
        public string Tray5 { get; set; } = "Disabled";
        public string Tray6 { get; set; } = "Disabled";

        public string DefaultVelocityPercentage { get; set; }

        public EditRecipeRobotTrayDataModel(Robot robot)
        {         
            Count = robot.BaseCount.ToString("F0");
            if (robot.Locations.Count > 5)
            {
                Tray1 = robot.Locations[0].ToString();
                Tray2 = robot.Locations[1].ToString();
                Tray3 = robot.Locations[2].ToString();
                Tray4 = robot.Locations[3].ToString();
                Tray5 = robot.Locations[4].ToString();
                Tray6 = robot.Locations[5].ToString();
            }
            DefaultVelocityPercentage = robot.DefaultVelocityPercentage.ToString();
        }

        public EditRecipeRobotTrayDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Robot ConvertToRobotRecipe()
        {
            var output = new Robot();
            output.BaseCount = Convert.ToInt32(Count);
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray1));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray2));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray3));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray4));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray5));
            output.Locations.Add(Enum.Parse<OutfeedLocationType>(Tray6));
            output.DefaultVelocityPercentage = Convert.ToInt32(DefaultVelocityPercentage);

            return output;
        }
    }
}
