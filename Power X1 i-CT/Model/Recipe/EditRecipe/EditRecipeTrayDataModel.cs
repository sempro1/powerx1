﻿using System;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    public class EditRecipeTrayDataModel
    {
        public string PocketOffsetX { get; set; }
        public string PocketOffsetY { get; set; }
        public string TrayHeight { get; set; }
        public string PocketDepth { get; set; }
        public string TrayToFirstPocketOffsetX { get; set; }
        public string TrayToFirstPocketOffsetY { get; set; }

        public EditRecipeTrayDataModel(Tray tray)
        {
            PocketOffsetX = tray.PocketOffsetX.ToString("F2");
            PocketOffsetY = tray.PocketOffsetY.ToString("F2");
            TrayHeight = tray.TrayHeight.ToString("F2");
            PocketDepth = tray.PocketDepth.ToString("F2");
            TrayToFirstPocketOffsetX = tray.TrayToFirstPocketOffsetX.ToString("F2");
            TrayToFirstPocketOffsetY = tray.TrayToFirstPocketOffsetY.ToString("F2");
        }

        public EditRecipeTrayDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Tray ConvertToTrayRecipe()
        {
            var output = new Tray();
            output.PocketOffsetX = Convert.ToDouble(PocketOffsetX);
            output.PocketOffsetY = Convert.ToDouble(PocketOffsetY);
            output.TrayHeight = Convert.ToDouble(TrayHeight);
            output.PocketDepth = Convert.ToDouble(PocketDepth);
            output.TrayToFirstPocketOffsetX = Convert.ToDouble(TrayToFirstPocketOffsetX);
            output.TrayToFirstPocketOffsetY = Convert.ToDouble(TrayToFirstPocketOffsetY);

            return output;
        }

    }
}
