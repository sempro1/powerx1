﻿using Microsoft.AspNetCore.Mvc;
using System;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class EditRecipeDeviceDataModel
    {
        public string DeviceSizeZ { get; set; }

        public EditRecipeDeviceDataModel(Device device)
        {
            DeviceSizeZ = device.DeviceSizeZ.ToString("F2");
        }

        public EditRecipeDeviceDataModel()
        {
            //Parameterless constructor is needed to create a serializable object
        }

        public Device ConvertToDeviceRecipe()
        {
            var output = new Device();
            output.DeviceSizeZ = Convert.ToDouble(DeviceSizeZ);

            return output;
        }
    }
}
