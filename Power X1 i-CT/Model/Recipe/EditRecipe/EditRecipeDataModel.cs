﻿using Microsoft.AspNetCore.Mvc;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Model
{
    [BindProperties]
    public class EditRecipeDataModel
    {
        public string CustomerName { get; set; }
        public bool Released { get; set; }
        public EditRecipeMagazineDataModel Magazine { get; set; } = new();
        public EditRecipeDeviceDataModel Device { get; set; } = new();
        public EditRecipeTrayDataModel Tray { get; set; } = new();
        public EditRecipeTrimDataModel Trim { get; set; } = new();
        public EditRecipeFormDataModel Form { get; set; } = new();
        public EditRecipeRobotTrayDataModel Robot { get; set; } = new();

        public EditRecipeDataModel(ProductRecipe recipe)
        {
            CustomerName = recipe.CustomerName;
            Released = recipe.Released;
            Magazine = new (recipe.Magazine);
            Device = new(recipe.Device);
            Tray = new(recipe.Tray);
            Trim = new(recipe.Trim);
            Form = new(recipe.Form);
            Robot = new(recipe.Robot);
        }

        public EditRecipeDataModel()
        {
            CustomerName = string.Empty;
        }

        public ProductRecipe ConvertToProductRecipe()
        {
            RecipeManager.IRecipeManager recipeManager = RecipeManager.RecipeManager.Instance;

            ProductRecipe output = recipeManager.GetRecipe(CustomerName);
            output.Magazine = Magazine.ConvertToMagazineRecipe();
            output.Device = Device.ConvertToDeviceRecipe();
            output.Tray = Tray.ConvertToTrayRecipe();
            output.Trim = Trim.ConvertToTrimRecipe();
            output.Form = Form.ConvertToFormRecipe();
            output.Robot = Robot.ConvertToRobotRecipe();
            output.Released = Released;

            return output;
        }
    }
}