﻿using MaterialTracking.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Model
{
    public class RecipeModel
    {
        public string Name { get; set; } = string.Empty;
        public string Created { get; set; } = string.Empty;
        public string LastModified { get; set; } = string.Empty;
        public string LastModifiedBy { get; set; } = string.Empty;
        public bool Released { get; internal set; }

        public RecipeModel(string name, DateTime created, DateTime lastModified, string lastModifiedBy, bool released)
        {
            Name = name;
            Created = created.ToString("dd-MM-yyyy HH:mm:ss");
            LastModified = lastModified.ToString("dd-MM-yyyy HH:mm:ss");
            LastModifiedBy = lastModifiedBy;
            Released = released;
        }

        public RecipeModel(ProductRecipe recipe)
        {
            Name = recipe.CustomerName;
            Created = recipe.Created.ToString("dd-MM-yyyy HH:mm:ss");
            LastModified = recipe.DateOfRevision.ToString("dd-MM-yyyy HH:mm:ss");
            LastModifiedBy = recipe.LastModifiedBy;
            Released = recipe.Released;
        }
    }
}