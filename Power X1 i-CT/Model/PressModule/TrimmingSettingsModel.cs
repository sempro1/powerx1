﻿using PressModule.Application.SubProcesses;
using RecipeManager;

namespace Power_X1_iCT.Model
{
    public class TrimmingSettingsModel
    {
        private Trimming mTrimming = PressModule.Application.PressModule.Instance.SubProcesses.Trimming;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double TopPosition { get; set; }
        public double SensorPosition { get; set; }
        public double ClosePosition { get; set; }
        public double RemovePosition { get; set; }
        public double MaintenancePosition { get; set; }
        public double Velocity { get; set; }

        public TrimmingSettingsModel()
        {
            Trimming_SettingsChanged(this, mTrimming.Settings);
            mTrimming.SettingsChanged += Trimming_SettingsChanged;
        }

        private void Trimming_SettingsChanged(object sender, TrimmingDataModel e)
        {
            TopPosition = e.TopPosition;
            SensorPosition = e.SensorPosition;
            ClosePosition = e.ClosePosition;
            RemovePosition = e.RemovePosition;
            MaintenancePosition = e.MaintenancePosition;
            Velocity = e.Velocity;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.Trim.TopPosition = TopPosition;
            productRecipe.Trim.SensorPosition = SensorPosition;
            productRecipe.Trim.ClosePosition = ClosePosition;
            productRecipe.Trim.RemovePosition = RemovePosition;
            productRecipe.Trim.MaintenancePosition = MaintenancePosition;
            productRecipe.Trim.Velocity = Velocity;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }
        public void Download()
        {
            var dataModel = new TrimmingDataModel();
            dataModel.TopPosition = TopPosition;
            dataModel.SensorPosition = SensorPosition;
            dataModel.ClosePosition = ClosePosition;
            dataModel.RemovePosition = RemovePosition;
            dataModel.MaintenancePosition = MaintenancePosition;
            dataModel.Velocity = Velocity;

            mTrimming.UpdateSettings(dataModel);
        }
    }
}
