﻿using PressModule.Application;

namespace Power_X1_iCT.Model
{
    public class PressModuleSettingsModel
    {
        private PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;
        private int mVelocityPercentage;

        public int VelocityPercentage
        {
            get => mVelocityPercentage;
            set
            {
                if (mVelocityPercentage != value)
                {
                    mVelocityPercentage = value;
                }
            }
        }

        public PressModuleSettingsModel()
        {
            PressModule_SettingsChanged(this, mPressModule.Settings);
            mPressModule.SettingsChanged += PressModule_SettingsChanged;
        }

        private void PressModule_SettingsChanged(object sender, PressModuleDataModel e)
        {
            VelocityPercentage = e.VelocityPercentage;
        }

        public void SetVelocityPercentage(int percentage)
        {
            mPressModule.SetVelocityPercentage(percentage);
        }
    }
}

