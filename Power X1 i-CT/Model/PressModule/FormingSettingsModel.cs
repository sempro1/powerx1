﻿using PressModule.Application.SubProcesses;
using RecipeManager;



namespace Power_X1_iCT.Model
{
    public class FormingSettingsModel
    {
        private Forming mForming = PressModule.Application.PressModule.Instance.SubProcesses.Forming;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public double TopPosition { get; set; }
        public double SensorPosition { get; set; }
        public double ClosePosition { get; set; }
        public double RemovePosition { get; set; }
        public double MaintenancePosition { get; set; }
        public double Velocity { get; set; }

        public FormingSettingsModel()
        {
            Forming_SettingsChanged(this, mForming.Settings);
            mForming.SettingsChanged += Forming_SettingsChanged;
        }
        private void Forming_SettingsChanged(object sender, FormingDataModel e)
        {
            TopPosition = e.TopPosition;
            SensorPosition = e.SensorPosition;
            ClosePosition = e.ClosePosition;
            RemovePosition = e.RemovePosition;
            MaintenancePosition = e.MaintenancePosition;
            Velocity = e.Velocity;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.Form.TopPosition = TopPosition;
            productRecipe.Form.SensorPosition = SensorPosition;
            productRecipe.Form.ClosePosition = ClosePosition;
            productRecipe.Form.RemovePosition = RemovePosition;
            productRecipe.Form.MaintenancePosition = MaintenancePosition;
            productRecipe.Form.Velocity = Velocity;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }

        public void Download()
        {
            var dataModel = new FormingDataModel();
            dataModel.TopPosition = TopPosition;
            dataModel.SensorPosition = SensorPosition;
            dataModel.ClosePosition = ClosePosition;
            dataModel.RemovePosition = RemovePosition;
            dataModel.MaintenancePosition = MaintenancePosition;
            dataModel.Velocity = Velocity;

            mForming.UpdateSettings(dataModel);
        }
    }
}
