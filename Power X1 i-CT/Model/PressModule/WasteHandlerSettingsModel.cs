﻿using PressModule.Application.SubProcesses;
using RecipeManager;

namespace Power_X1_iCT.Model
{
    public class WasteHandlerSettingsModel
    {
        private WasteHandler mWasteHandler = PressModule.Application.PressModule.Instance.SubProcesses.WasteHandler;
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;

        public int AlmostFullCount { get; set; }
        public int MaximumWasteCount { get; set; }

        public WasteHandlerSettingsModel()
        {
            WasteHandler_SettingsChanged(this, mWasteHandler.Settings);
            mWasteHandler.SettingsChanged += WasteHandler_SettingsChanged;
        }

        private void WasteHandler_SettingsChanged(object sender, WasteHandlerDataModel e)
        {
            AlmostFullCount = e.AlmostFullCount;
            MaximumWasteCount = e.MaximumWasteCount;
        }

        public void Save()
        {
            var productRecipe = mRecipeManager.GetCurrentRecipe();
            productRecipe.WasteHandlerBin.AlmostFullCount = AlmostFullCount;
            productRecipe.WasteHandlerBin.MaximumWasteCount = MaximumWasteCount;
            mRecipeManager.SaveRecipe(productRecipe);

            Download();
        }
        public void Download()
        {
            var dataModel = new WasteHandlerDataModel();
            dataModel.AlmostFullCount = AlmostFullCount;
            dataModel.MaximumWasteCount = MaximumWasteCount;

            mWasteHandler.UpdateSettings(dataModel);
        }
    }
}
