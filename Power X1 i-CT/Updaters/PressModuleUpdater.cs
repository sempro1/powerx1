﻿using log4net;
using Logging;
using MagazineLoader.Application.Resources;
using MagazineLoader.PLCInterfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Power_X1_iCT;
using Power_X1_iCT.Resources;
using Power_X1_iCT.Resources.StatusBar;
using PressModule.Application.Resources;
using PressModule.Application.SubProcesses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PressModule.Updater
{
    public class PressModuleUpdater
    {
        private const string PressModuleStatusUpdate = nameof(PressModuleStatusUpdate);
        private const string PressModuleStateUpdate = nameof(PressModuleStateUpdate);
        private const string PressModuleSimulationChanged = nameof(PressModuleSimulationChanged);

        private readonly ILog mLog = LogClient.Get();

        private const string cSimulationID = "PressModuleSimulation";
        private const string cPressModuleStatusID = "PressModuleStatus";
        private readonly Application.PressModule mPressModule = Application.PressModule.Instance;

        private const string cTrimmingStatusID = "TrimmingStatus";
        private readonly Trimming mTrimming = Application.PressModule.Instance.SubProcesses.Trimming;

        private const string cWasteHandlerStatusID = "WasteHandlerStatus";
        private readonly WasteHandler mWasteHandler = Application.PressModule.Instance.SubProcesses.WasteHandler;

        private const string cDambarStatusID = "DambarStatus";
        private readonly Dambar mDambar = Application.PressModule.Instance.SubProcesses.Dambar;

        private const string cFormingStatusID = "FormingStatus";
        private readonly Forming mForming = Application.PressModule.Instance.SubProcesses.Forming;

        private const string cIndexHandlerStatusID = "IndexHandlerStatus";
        private readonly IndexHandler mIndexHandler = Application.PressModule.Instance.SubProcesses.IndexHandler;

        private const string cTrimStatusID = "TrimStatus";
        private readonly Trim mTrim = Application.PressModule.Instance.Resources.Trim;

        private const string cFormStatusID = "FormStatus";
        private readonly Form mForm = Application.PressModule.Instance.Resources.Form;

        private const string cIndexerStatusID = "IndexerStatus";
        private readonly Indexer mIndexer = Application.PressModule.Instance.Resources.Indexer;

        private const string cVacuumCleanerStatusID = "VacuumCleanerStatus";

        private readonly VacuumCleaner mVacuumCleaner = Application.PressModule.Instance.Resources.VacuumCleaner;

        private readonly IHubContext<PowerX1iCTHub> mContext;

        public PressModuleUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mLog.Info("PressModuleUpdater");

            mContext = context;
            mPressModule.StateChanged += async (sender, e) => await SendStateChanged();
            mPressModule.StatusChanged += async (sender, e) => await SendStatusChanged(cPressModuleStatusID, e);
            mTrimming.StatusChanged += async (sender, e) => await SendStatusChanged(cTrimmingStatusID, e);
            mWasteHandler.StatusChanged += async (sender, e) => await SendStatusChanged(cWasteHandlerStatusID, e);
            mDambar.StatusChanged += async (sender, e) => await SendStatusChanged(cDambarStatusID, e);
            mForming.StatusChanged += async (sender, e) => await SendStatusChanged(cFormingStatusID, e);
            mIndexHandler.StatusChanged += async (sender, e) => await SendStatusChanged(cIndexHandlerStatusID, e);
            mTrim.StatusChanged += async (sender, e) => await SendStatusChanged(cTrimStatusID, e);
            mForm.StatusChanged += async (sender, e) => await SendStatusChanged(cFormStatusID, e);
            mIndexer.StatusChanged += async (sender, e) => await SendStatusChanged(cIndexerStatusID, e);
            mVacuumCleaner.StatusChanged += async (sender, e) => await SendStatusChanged(cVacuumCleanerStatusID, e);
            mPressModule.SimulationChanged += async (sender, e) => await SendPressModuleSimulationChanged(cSimulationID, e);
        }

        internal JsonResult GetStatusses()
        {
            Dictionary<string, string> statusses = new();

            statusses.Add(cPressModuleStatusID, StatusBar.ConvertToString(mPressModule.Status));

            statusses.Add(cTrimmingStatusID, StatusBar.ConvertToString(mTrimming.Status));
            statusses.Add(cWasteHandlerStatusID, StatusBar.ConvertToString(mWasteHandler.Status));
            statusses.Add(cDambarStatusID, StatusBar.ConvertToString(mDambar.Status));
            statusses.Add(cFormingStatusID, StatusBar.ConvertToString(mForming.Status));
            statusses.Add(cIndexHandlerStatusID, StatusBar.ConvertToString(mIndexHandler.Status));
            statusses.Add(cTrimStatusID, StatusBar.ConvertToString(mTrim.Status));
            statusses.Add(cFormStatusID, StatusBar.ConvertToString(mForm.Status));
            statusses.Add(cIndexerStatusID, StatusBar.ConvertToString(mIndexer.Status));
            statusses.Add(cVacuumCleanerStatusID, StatusBar.ConvertToString(mVacuumCleaner.Status));

            return new JsonResult(statusses);
        }

        private async Task SendStatusChanged(string itemToUpdate, int value)
        {
            //Convert value to statusbar item text
            var statusNameOfIntValue = StatusBar.ConvertToString(value);

            mLog.Info($"SendStatusChanged: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(PressModuleStatusUpdate, itemToUpdate, statusNameOfIntValue);
        }

        private async Task SendStateChanged()
        {
            await mContext.Clients.All.SendAsync(PressModuleStateUpdate);
        }

        private async Task SendPressModuleSimulationChanged(string itemToUpdate, bool value)
        {
            mLog.Info($"SendAsyncToClients: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(PressModuleSimulationChanged, itemToUpdate, value);
        }
    }
}
