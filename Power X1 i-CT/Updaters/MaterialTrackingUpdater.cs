﻿using log4net;
using Logging;
using MagazineLoader.Application;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using System.Threading.Tasks;
using Power_X1_iCT;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MaterialTracking
{
    public class MaterialTrackingUpdater
    {
        private const string HasMaterialChanged = nameof(HasMaterialChanged);

        private readonly ILog mLog = LogClient.Get();
        private readonly IHubContext<PowerX1iCTHub> mContext;

        private MagazineLoaderModule mMagazineLoader = MagazineLoaderModule.Instance;
        private const string cUpperConveyorHasMagazineID = "UpperConveyorHasMagazine";
        private const string cMagazineLoaderLiftHasMagazineID = "MagazineLoaderLiftHasMagazine";
        private const string cLowerConveyorHasMagazineID = "LowerConveyorHasMagazine";
        private const string cOutfeedHasMaterialChangedID = "OutfeedHasMaterial";

        private PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;
        private const string cInfeedMaterialID = "InfeedHasMaterial";
        private const string cTrimmingMaterialID = "TrimmingHasMaterial";
        private const string cDambarMaterialID = "DambarHasMaterial";
        private const string cWasteBinMaterialID = "WasteBinHasMaterial";
        private const string cFormingMaterialID = "FormingHasMaterial";

        private const string cIndexerPosition0HasMaterialID = "IndexerPosition0HasMaterial";
        private const string cIndexerPosition1HasMaterialID = "IndexerPosition1HasMaterial";
        private const string cIndexerPosition2HasMaterialID = "IndexerPosition2HasMaterial";
        private const string cIndexerPosition3HasMaterialID = "IndexerPosition3HasMaterial";
        private const string cIndexerPosition4HasMaterialID = "IndexerPosition4HasMaterial";

        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;
        private const string cRobotInfeedHasMaterialID = "RobotInfeedHasMaterial";
        private const string cRobotHasMaterialID = "RobotHasMaterial";

        private const string cTray1TrayID = "Tray1HasTray";
        private const string cTray2TrayID = "Tray2HasTray";
        private const string cTray3TrayID = "Tray3HasTray";
        private const string cTray4TrayID = "Tray4HasTray";
        private const string cTray5TrayID = "Tray5HasTray";
        private const string cTray6TrayID = "Tray6HasTray";

        public MaterialTrackingUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mLog.Info("MaterialTrackingUpdater");
            mContext = context;

            mMagazineLoader.MaterialTracking.UpperConveyorHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cUpperConveyorHasMagazineID, e);
            mMagazineLoader.MaterialTracking.MagazineLiftHasMaterialChanged += async(sender, e) => await SendHasMaterialChanged(cMagazineLoaderLiftHasMagazineID, e);
            mMagazineLoader.MaterialTracking.OutfeedHasMaterialChanged += async(sender, e) => await SendHasMaterialChanged(cOutfeedHasMaterialChangedID, e);
            mMagazineLoader.MaterialTracking.LowerConveyorHasMaterialChanged += async(sender, e) => await SendHasMaterialChanged(cLowerConveyorHasMagazineID, e);

            mPressModule.MaterialTracking.InfeedHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cInfeedMaterialID, e);
            mPressModule.MaterialTracking.TrimmingHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cTrimmingMaterialID, e);
            mPressModule.MaterialTracking.DambarHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cDambarMaterialID, e);
            mPressModule.MaterialTracking.WasteBinHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cWasteBinMaterialID, e);
            mPressModule.MaterialTracking.FormingHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cFormingMaterialID, e);

            mPressModule.MaterialTracking.IndexerPosition0HasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cIndexerPosition0HasMaterialID, e);
            mPressModule.MaterialTracking.IndexerPosition1HasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cIndexerPosition1HasMaterialID, e);
            mPressModule.MaterialTracking.IndexerPosition2HasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cIndexerPosition2HasMaterialID, e);
            mPressModule.MaterialTracking.IndexerPosition3HasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cIndexerPosition3HasMaterialID, e);
            mPressModule.MaterialTracking.IndexerPosition4HasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cIndexerPosition4HasMaterialID, e);

            mRobotToTrayUnloader.MaterialTracking.InfeedHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cRobotInfeedHasMaterialID, e);
            mRobotToTrayUnloader.MaterialTracking.RobotHasMaterialChanged += async (sender, e) => await SendHasMaterialChanged(cRobotHasMaterialID, e);
            mRobotToTrayUnloader.MaterialTracking.TrayIdChanged += MaterialTracking_TrayIdChanged;
        }

        private async void MaterialTracking_TrayIdChanged(object sender, Lib.HasTrayChangedDataModel e)
        {
            switch (e.TrayPosition)
            {
                case Lib.TrayPosition.Tray1:
                    await SendHasMaterialChanged(cTray1TrayID, e.TrayId);
                    break;
                case Lib.TrayPosition.Tray2:
                    await SendHasMaterialChanged(cTray2TrayID, e.TrayId);
                    break;
                case Lib.TrayPosition.Tray3:
                    await SendHasMaterialChanged(cTray3TrayID, e.TrayId);
                    break;
                case Lib.TrayPosition.Tray4:
                    await SendHasMaterialChanged(cTray4TrayID, e.TrayId);
                    break;
                case Lib.TrayPosition.Tray5:
                    await SendHasMaterialChanged(cTray5TrayID, e.TrayId);
                    break;
                case Lib.TrayPosition.Tray6:
                    await SendHasMaterialChanged(cTray6TrayID, e.TrayId);
                    break;
            }
        }


        public JsonResult GetAllMaterialTrackingData()
        {
            Dictionary<string, object> result = new();

            result.Add(cUpperConveyorHasMagazineID, mMagazineLoader.MaterialTracking.UpperConveyorHasMaterial);
            result.Add(cMagazineLoaderLiftHasMagazineID, mMagazineLoader.MaterialTracking.MagazineLiftHasMaterial);
            result.Add(cLowerConveyorHasMagazineID, mMagazineLoader.MaterialTracking.LowerConveyorHasMaterial);
            result.Add(cOutfeedHasMaterialChangedID, mMagazineLoader.MaterialTracking.OutfeedHasMaterial);

            result.Add(cInfeedMaterialID, mPressModule.MaterialTracking.GetPressPositionDeviceId(0));
            result.Add(cTrimmingMaterialID, mPressModule.MaterialTracking.GetPressPositionDeviceId(1));
            result.Add(cDambarMaterialID, mPressModule.MaterialTracking.GetPressPositionDeviceId(2));
            result.Add(cWasteBinMaterialID, mPressModule.MaterialTracking.GetPressPositionDeviceId(3));
            result.Add(cFormingMaterialID, mPressModule.MaterialTracking.GetPressPositionDeviceId(4));

            result.Add(cIndexerPosition0HasMaterialID, mPressModule.MaterialTracking.GetIndexerPositionDeviceId(0));
            result.Add(cIndexerPosition1HasMaterialID, mPressModule.MaterialTracking.GetIndexerPositionDeviceId(1));
            result.Add(cIndexerPosition2HasMaterialID, mPressModule.MaterialTracking.GetIndexerPositionDeviceId(2));
            result.Add(cIndexerPosition3HasMaterialID, mPressModule.MaterialTracking.GetIndexerPositionDeviceId(3));
            result.Add(cIndexerPosition4HasMaterialID, mPressModule.MaterialTracking.GetIndexerPositionDeviceId(4));

            result.Add(cRobotInfeedHasMaterialID, mRobotToTrayUnloader.MaterialTracking.GetInfeedPositionDeviceId());
            result.Add(cRobotHasMaterialID, mRobotToTrayUnloader.MaterialTracking.GetRobotPositionDeviceId());

            result.Add(cTray1TrayID, mRobotToTrayUnloader.MaterialTracking.Tray1ID);
            result.Add(cTray2TrayID, mRobotToTrayUnloader.MaterialTracking.Tray2ID);
            result.Add(cTray3TrayID, mRobotToTrayUnloader.MaterialTracking.Tray3ID);
            result.Add(cTray4TrayID, mRobotToTrayUnloader.MaterialTracking.Tray4ID);
            result.Add(cTray5TrayID, mRobotToTrayUnloader.MaterialTracking.Tray5ID);
            result.Add(cTray6TrayID, mRobotToTrayUnloader.MaterialTracking.Tray6ID);            

            return new JsonResult(result);
        }

        private async Task SendHasMaterialChanged(string itemToUpdate, object value)
        {
            mLog.Info($"SendAsyncToClients: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(HasMaterialChanged, itemToUpdate, value);
        }
    }
}

