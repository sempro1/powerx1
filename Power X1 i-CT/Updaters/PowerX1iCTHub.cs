﻿using Microsoft.AspNetCore.SignalR;

namespace Power_X1_iCT
{
    public class PowerX1iCTHub : Hub
    {
        public const string Pattern = nameof(PowerX1iCTHub);
    }
}
