﻿using log4net;
using Logging;
using MaterialTracking;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Power_X1_iCT;
using Power_X1_iCT.Model;

namespace LotControl
{
    public class LotControlUpdater
    {
        private const string LotsUpdated = nameof(LotsUpdated);
        private const string DevicesUpdated = nameof(DevicesUpdated);

        private readonly ILog mLog = LogClient.Get();
        private readonly IHubContext<PowerX1iCTHub> mContext;
        private Application.LotControl mLotControl;
        private MaterialTrackingApplication mMaterialTracking;

        public LotControlUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mLog.Debug("LotControlUpdater");
            mContext = context;
            mLotControl = Application.LotControl.Instance;
            mLotControl.LotAdded += LotUpdated;
            mLotControl.LotRemoved += LotUpdated;
            mLotControl.LotOpened += LotUpdated;
            mLotControl.LotClosed += LotUpdated;
            mLotControl.LotUpdated += LotUpdated;

            mMaterialTracking = MaterialTrackingApplication.Instance;
            mMaterialTracking.DeviceUpdated += DeviceUpdated;
        }

        private async void LotUpdated(object sender, string e)
        {
            mLog.Info($"LotUpdated");
            await mContext.Clients.All.SendAsync(LotsUpdated);
        }

        private async void DeviceUpdated(object sender, int e)
        {
            await mContext.Clients.All.SendAsync(DevicesUpdated);
        }
    }
}

