﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using System.Linq;

namespace Power_X1_iCT
{
    public class EventManagerUpdater
    {
        private const string UpdateEventsReceived = nameof(UpdateEventsReceived);

        private readonly IHubContext<PowerX1iCTHub> mContext;
        private EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;

        public EventManagerUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mContext = context;
            mEventManager.EventAdded += (object sender, EventHandler.Lib.Event e) => UpdateEvents();
            mEventManager.EventRemoved += (object sender, EventHandler.Lib.Event e) => UpdateEvents();
        }

        ~EventManagerUpdater()
        {
            mEventManager.EventAdded -= (object sender, EventHandler.Lib.Event e) => UpdateEvents();
            mEventManager.EventRemoved -= (object sender, EventHandler.Lib.Event e) => UpdateEvents(); ;
        }

        public void UpdateEvents()
        {
            mContext.Clients.All.SendAsync(UpdateEventsReceived);
        }
    }
}

