﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using System.Threading.Tasks;
using Logging;
using log4net;
using Power_X1_iCT;
using LineControl.Application;

namespace LineControl
{
    public class LineControlUpdater
    {
        private const string ReceiveStateChanged = nameof(ReceiveStateChanged);

        private const string CheckBoxChanged = nameof(CheckBoxChanged);
        private const string DryRunChanged = nameof(DryRunChanged);
        

        private readonly ILog mLog = LogClient.Get();

        private Application.LineControl mLineControl = Application.LineControl.Instance;

        private readonly IHubContext<PowerX1iCTHub> mContext;

        public LineControlUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mLog.Info("LineControlUpdater");

            mContext = context;
            mLineControl.DataChanged += LineControl_DataChanged;
        }

        private async void LineControl_DataChanged(object sender, LineControlDataModel e)
        {
            await mContext.Clients.All.SendAsync(DryRunChanged);
            await SendStateChanged();
        }

        private async Task SendStateChanged()
        {
            mLog.Debug($"SendStateChanged: LineControl");
            await mContext.Clients.All.SendAsync(ReceiveStateChanged);
        }
    }
}

