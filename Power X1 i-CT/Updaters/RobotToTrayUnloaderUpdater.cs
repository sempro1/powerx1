﻿using log4net;
using Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Power_X1_iCT;
using Power_X1_iCT.Resources;
using Power_X1_iCT.Resources.StatusBar;
using RobotToTrayUnloader.Application.Resources;
using RobotToTrayUnloader.Application.SubProcesses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RobotToTrayUnloader.Updater
{
    public class RobotToTrayUnloaderUpdater
    {
        private const string RobotToTrayUnloaderStatusUpdate = nameof(RobotToTrayUnloaderStatusUpdate);
        private const string RobotToTrayUnloaderStateUpdate = nameof(RobotToTrayUnloaderStateUpdate);
        private const string RobotToTrayUnloaderSimulationChanged = nameof(RobotToTrayUnloaderSimulationChanged);

        private readonly ILog mLog = LogClient.Get();

        private const string cSimulationID = "RobotToTrayUnloaderSimulation";
        private const string cRobotToTrayUnloaderStatusID = "RobotToTrayUnloaderStatus";
        private readonly Application.RobotToTrayUnloader mRobotToTrayUnloader = Application.RobotToTrayUnloader.Instance;

        private const string cInspectionStatusID = "InspectionStatus";
        private readonly Inspection mInspection = Application.RobotToTrayUnloader.Instance.SubProcesses.Inspection;

        private const string cScanningStageStatusID = "ScanningstageStatus";
        private readonly ScanningStage mScanningStage = Application.RobotToTrayUnloader.Instance.Resources.ScanningStage;

        private const string cRobotStatusID = "RobotStatus";
        private readonly Robot mRobot = Application.RobotToTrayUnloader.Instance.SubProcesses.Robot;

        private readonly IHubContext<PowerX1iCTHub> mContext;

        public RobotToTrayUnloaderUpdater(IHubContext<PowerX1iCTHub> context)
        {
            mLog.Info("RobotToTrayUnloaderUpdater");

            mContext = context;
            mRobotToTrayUnloader.StateChanged += async(Sender, e) => await SendStateChanged();
            mRobotToTrayUnloader.StatusChanged += async (Sender, e) => await SendStatusChanged(cRobotToTrayUnloaderStatusID, e);
            mInspection.StatusChanged += async (sender, e) => await SendStatusChanged(cInspectionStatusID, e);
            mScanningStage.StatusChanged += async (sender, e) => await SendStatusChanged(cScanningStageStatusID, e);
            mRobot.StatusChanged += async (sender, e) => await SendStatusChanged(cRobotStatusID, e);
            mRobotToTrayUnloader.SimulationChanged += async (sender, e) => await SendCheckBoxChangedAsyncToClients(cSimulationID, e);
        }

        internal JsonResult GetStatusses()
        {
            Dictionary<string, string> statusses = new();

            statusses.Add(cRobotToTrayUnloaderStatusID, StatusBar.ConvertToString(mRobotToTrayUnloader.Status));

            statusses.Add(cInspectionStatusID, StatusBar.ConvertToString(mInspection.Status));
            statusses.Add(cRobotStatusID, StatusBar.ConvertToString(mRobot.Status));
            statusses.Add(cScanningStageStatusID, StatusBar.ConvertToString(mScanningStage.Status));

            return new JsonResult(statusses);
        }

        private async Task SendStatusChanged(string itemToUpdate, int value)
        {
            //Convert value to statusbar item text
            var statusNameOfIntValue = StatusBar.ConvertToString(value);

            mLog.Info($"SendStatusChanged: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderStatusUpdate, itemToUpdate, statusNameOfIntValue);
        }

        private async Task SendStateChanged()
        {
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderStateUpdate);
        }
        private async Task SendCheckBoxChangedAsyncToClients(string itemToUpdate, bool value)
        {
            mLog.Info($"SendAsyncToClients: {itemToUpdate}, value: {value}");
            await mContext.Clients.All.SendAsync(RobotToTrayUnloaderSimulationChanged, itemToUpdate, value);
        }
    }          
}
