﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Areas.Identity.Data
{
    public class ForgotPasswordInputModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
