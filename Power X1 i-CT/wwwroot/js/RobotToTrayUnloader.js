﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    // StatusBar region
    connection.on("RobotToTrayUnloaderStatusUpdate", function (item, status) {
        receiveStatusChanged(item, status);
    });

    // StateBar region
    connection.on("RobotToTrayUnloaderStateUpdate", function (item, state) {
        getState();
    });

    // CheckBoxChanged region
    connection.on("CheckBoxChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('RobotToTrayUnloaderState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get status");
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        $(item).removeClass('busy')
        item.textContent = status;
        $(item).addClass(status.toLowerCase());
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupRobotToTrayUnloader?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get simulation enabled");
            },
        });
    }

    function receiveSimulationEnabled(value) {
        var item = document.getElementById('RobotToTrayUnloaderSimulation');
        item.checked = value;
    }
})

$("#CloseMainRobotToTrayUnloader").click(function (e) {
    e.preventDefault();
    $("#SetupMainRobotToTrayUnloaderContainer").hide();
});

$("#CloseSetupInspection").click(function (e) {
    e.preventDefault();
    $("#SetupInspectionContainer").hide();
});

$("#CloseSetupRobot").click(function (e) {
    e.preventDefault();
    $("#SetupRobotContainer").hide();
});

$("#CloseSetupRobotPick").click(function (e) {
    e.preventDefault();
    $("#SetupRobotPickContainer").hide();
});

$("#CloseSetupRobotPlace").click(function (e) {
    e.preventDefault();
    $("#SetupRobotPlaceContainer").hide();
});

$("#CloseSetupRobotHome").click(function (e) {
    e.preventDefault();
    $("#SetupRobotHomeContainer").hide();
});

$(function () {
    $("#SetupMainRobotToTrayUnloader").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainRobotToTrayUnloaderContainer");
    });

    $("#SetupInspection").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupInspectionContainer");
    });

    $("#SetupRobot").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotContainer");
    });

    $("#SetupRobotPick").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotPickContainer");
    });

    $("#SetupRobotPlace").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotPlaceContainer");
    });

    $("#SetupRobotHome").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupRobotHomeContainer");
    });

    $("#DebugScanningStage").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugScanningStageContainer");
    });

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupMainRobotToTrayUnloaderContainer").hide();
        $("#SetupInspectionContainer").hide();
        $("#SetupRobotContainer").hide();
        $("#SetupRobotPickContainer").hide();
        $("#SetupRobotPlaceContainer").hide();
        $("#SetupRobotHomeContainer").hide();

    }

    function hideResourceContainers() {
    $("#DebugScanningStageContainer").hide();
    }
});
