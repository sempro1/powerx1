﻿"use strict";
$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    // StatusBar region
    connection.on("ReceiveStatusChanged", function (item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        item.textContent = status;

        switch (status) {
            case 'Ready':
                $(item).addClass(status.toLowerCase());
                break;
            case 'Busy':
                $(item).addClass(status.toLowerCase());
                break;
            case 'Error':
                $(item).addClass(status.toLowerCase());
                break;
        }
    });

    // CheckBoxChanged region
    connection.on("CheckBoxChanged", function (item, value) {
        var item = document.getElementById(item);

        if (value)
            item.setAttribute('checked', 'checked');
        else
            item.removeAttribute('checked');
    });

    // State region
    connection.on("ReceiveStateChanged", function () {
        getLineState();
    });

    connection.start().then(function () {
    }).catch(function (err) {
        // maybe try a reconnect?
        return console.error(err.toString());
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getLineState();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();
})

function getLineState() {
    $.ajax({
        url: "../../StartStop?handler=GetLineControlState",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (state) {
            receiveLineStateChanged(state)
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
        },
    });
}

function getDryRunState() {
    $.ajax({
        url: "../../StartStop?handler=GetDryRunState",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (state) {
            receiveDryRunChanged(state)
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
        },
    });
}

function getUPH() {
    $.ajax({
        url: "../../StartStop?handler=GetUPH",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (UPH) {
            receivedUPH(UPH)
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
        },
    });
}

function receivedUPH(UPH) {
    document.getElementById('UPHLabel').textContent = UPH;
}

function receiveDryRunChanged(state) {
    var label = document.getElementById('dryRunLabel');
    if (state)
        label.textContent = 'Enabled';
    else
        label.textContent = 'Disabled';
}

function receiveLineStateChanged(state) {
    var item = document.getElementById('start-button');
    // Remove all classes
    $(item).removeAttr("class");
    $(item).addClass(state.state.toLowerCase());
}

function toggleSideBar() {
    var navBar = document.getElementById('nav-bar');
    var toggleSideBarImage = document.getElementById('toggleSideBarImage');
    var body = document.getElementById('body-pd');
    var topBar = document.getElementById('top-bar');
    var logoImage = document.getElementById('logo-image');
    var topBarContent = document.getElementById('top-bar-content');

    var bodyView = document.getElementById('body-view');

    if ($(navBar).hasClass("hide")) {
        $(navBar).removeClass("hide");
        toggleSideBarImage.src = "/icons/maximize.png";
        $(body).removeClass("body-maximum");

        $(topBar).removeClass("top-minimum");
        $(topBar).addClass("top-maximum");

        $(logoImage).addClass("logo-maximum");
        $(logoImage).removeClass("logo-minimum");

        $(topBarContent).removeClass("top-bar-content-minimum");
        $(bodyView).removeClass("body-view-content-minimum");
        $(bodyView).addClass("body-view-content-maximum");

    }
    else {
        $(navBar).addClass("hide");
        toggleSideBarImage.src = "/icons/minimize.png";
        $(body).addClass("body-maximum");

        $(topBar).addClass("top-minimum");
        $(topBar).removeClass("top-maximum");

        $(logoImage).removeClass("logo-maximum");
        $(logoImage).addClass("logo-minimum");

        $(topBarContent).addClass("top-bar-content-minimum");

        $(bodyView).removeClass("body-view-content-maximum");
        $(bodyView).addClass("body-view-content-minimum");
    }
}
