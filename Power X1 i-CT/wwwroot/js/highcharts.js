﻿Highcharts.chart('container', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Reject reasons current lot'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }
    },
    series: [{
        type: 'pie',
        name: 'Reject reason',
        data: [
            ['OK', 85.0],
            ['Dambar check failed', 4.8],
            {
                name: 'Signal pin 5 not OK',
                y: 1.2,
                sliced: true,
                selected: true
            },
            ['Power pin 1 not OK', 4.5],
            ['Power pin 3 not OK', 3.2],
            ['Other', 1.3]
        ]
    }]
});