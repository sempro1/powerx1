﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    // StatusBar region
    connection.on("PressModuleStatusUpdate", function (item, status) {
        receiveStatusChanged(item, status);
    });

    // StateBar region
    connection.on("PressModuleStateUpdate", function (item, state) {
        getState();
    });

    // CheckBoxChanged region
    connection.on("PressModuleSimulationChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupPressModule?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('PressModuleState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupPressModule?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        $(item).removeClass('busy')
        item.textContent = status;
        $(item).addClass(status.toLowerCase());
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupPressModule?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get simulation enabled");
            },
        });
    }

    function receiveSimulationEnabled(value) {
        var item = document.getElementById('PressModuleSimulation');
        item.checked = value;
    }
})

$("#CloseMainPressModule").click(function (e) {
    e.preventDefault();
    $("#SetupMainPressModuleContainer").hide();
});

$("#CloseSetupTrimming").click(function (e) {
    e.preventDefault();
    $("#SetupTrimContainer").hide();
});

$("#CloseSetupForming").click(function (e) {
    e.preventDefault();
    $("#SetupWasteHandlerContainer").hide();
});

$("#CloseSetupForming").click(function (e) {
    e.preventDefault();
    $("#SetupFormContainer").hide();
});

$(function () {
    $("#SetupMainPressModule").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainPressModuleContainer");
    });

    $("#SetupTrimming").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupTrimmingContainer");
    });

    $("#SetupWasteHandler").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupWasteHandlerContainer");
    });

    $("#SetupForming").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupFormingContainer");
    });

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupMainPressModuleContainer").hide();
        $("#SetupTrimmingContainer").hide();
        $("#SetupWasteHandlerContainer").hide();
        $("#SetupFormingContainer").hide();
    }

    function hideResourceContainers() {
        $("#DebugTrimContainer").hide();
        $("#DebugFormmContainer").hide();
        $("#DebugIndexerContainer").hide();
        $("#DebugVacuumCleanerContainer").hide();
    }
});
