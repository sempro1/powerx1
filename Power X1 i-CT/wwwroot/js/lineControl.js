﻿"use strict";
$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    connection.on("DryRunChanged", function () {
        getDryRunEnabled();
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getDryRunEnabled();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();
})

function getDryRunEnabled() {
    var dryRunElement = document.getElementById('InDryRun');
    if (dryRunElement == null)
        return;

    $.ajax({
        url: "../Setup/Setup?handler=GetDryRunEnabled",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
        },
        success: function (data) {
            if (data)
                dryRunElement.setAttribute('checked', 'checked');
            else
                dryRunElement.removeAttribute('checked');
        }
    });
}