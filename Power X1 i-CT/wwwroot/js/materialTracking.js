﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build()

    // CheckBoxChanged region
    connection.on("HasMaterialChanged", function (item, value) {
        setValue(item, value);
    });

    // State region
    connection.on("MagazineLoaderStateUpdate", function () {
        getStates();
    });

    connection.on("PressModuleStateUpdate", function (item, state) {
        getStates();
    });

    connection.on("ReceiveStateUpdate", function (item, state) {
        getState();
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getData();
                getStates();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getData() {
        $.ajax({
            url: "./Production?handler=GetMaterialTrackingData",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (data) {
                Object.entries(data).forEach(([key, value]) => {
                    setValue(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "PLC returned error string");
            },
        });
    }

    function setValue(item, value) {
        var item = document.getElementById(item);
        if (value != 0) {

            item.setAttribute('checked', 'checked');
            item.setAttribute('onclick', 'setSelectedDeviceData(' + value + ');return false;');
            item.checked = true;
        }
        else {
            item.removeAttribute('checked');
            item.setAttribute('onclick', 'return false;e.preventDefault()');
            item.checked = false;
        }
    }

    function getStates() {
        $.ajax({
            url: "./Production?handler=GetStates",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (states) {
                receiveStatesChanged(states);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStatesChanged(states) {
        var state;
        var element;

        //MagazineLoader
        state = states.states[0];
        element = document.getElementById('magazineLoaderTracking');
        setState(element, state);

        //PressModule
        state = states.states[1];
        element = document.getElementById('pressModuleTracking');
        setState(element, state);

        //Offloader
        state = states.states[2];
        element = document.getElementById('robotToTrayUnloaderTracking');
        setState(element, state);
    }

    function setState(element, state) {
        $(element).removeClass('stopping');
        $(element).removeClass('stopped');
        $(element).removeClass('starting');
        $(element).removeClass('running');
        $(element).addClass(state.toLowerCase());
    }
})