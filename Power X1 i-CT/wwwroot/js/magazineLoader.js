﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    // StatusBar region
    connection.on("MagazineLoaderStatusUpdate", function (item, status) {
       receiveStatusChanged(item, status);       
    });

    // StateBar region
    connection.on("MagazineLoaderStateUpdate", function () {
        getState();       
    });

    // CheckBoxChanged region
    connection.on("MagazineLoaderSimulationChanged", function (item, value) {
        var item = document.getElementById(item);
        item.checked = value;
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getState();
                getStatusses();
                getSimulationEnabled();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();

    function getState() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetState",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (state) {
                receiveStateChanged(state);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get state");
            },
        });
    }

    function receiveStateChanged(state) {
        var item = document.getElementById('MagazineLoaderState');
        $(item).removeClass('stopping');
        $(item).removeClass('stopped');
        $(item).removeClass('starting');
        $(item).removeClass('running');
        item.textContent = state.state;
        $(item).addClass(state.state.toLowerCase());
    }

    function getStatusses() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetStatusses",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (statusses) {
                Object.entries(statusses).forEach(([key, value]) => {
                    receiveStatusChanged(key, value);
                });
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get status");
            },
        });
    }

    function receiveStatusChanged(item, status) {
        var item = document.getElementById(item);
        $(item).removeClass('ready');
        $(item).removeClass('error');
        $(item).removeClass('busy')
        item.textContent = status;
        $(item).addClass(status.toLowerCase());
    }

    function getSimulationEnabled() {
        $.ajax({
            url: "./SetupMagazineLoader?handler=GetSimulationEnabled",
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (value) {
                receiveSimulationEnabled(value);
            },
            error: function (response) {
                console.log("Error handler");
                console.log(response);
                alert("Error: " + "Failed to get simulation enabled");
            },
        });
    }

    function receiveSimulationEnabled(value){
        var item = document.getElementById('MagazineLoaderSimulation');
            item.checked = value;
    }
})

$("#CloseMainMagazineLoader").click(function (e) {
    e.preventDefault();
    $("#SetupMainMagazineLoaderContainer").hide();
});

$("#CloseSetupLoadMagazine").click(function (e) {
    e.preventDefault();
    $("#SetupLoadMagazineContainer").hide();
});

$("#CloseSetupUnloadMagazine").click(function (e) {
    e.preventDefault();
    $("#SetupUnloadMagazineContainer").hide();
});

$("#CloseProvideMaterial").click(function (e) {
    e.preventDefault();
    $("#SetupProvideMaterialContainer").hide();
});

$("#CloseUpperConveyorDebug").click(function (e) {
    e.preventDefault();
    $("#DebugUpperConveyorContainer").hide();
});

$(function () {
    $("#SetupMainMagazineLoader").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupMainMagazineLoaderContainer");
    });

    $("#SetupLoadMagazine").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupLoadMagazineContainer");
    });

    $("#SetupProvideMaterial").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupProvideMaterialContainer");
    });

    $("#SetupUnloadMagazine").click(function (e) {
        OnClickProcessSettingsButton(e, "#SetupUnloadMagazineContainer");
    });

    $("#DebugUpperConveyor").click(function (e) {
        OnClickResourceSettingsButton(e, "#DebugUpperConveyorContainer");
    });

    function OnClickProcessSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideProcessContainers();
            item.show();
            return;
        }
        hideProcessContainers();
    }

    function OnClickResourceSettingsButton(e, nameOfContainer) {
        e.preventDefault();
        var item = $(nameOfContainer);
        if (item[0].style.display == "none") {
            hideResourceContainers();
            item.show();
            return;
        }
        hideResourceContainers();
    }

    function hideProcessContainers() {
        $("#SetupLoadMagazineContainer").hide();
        $("#SetupProvideMaterialContainer").hide();
        $("#SetupUnloadMagazineContainer").hide();
        $("#SetupMainMagazineLoaderContainer").hide();
    }

    function hideResourceContainers() {
        $("#DebugUpperConveyorContainer").hide();
    }
});

$(function () {
    $("#Simulation").change(function (e) {
        e.preventDefault();
        sendCommand("SetupMagazineLoader", "SimulationChanged");
    });

    
})