﻿"use strict";
$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    connection.on("LotsUpdated", function () {
        getLotDataTable();
        getSelectedLotData(selectedLotName);
    });

    connection.on("DevicesUpdated", function () {
        getSelectedDeviceData(selectedDeviceId);
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getLotDataTable();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();
})

function getLotDataTable() {
    var $tbl = $('#tblLots');

    if ($tbl == null) {
        return;
    }
    $.ajax({
        url: "../Production/Production?handler=GetLots",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "Unable to get lots from webserver");
        },
        success: function (data) {
            $tbl.empty();

            $.each(data.lots, function (i, lotItem) {
                var binButton = "";
                if (lotItem.State == "Planned") {
                    binButton =
                        '<td>' +
                        '<button class="smallbutton" onclick="RemoveLot(\'' + lotItem.CustomerName + '\')" ' + 'title="Remove lot">' +
                        '<img src="../icons/minus.png" width="40">' +
                        '</button>' +
                        '</td>'
                }
                else {
                    binButton ='<td>' + '</td>'
                }
                $tbl.append
                    (
                        '<tr  onclick="setSelectedLotData(\'' + lotItem.CustomerName + '\')">' +
                            '<td style="vertical-align:middle">' + lotItem.State + '</td>' +
                            '<td style="vertical-align:middle">' + lotItem.CustomerName + '</td>' +
                        binButton +
                        '</tr>'
                    );
            });
        }
    });
}

var selectedLotName = "";
function setSelectedLotData(lotName) {
    selectedLotName = lotName;

    //Hide close lot button, as you dont know if you can close it yet
    $("#ExecuteCloseLotButton").hide();

    //Button pressed, change container
    hideDeviceViewContainer();
    $("#AddLotContainer").hide();
    $("#ViewLotContainer").show();

    //Mark data as retreiving
    $("#SelectedLotName").text(lotName);
    $("#SelectedLotState").text("Retreiving data..");
    $("#SelectedLotOpened").text("Retreiving data..");
    $("#SelectedLotClosed").text("Retreiving data..");
    $("#SelectedLotDevices").text("Retreiving data..");
    $("#SelectedLotLeadFrames").text("Retreiving data..");

    getSelectedLotData(selectedLotName);
}

var selectedDeviceId = "";
function setSelectedDeviceData(deviceId) {
    selectedDeviceId = deviceId;

    //Button pressed, change container
    hideLotViewContainer();
    $("#AddLotContainer").hide();
    $("#ViewDeviceContainer").show();

    //Mark data as retreiving
    $("#SelectedDeviceLot").text("Retreiving data..");
    $("#SelectedDeviceId").text(deviceId);
    $("#SelectedDeviceState").text("Retreiving data..");
    $("#SelectedDeviceCreated").text("Retreiving data..");
    $("#SelectedDeviceProcessed").text("Retreiving data..");
    $("#SelectedInspection3dResult").text("Retreiving data..")
    $("#SelectedInspectionDambarResult").text("Retreiving data..")

    getSelectedDeviceData(selectedDeviceId);
}

function getSelectedLotData(lotName) {
    if (lotName == "")
        return;

    hideDeviceViewContainer();
    $("#AddLotContainer").hide();
    $("#ViewLotContainer").show();
    $.ajax({
        url: "../Production/Production?handler=GetSelectedLot",
        data: { input: lotName },
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "Unable to get lots from webserver");
        },
        success: function (data) {
            $("#SelectedLotName").text(data.selectedLotModel.LotName);
            $("#SelectedLotState").text(data.selectedLotModel.State);
            $("#SelectedLotOpened").text(data.selectedLotModel.Opened);
            $("#SelectedLotClosed").text(data.selectedLotModel.Closed);
            $("#SelectedLotDevices").text(data.selectedLotModel.DeviceCount);
            $("#SelectedLotLeadFrames").text(data.selectedLotModel.LeadFrameCount);

            if (data.selectedLotModel.State == "Opened") {
                $("#ExecuteCloseLotButton").show();
            }
            else {
                $("#ExecuteCloseLotButton").hide();
            }
        }
    });
}

function getSelectedDeviceData(deviceId) {

    if (deviceId == "")
        return;

    hideLotViewContainer();
    $("#AddLotContainer").hide();
    $("#ViewDeviceContainer").show();
    $.ajax({
        url: "../Production/Production?handler=GetSelectedDevice",
        data: { input: deviceId },
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "Unable to get lots from webserver");
        },
        success: function (data) {
            $("#SelectedDeviceLot").text(data.selectedDeviceModel.LotName);
            $("#SelectedDeviceId").text(data.selectedDeviceModel.Id);
            $("#SelectedDeviceState").text(data.selectedDeviceModel.State);
            $("#SelectedDeviceCreated").text(data.selectedDeviceModel.Created);
            $("#SelectedDeviceProcessed").text(data.selectedDeviceModel.Processed);
            $("#SelectedInspection3dResult").text(data.selectedDeviceModel.Inspection)
            $("#SelectedInspectionDambarResult").text(data.selectedDeviceModel.DambarInspection)
        }
    });
}

$("#CloseAddLot").click(function (e) {
    e.preventDefault();
    $("#AddLotContainer").hide();
});

$("#AddLotButton").click(function (e) {
    e.preventDefault();
    $("#AddLotContainer").show();
    hideLotViewContainer();
    hideDeviceViewContainer();
});

$("#CloseLotView").click(function (e) {
    e.preventDefault();
    hideLotViewContainer();
});

function hideLotViewContainer() {
    $("#ViewLotContainer").hide();
    selectedLotName = "";
}

$("#CloseDeviceView").click(function (e) {
    e.preventDefault();
    hideDeviceViewContainer();

});

function hideDeviceViewContainer() {
    $("#ViewDeviceContainer").hide();
    selectedDeviceId = "";
}

$("#ExecuteAddLotButton").click(function (e) {
    e.preventDefault();
    if ($("#AddLotName").val() == "") {
        return;
    }
    else {
        sendCommand("Production", "AddLot", $("#AddLotName").val());
        $("#AddLotContainer").hide();
    }
});

$("#ExecuteCloseLotButton").click(function (e) {
    e.preventDefault();
    sendCommand("Production", "CloseCurrentLot");
});

$("#ExecuteRemoveLotButton").click(function (e) {
    e.preventDefault();
    sendCommand("Production", "RemoveLot", $("#LotName").val());
    $("#AddLotContainer").hide();
    hideLotViewContainer();
});

function RemoveLot(lotName) {
    hideLotViewContainer();
    sendCommand("Production", "RemoveLot", lotName);
};