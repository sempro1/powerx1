﻿"use strict";

$(() => {
    //Create a connection to the signalR hub to receive message updates
    var connection = new signalR.HubConnectionBuilder().withUrl("/PowerX1iCTHub").build();

    connection.on("UpdateEventsReceived", function () {
        getEventData();
        getEventCounts();
        setSelectedEvent(selectedEvent);
    });

    async function start() {
        try {
            await connection.start().then(function () {
                getEventData();
                getEventCounts();
            });
        } catch (err) {
            console.log(err);
            setTimeout(start, 5000);
        }
    };

    connection.onclose(async () => {
        await start();
    });

    // Start the connection
    start();
});

function getEventData() {
    var $tbl = $('#tblEvents');

    if ($tbl == null) {
        return;
    }
    $.ajax({
        url: "../../Event/Event?handler=GetEvents",
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "PLC returned error string");
        },
        success: function (data) {
            $tbl.empty();

            $.each(data.events, function (i, eventItem) {
                $tbl.append
                    (
                    '<tr onclick="doSetSelectedEventPostback(' + eventItem.ID + ')">' +
                        '<td>' +
                        '<div style="height: 20px; width: 20px; border-radius: 15px; background: ' + eventItem.Color + '"></div>' + '</td>' +
                        '<td>' + eventItem.Created + '</td>' +
                        '<td>' + eventItem.Title + '</td>' +
                        '<td>' + eventItem.Module + '</td>' +
                    '<tr>'
                    );
            });

            if (!data.events.includes(x => x.ID == selectedEvent)) {

                if (selectedEvent != "") {
                    selectedEvent = "";
                    setSelectedEvent(0);
                    resetSelectedEventPage();
                }
            }

            if (selectedEvent == "" && data.events.length > 0) {
                doSetSelectedEventPostback(data.events[0]);
            }
        }
    });
}

var selectedEvent = "";
function doSetSelectedEventPostback(id) {
    $.ajax({
        url: "./Event?handler=GetSelectedEvent",
        type: 'POST',
        data: { input: id },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (data) {
            selectedEvent = data.data;
            setSelectedEvent(data.data)
            getSelectedEventPage(data.data);
        }
    });
}

function setSelectedEvent(event) {
    var mainEvent = document.getElementById("mainEvent");
    var title = document.getElementById("EventTitle");
    var content = document.getElementById("eventContent");

    if (event == 0 || event.Id == 0) {
        $(mainEvent).removeClass("show");
        $(mainEvent).addClass("hide");
        mainEvent.style.border = "4px solid gray";
        $(eventData).hide();
        $(content).hide();
        title.textContent = "No events";
        $(content).removeClass("show");
        $(content).addClass("hide");
    } 
    else {
        mainEvent.style.border = "4px solid " + event.Color;
        $(mainEvent).removeClass("hide");
        $(mainEvent).addClass("show");
        $(eventData).show();
        $(content).show();
        title.textContent = event.Title;
        var module = document.getElementById("EventModule");
        module.textContent = event.Module;
        var process = document.getElementById("EventProcess");
        process.textContent = event.Process;
        var created = document.getElementById("EventCreationDate");
        created.textContent = event.Created;
    }
}

function getSelectedEventPage(event) {
    if (event.ID == "")
        return;

    $.ajax({
        url: "./Event?handler=GetSelectedEventPage",
        data: { input: event.ID },
        type: 'POST',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        success: function (data) {
            var content = document.getElementById("eventContent");
            $(content).html(data);
        }
    });
}

function resetSelectedEventPage() {
    var content = document.getElementById("eventContent");
    $(content).html();
}