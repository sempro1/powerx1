﻿
function sendCommand(page, commandName) {
    let url = "./" + page + "?handler=" + commandName;
    $.ajax({
        type: "POST",
        url: url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "PLC returned error string");
        },
        success: function (response) {
            console.log(response);
            $('#result').text(response.message);
        }
    });
}

function sendCommand(page, commandName, parameter) {
    let url = "./" + page + "?handler=" + commandName;
    $.ajax({
        type: "POST",
        url: url,
        data: { input: parameter },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        error: function (response) {
            console.log("Error handler");
            console.log(response);
            alert("Error: " + "PLC returned error string");
        },
        success: function (response) {
            console.log(response);
            $('#result').text(response.message);
        }
    });
}
