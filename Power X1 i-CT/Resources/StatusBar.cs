﻿namespace Power_X1_iCT.Resources.StatusBar
{
    public class StatusBar
    {
        public static string StatusBarUpdateMessage { get => "ReceiveStatusUpdate"; }

        public enum Status
        {
            Ready = 1,
            Busy = 2,
            Error = 3
        }

        public static string ConvertToString(int status)
{
            return ((Status)status).ToString();
        }
    }
}
