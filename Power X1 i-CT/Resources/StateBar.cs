﻿namespace Power_X1_iCT.Resources
{
    public class StateBar
    {
        public enum State
        {
            Stopped = 1,
            Starting = 2,
            Running = 3,
            Stopping = 4,
            Error = 5
        }

        public static string ConvertToString(int state)
        {
            return ((State)state).ToString();
        }
    }
}
