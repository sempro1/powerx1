@echo off
set /p versionInfo=< ..\versionInfo.txt

echo Version number in file: %versionInfo%

setlocal EnableExtensions DisableDelayedExpansion
set "FullVersion=%versionInfo%"
if not defined FullVersion set "FullVersion=1.0.0.512"
echo Version to increment is: %FullVersion%

rem Make sure that the environment variables used below are
rem not defined already by chance outside the batch file.
set "MajorVersion="
set "MinorVersion="
set "Maintenance="
set "BuildNumber="

rem Split up the version string into four integer values.
for /F "tokens=1-4 delims=." %%I in ("%FullVersion%") do (
    if not "%%L" == "" (
        set "MajorVersion=%%I."
        set "MinorVersion=%%J."
        set "Maintenance=%%K."
        set "BuildNumber=%%L"
    ) else if not "%%K" == "" (
        set "MajorVersion=%%I."
        set "MinorVersion=%%J."
        set "BuildNumber=%%K"
    ) else if not "%%J" == "" (
        set "MajorVersion=%%I."
        set "BuildNumber=%%J"
    ) else set "BuildNumber=%%I"
)
set /A BuildNumber+=1
rem Concatenate the version string together with incremented build number.
set "newVersion=%MajorVersion%%MinorVersion%%Maintenance%%BuildNumber%"

echo Building: %newVersion%

REM create build
msbuild "H:\Projects\PowerX1\Power X1 i-CT\Power X1 i-CT.sln" /property:Configuration=Release /p:DeployOnBuild=true /p:PublishProfile=FolderProfile /m /p:AssemblyVersion=%newVersion%

REM create installer
"C:\Program Files (x86)\Inno Setup 6\iscc.exe" "Installer script PowerX1 i-CT.iss"

> ..\versionInfo.txt echo %newVersion%
echo Updated version to: %newVersion%
pause
