using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace Power_X1_ICT
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //webBuilder.UseKestrel();
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseIISIntegration();
                    //webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    //webBuilder.Build();
                }).ConfigureLogging(builder => {
                    builder.SetMinimumLevel(LogLevel.Debug);
                    //builder.AddLog4Net("log4net.config");
                });
    }
}
