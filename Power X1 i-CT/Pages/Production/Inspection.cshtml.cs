using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class InspectionModel : PageModel
    {
        public List<InspectionDataModel> InspectionModels { get; set; } = new();
         
        public JsonResult OnPostGetInspectionModels()
        {
            InspectionModels.Add(GetDambarInspectionDataModel());
            InspectionModels.Add(GetInspection3DDataModel());
            InspectionModels.Add(GetTopInspectionDataModel());            
            InspectionModels.Add(GetBottomInspectionDataModel());

            return new JsonResult(InspectionModels);
        }

        private InspectionDataModel GetBottomInspectionDataModel()
        {
            var result = DateTime.Now.Ticks % 2 == 0;
            var bottomInspection = new InspectionDataModel("Bottom inspection")
            {
                Result = result,
                DeviceId = GetRandomValue(100, 999).ToString(),
                InspectionResults = new List<InspectionResult>()
                {   new InspectionResult("Body defects", GetRandomValue(0, 10), "#", 0, 9),
                },
            };
            bottomInspection.Image = GetRandomByteArrayImageFromFolder(bottomInspection.Name);
            bottomInspection.Result = !bottomInspection.InspectionResults.Any(x => !x.Result);

            return bottomInspection;
        }

        private InspectionDataModel GetTopInspectionDataModel()
        {
            bool result = DateTime.Now.Ticks % 2 == 0;
            var topInspection = new InspectionDataModel("Top inspection")
            {
                Result = result,
                DeviceId = GetRandomValue(100, 999).ToString(),
                InspectionResults = new List<InspectionResult>()
                {     new InspectionResult("Body defects", GetRandomValue(0, 10), "#", 0, 9),
                },
            };
            topInspection.Image = GetRandomByteArrayImageFromFolder(topInspection.Name);
            topInspection.Result = !topInspection.InspectionResults.Any(x => !x.Result);
            return topInspection;
        }

        private InspectionDataModel GetInspection3DDataModel()
        {
            var inspection3D = new InspectionDataModel("3D inspection")
            {
                DeviceId = GetRandomValue(100, 999).ToString(),
                InspectionResults = new List<InspectionResult>()
                { new InspectionResult("Signal pin 1 Y", GetRandomValue(20.0, 21), "mm", 20.005, 20.995),
                    new InspectionResult("Signal pin 1 X", GetRandomValue(11.0, 12), "mm", 11.005, 11.99),
                    new InspectionResult("Signal pin 2 Y", GetRandomValue(21.0, 22), "mm", 20.005, 22.995),
                    new InspectionResult("Signal pin 2 X", GetRandomValue(7.0, 8), "mm", 7.005, 7.995),
                    new InspectionResult("Signal pin 3 Y", GetRandomValue(22.0, 23), "mm", 22.005, 22.995),
                    new InspectionResult("Signal pin 3 X", GetRandomValue(4.0, 5), "mm", 4.005, 4.995),
                    new InspectionResult("Signal pin 4 Y", GetRandomValue(23.0, 24), "mm", 23.005, 23.995),
                    new InspectionResult("Signal pin 4 X", GetRandomValue(0.0, 1), "mm", 0.005, 0.995),
                    new InspectionResult("Signal Pin 5 Y", GetRandomValue(23.0, 24), "mm", 23.005, 23.995),
                    new InspectionResult("Signal Pin 5 X", GetRandomValue(44.0, 45), "mm", 44.005, 44.995),
                    new InspectionResult("Signal Pin 6 Y", GetRandomValue(22.0, 23), "mm", 22.005, 23.995),
                    new InspectionResult("Signal Pin 6 X", GetRandomValue(47.0, 48), "mm", 47.005, 47.995),
                    new InspectionResult("Signal Pin 7 Y", GetRandomValue(21.0, 22), "mm", 21.005, 21.995),
                    new InspectionResult("Signal Pin 7 X", GetRandomValue(51.0, 52), "mm", 51.005, 51.995),
                    new InspectionResult("Signal Pin 8 Y", GetRandomValue(20.0, 22), "mm", 20.005, 22.995),
                    new InspectionResult("Signal Pin 8 X", GetRandomValue(54.0, 55), "mm", 54.005, 54.995),
                    new InspectionResult("Power pin 1 X", GetRandomValue(32.0, 33), "mm", 32.005, 32.995),
                    new InspectionResult("Power pin 2 X", GetRandomValue(14.0, 15), "mm", 14.005, 14.995),
                    new InspectionResult("Power pin 3 X", GetRandomValue(32.0, 33), "mm", 32.005, 32.995),
                },
            };
            inspection3D.Image = GetRandomByteArrayImageFromFolder(inspection3D.Name);
            inspection3D.Result = !inspection3D.InspectionResults.Any(x => !x.Result);
            return inspection3D;
        }

        private InspectionDataModel GetDambarInspectionDataModel()
        {
            var dambarInspection = new InspectionDataModel("Dambar inspection")
            {
                DeviceId = GetRandomValue(100, 999).ToString("N0"),
                InspectionResults = new List<InspectionResult>()
                { new InspectionResult("Signal pin 1 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 1 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 2 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 2 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 3 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 3 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 4 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 4 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 5 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 5 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 6 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 6 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 7 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 7 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 8 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Signal pin 8 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 1 up", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 1 down", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 2 left", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 2 right", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 3 up", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269),
                    new InspectionResult("Power pin 3 down", GetRandomValue(0.009, 0.027), "mm", 0.0091, 0.0269)
                },
                VisionApplication = "KeyenceInspection3D:",
            };
            dambarInspection.Image = GetRandomByteArrayImageFromFolder(dambarInspection.Name);
            dambarInspection.Result = !dambarInspection.InspectionResults.Any(x => !x.Result);
            return dambarInspection;
        }

        public JsonResult OnPostGetInspectionModel(string input)
        {
            JsonResult output;
            switch (input)
            {
                case "Dambar inspection":
                    output =  new JsonResult(GetDambarInspectionDataModel());
                    break;
                case "3D inspection":
                    output = new JsonResult(GetInspection3DDataModel());
                    break;
                case "Top inspection":
                    output = new JsonResult(GetTopInspectionDataModel());
                    break;
                case "Bottom inspection":
                    output = new JsonResult(GetBottomInspectionDataModel());
                    break;
                default:
                    output = new JsonResult(GetDambarInspectionDataModel());
                    break;
            }

            return output;
        }

        private byte[] GetRandomByteArrayImageFromFolder(string folder)
        {
            var rand = new Random();
            string ImageLocationPath = Directory.GetCurrentDirectory() + "/bin/Inspections/" + folder;
            var files = Directory.GetFiles(ImageLocationPath, "*.png");
            var file = files[rand.Next(files.Length)];

            return System.IO.File.ReadAllBytes(file);
        }

        private double GetRandomValue(double min, double max)
        {
             return min + (max - min) * new Random().NextDouble();      
        }
                
        private int GetRandomValue(int min, int max)
        {
            return new Random().Next(min, max);
        }

       
    }
}

