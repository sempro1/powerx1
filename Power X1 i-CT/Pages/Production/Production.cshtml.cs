using LineControl;
using LotControl;
using LotControl.Lib;
using MagazineLoader;
using MaterialTracking;
using MaterialTracking.Lib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using Power_X1_iCT.Resources;
using PressModule.Updater;
using RobotToTrayUnloader.Updater;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class ProductionModel : PageModel
    {
        private MaterialTrackingUpdater mMaterialTrackingUpdater;
        private LotControl.Application.LotControl mLotControl;
        private MaterialTrackingApplication mMaterialTracking;
        private readonly MagazineLoader.Application.MagazineLoaderModule mMagazineLoader = MagazineLoader.Application.MagazineLoaderModule.Instance;
        private readonly PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;
        private readonly RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;

        public AddLotModel AddLotModel { get; set; } = new AddLotModel();

        public ProductionModel(MaterialTrackingUpdater materialTrackingUpdater, LotControlUpdater lotControlUpdater, MagazineLoaderUpdater loaderUpdater, PressModuleUpdater pressUpdater, RobotToTrayUnloaderUpdater robotToTrayUnloaderUpdater)
        {
            mMaterialTrackingUpdater = materialTrackingUpdater;
            mLotControl = LotControl.Application.LotControl.Instance;
            mMaterialTracking = MaterialTrackingApplication.Instance;
        }

        public JsonResult OnPostGetMaterialTrackingData()
        {
            return mMaterialTrackingUpdater.GetAllMaterialTrackingData();
        }

        public void OnPostAddLot(string input)
        {
            mLotControl.AddPlannedLot(input);
        }

        public void OnPostRemoveLot(string input)
        {
            mLotControl.RemovePlannedLot(input);
        }

        public void OnPostCloseCurrentLot()
        {
            mLotControl.CloseLot();
        }

        public JsonResult OnPostGetLots()
        {
            List<LotDataModel> lots = new();

            foreach (var lot in mLotControl.PlannedLots)
            {
                lots.Add(new LotDataModel(lot, LotProcessState.Planned));
            }

            foreach (var lot in mMaterialTracking.GetLots().OrderByDescending(x => x.Id).Take(30))
            {
                lots.Add(new LotDataModel(lot.CustomerName, lot.ProcessState));
            }

            return new JsonResult(new { lots = lots });
        }

        public JsonResult OnPostGetSelectedLot(string input)
        {
            var selectedLotModel = new SelectedLotModel
            {
                LotName = input,
                Opened = "Not available",
                Closed = "Not available"
            };

            if (mLotControl.PlannedLots.Any(x => x == input))
            {
                selectedLotModel.State = LotProcessState.Planned.ToString();
            }
            else
            {
                var lot = mMaterialTracking.GetLot(input);
                selectedLotModel.State = lot.ProcessState.ToString();

                if (lot.StartTime != System.DateTime.MinValue)
                {
                    selectedLotModel.Opened = lot.StartTime.ToString("dd-MM-yyyy HH:mm:ss");
                }
                if (lot.EndTime != System.DateTime.MinValue)
                {
                    selectedLotModel.Closed = lot.EndTime.ToString("dd-MM-yyyy HH:mm:ss");
                }

                selectedLotModel.DeviceCount = lot.Devices.Count;
                selectedLotModel.LeadFrameCount = lot.LeadFrames.Count;
            }

            return new JsonResult(new { selectedLotModel = selectedLotModel });
        }

        public JsonResult OnPostGetSelectedDevice(string input)
        {
            var selectedDeviceModel = new SelectedDeviceModel();
            var id = Convert.ToInt32(input);
            var deviceReport = mMaterialTracking.GetDevice(id, mLotControl.CurrentLotId);

            selectedDeviceModel.Id = id;
            selectedDeviceModel.LotName = mMaterialTracking.GetLotName(mLotControl.CurrentLotId);
            selectedDeviceModel.State = deviceReport.ProcessState.ToString();
            selectedDeviceModel.Created = deviceReport.CreatedTime.ToString("dd-MM-yyyy HH:mm:ss");
            selectedDeviceModel.DambarInspection = deviceReport.InspectionDambarResult.ToString();
            selectedDeviceModel.Inspection = deviceReport.Inspection3DResult.ToString();
            selectedDeviceModel.Processed = deviceReport.EndTime != DateTime.MinValue ? deviceReport.EndTime.ToString("dd-MM-yyyy HH:mm:ss") : "Not available";

            return new JsonResult(new { selectedDeviceModel = selectedDeviceModel });
        }

        public JsonResult OnPostGetInspection3dResult()
        {
            var Inspection3dResult = new InspectionModel();
            return new JsonResult(new { Inspection3dResult = Inspection3dResult });
        }

        public JsonResult OnPostGetStates()
        {
            var states = new List<string>
            {
                StateBar.ConvertToString(mMagazineLoader.State),
                StateBar.ConvertToString(mPressModule.State),
                StateBar.ConvertToString(mRobotToTrayUnloader.State)
            };

            return new JsonResult(new { states });
        }
    }
}
