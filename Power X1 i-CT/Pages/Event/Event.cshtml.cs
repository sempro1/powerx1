using EventHandler.Lib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class EventModel : PageModel
    {
        private readonly EventManagerUpdater mUpdater;
        private readonly EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;

        public EventModel(EventManagerUpdater updater)
        {
            mUpdater = updater;
        }

        public JsonResult OnPostGetEvents()
        {
            return new JsonResult(new { events = mEventManager.ActiveEvents.Select(x => new Event(x)) });
        }

        public IActionResult OnPostGetSelectedEvent(string input)
        {
            var selectedEvent = mEventManager.ActiveEvents.FirstOrDefault(x => x.ID == Convert.ToInt32(input));

            if (selectedEvent != null)
                return new JsonResult(new { data = new Event(selectedEvent) });

            return new EmptyResult();
        }

        public IActionResult OnPostGetSelectedEventPage(string input)
        {
            var selectedEvent = mEventManager.ActiveEvents.FirstOrDefault(x => x.ID == Convert.ToInt32(input));

            if (selectedEvent != null)
                return Partial(selectedEvent.PageLocation);

            return new EmptyResult();
        }

        public JsonResult OnPostGetEventCounts()
        {
            var errorCount = 0;
            var assistCount = 0;
            var warningCount = 0;
            var informationCount = 0;

            // Create local copy else the foreach will crash when an event is added while we are reading it.
            List<EventHandler.Lib.Event> activeEvents = new();
            activeEvents.AddRange(mEventManager.ActiveEvents);

            foreach (var item in activeEvents)
            {
                switch (item.Type)
                {
                    case EventType.Error:
                        errorCount++;
                        break;
                    case EventType.Assist:
                        assistCount++;
                        break;
                    case EventType.Warning:
                        warningCount++;
                        break;
                    case EventType.Info:
                        informationCount++;
                        break;
                }
            }

            var result = new
            {
                errorCount = errorCount,
                assistCount = assistCount,
                warningCount = warningCount,
                informationCount = informationCount
            };

            return new JsonResult(result);
        }
    }
}
