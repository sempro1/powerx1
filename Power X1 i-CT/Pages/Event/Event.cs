﻿using EventHandler.Lib;

namespace Power_X1_iCT
{
    public class Event
    {
        public int ID { get; set; }
        public string Created { get; set; }
        public string Title { get; set; }
        public string Process { get; set; }
        public string Module { get; set; }
        public EventType Type { get; set; }
        public string Color => TypeToColor(Type);

        public Event(EventHandler.Lib.Event eventInput)
        {
            ID = eventInput.ID;
            Title = eventInput.Title;
            Type = eventInput.Type;
            Module = eventInput.Module;
            Process = eventInput.Process;
            Created = eventInput.Created.ToString("G");
        }

        public Event()
        {
            Created = string.Empty;
            Title = string.Empty;
            Process = string.Empty;
            Module = string.Empty;
        }

        private static string TypeToColor(EventType type)
        {
            switch (type)
            {
                case EventType.Info:
                    return "lightblue";
                case EventType.Assist:
                    return "orange";
                case EventType.Warning:
                    return "yellow";
                case EventType.Error:
                    return "red";
                default:
                    return string.Empty;

            }
        }
    }
}
