using MachineManager;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Power_X1_iCT.Pages
{
    public class PLCPageModel
    {
        private readonly PlcService mPLCService = PlcService.Instance;

        public void OnPostConnect()
        {
            mPLCService.Connect();
        }

        public void OnPostDisconnect()
        {
            mPLCService.Disconnect();
        }
    }
}
