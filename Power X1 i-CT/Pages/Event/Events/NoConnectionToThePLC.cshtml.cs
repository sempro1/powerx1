using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class NoConnectionToThePLC : PageModel
    {
        public PLCPageModel PLCPageModel => new PLCPageModel();

        public void OnPostConnect()
        {
            PLCPageModel.OnPostConnect();
        }

        public void OnPostDisconnect()
        {
            PLCPageModel.OnPostDisconnect();
        }
    }
}
