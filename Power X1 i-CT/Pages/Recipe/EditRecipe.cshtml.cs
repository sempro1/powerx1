using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using RecipeManager;
using System;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Product;

namespace Power_X1_iCT.Pages
{
    public class EditRecipeModel : PageModel
    {
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public string RecipeName { get; set; } = string.Empty;

        public EditRecipeDataModel Recipe { get; set; } = new();        

        public void OnGet(string recipe)
        {
            if (recipe != null)
            {
                RecipeName = recipe;
                Recipe = new EditRecipeDataModel(mRecipeManager.GetRecipe(RecipeName));
            }
        }

        public LocalRedirectResult OnPostSaveRecipe(EditRecipeDataModel input)
        {
            ProductRecipe recipe = input.ConvertToProductRecipe();

            IRecipeManager recipeManager = RecipeManager.RecipeManager.Instance;

            recipeManager.SaveRecipe(recipe);
            return LocalRedirect("/Recipe/Recipe");
        }

        public LocalRedirectResult OnPostCloseRecipe()
        {
            return LocalRedirect("/Recipe/Recipe");
        }       
    }
}
