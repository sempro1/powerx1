using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using RecipeManager;
using System.Collections.Generic;
using System.Linq;

namespace Power_X1_iCT.Pages
{
    public class RecipeModel : PageModel
    {
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public List<Model.RecipeModel> RecipeModels { get; set; } = new List<Model.RecipeModel>();
        public Model.RecipeModel CurrentRecipe { get; set; }

        public void OnGet()
        {
            UpdateRecipeModels();
        }

        private void UpdateRecipeModels()
        {
            var recipeNames = mRecipeManager.GetProductRecipeNames();

            foreach (var recipeName in recipeNames)
            {
                var recipe = mRecipeManager.GetRecipe(recipeName);

                RecipeModels.Add(new Model.RecipeModel(recipe));
            }

            CurrentRecipe = new(mRecipeManager.GetCurrentRecipe());
        }

        public LocalRedirectResult OnPostCopyRecipe(ChangeRecipeModel input)
        {
            if (CanCopyRecipeName(input.ToName))
            {
                if (input.ToName != string.Empty && input.FromName != string.Empty)
                    mRecipeManager.CopyRecipe(input.FromName, input.ToName);
            }
            return new LocalRedirectResult("/Recipe/Recipe");
        }

        public LocalRedirectResult OnPostDeleteRecipe(string input)
        {
            var recipeNames = mRecipeManager.GetProductRecipeNames();

            if (recipeNames.Contains(input))
            {
                mRecipeManager.DeleteRecipe(input);
            }
            return new LocalRedirectResult("/Recipe/Recipe");
        }

        public LocalRedirectResult OnPostRenameRecipe(ChangeRecipeModel input)
        {
            if (CanCopyRecipeName(input.ToName))
            {
                if (input.ToName != string.Empty && input.FromName != string.Empty)
                    mRecipeManager.RenameRecipe(input.FromName, input.ToName);
            }
            return new LocalRedirectResult("/Recipe/Recipe");
        }

        public JsonResult OnPostGetRecipeModels()
        {
            UpdateRecipeModels();

            return new JsonResult(new { recipeModels = RecipeModels });
        }

        public JsonResult OnPostValidateNewRecipename(string input)
        {
            bool result = CanCopyRecipeName(input);
            return new JsonResult(new { data = result });
        }

        private bool CanCopyRecipeName(string input)
        {
            var recipeNames = mRecipeManager.GetProductRecipeNames();
            bool result = input != null && input != string.Empty && !recipeNames.Any(x => x == input);
            return result;
        }
    }
}
