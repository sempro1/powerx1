using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Power_X1_iCT.Model;
using RecipeManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using X1.Recipe.Lib;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class CompareRecipeModel : PageModel
    {
        private IRecipeManager mRecipeManager = RecipeManager.RecipeManager.Instance;
        public string RecipeName { get; set; }
        public List<string> RecipeNames { get; private set; } = new List<string>();

        public List<RecipeValue> RecipeValues { get; private set; } = new List<RecipeValue>();

        public void OnGet()
        {
            RecipeName = "None";
            RecipeNames = mRecipeManager.GetProductRecipeNames().ToList();

            AddRecipeValuesOf<Trim>();

            AddRecipeValuesOf<Form>();
        }

        private void AddRecipeValuesOf<T>()
        {
            var propertyInfos = typeof(T).GetProperties(BindingFlags.Public|BindingFlags.Instance);

            // sort properties by name
            Array.Sort(propertyInfos,
                delegate (PropertyInfo propertyInfo1, PropertyInfo propertyInfo2)
                { return propertyInfo1.Name.CompareTo(propertyInfo2.Name); });

            foreach (var property in propertyInfos)
            {
                RecipeValues.Add(new RecipeValue(typeof(T).Name + "." + property.Name));
            }
        }

        public JsonResult OnPostGetRecipeNames()
        {
            RecipeNames = mRecipeManager.GetProductRecipeNames().ToList();

            return new JsonResult(RecipeNames);
        }

        public JsonResult OnPostGetRecipeRevisions(string input)
        {
            var recipes = mRecipeManager.GetAllRevisionsOfRecipe(input);

            return new JsonResult(recipes);
        }

        public JsonResult OnPostGetValuesOfRecipeName(string recipeName, string[] valueNames)
        {
            var recipes = mRecipeManager.GetAllRevisionsOfRecipe(recipeName);
            List<DataSet> dataSets = new();

            for (int i = 0; i < 10 && i < valueNames.Length; i++)
            {
                var valueName = valueNames[i];
                var dataSet = new DataSet(valueName);

                var data = new List<DataPoint>();
                foreach (var recipe in recipes)
                {
                    var value = GetPropValue<double>(recipe, valueName);
                    data.Add(new DataPoint(recipe.DateOfRevision.ToString("yyyy-MM-dd hh:mm:ss"), value.ToString()));
                }
                dataSet.Data = data;
                dataSets.Add(dataSet);
            }

            return new JsonResult(dataSets);
        }

        private object GetPropValue(object obj, string name)
        {
            foreach (string part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        private T GetPropValue<T>(object obj, string name)
        {
            object retval = GetPropValue(obj, name);
            if (retval == null) { return default; }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }
    }

    public class RecipeValue
    {
        public string Name { get; set; }
        public RecipeValue(string name)
        {
            Name = name;
        }
    }
}
