using CsvHelper;
using CsvHelper.Configuration;
using MaterialTracking;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Power_X1_iCT.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using DataPoint = Power_X1_iCT.Model.DataPoint;
using DataSet = Power_X1_iCT.Model.DataSet;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class PerformanceModel : PageModel
    {
        private static readonly CsvConfiguration mCsvConfig = new (CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = true,
            Delimiter = ",",
            MissingFieldFound = null
        };

        private static string mBinPath = Directory.GetCurrentDirectory() + "\\bin\\";
        public JsonResult OnPostGetTrimForceLoggingData(string input)
        {

            ForceLoggingProcessModel trimModel = GetTrimModel(input);
            return new JsonResult(new { data = trimModel });
        }
        private static ForceLoggingProcessModel GetTrimModel(string excelFileName)
        {
            ForceLoggingProcessModel trimModel = new("Trim")
            {
                ProductPosition = 23.05,
                ProductThickness = 1.0
            };
            trimModel.DataSets = GetDataSetsFromFileFor(excelFileName, trimModel.Name).ToList();

            return trimModel;
        }
        private static IEnumerable<DataSet> GetDataSetsFromFileFor(string excelFileName, string name)
        {
            string path = mBinPath + excelFileName;
            // Use CsvHelper to read csv file
            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, mCsvConfig);
            // Move to section name
            csv.Read();
            csv.ReadHeader();

            UpdateCsvTo(csv, name);

            csv.Read();
            csv.ReadHeader();

            return CreateDataSets(csv);
        }
        public JsonResult OnPostGetFormForceLoggingData(string input)
        {
            ForceLoggingProcessModel formModel = GetFormModel(input);
            return new JsonResult(new { data = formModel });
        }

        private static ForceLoggingProcessModel GetFormModel(string excelFileName)
        {
            ForceLoggingProcessModel formModel = new("Form")
            {
                ProductPosition = 21.5,
                ProductThickness = 1.0
            };

            formModel.DataSets = GetDataSetsFromFileFor(excelFileName, formModel.Name).ToList();

            return formModel;
        }
        private static List<DataSet> CreateDataSets(CsvReader csv)
        {
            Dictionary<int, DataSet> dataSets = new();
            var columnCount = csv.HeaderRecord.Length;

            for (int i = 1; i < columnCount; i++)
            {
                string title = csv.GetField<string>(csv.HeaderRecord[i]);
                dataSets.Add(i, new(title));
            }

            csv.Read();
            csv.ReadHeader();
            var cellPosition = csv.GetField<string>(0);
            var cellValue = csv.GetField<string>(1);
            try
            {
                while (cellValue != null &&
                    double.TryParse(cellPosition, out _) &&
                    double.TryParse(cellValue, out _))
                {
                    
                    for (int i = 1; i < columnCount; i++)
                    {

                        cellValue = csv.GetField<string>(i);
                        dataSets[i].Data.Add(new DataPoint(Convert.ToDouble(cellPosition).ToString(), Convert.ToDouble(cellValue).ToString()));
                    }
                    csv.Read();

                    // Get new values to check if these values are still doubles
                    cellPosition = csv.GetField<string>(0);
                    cellValue = csv.GetField<string>(1);
                }
            }
            catch(Exception)
            {
                // don't crash
            }

            return dataSets.Values.ToList();
        }
        /// <summary>
        /// Will update the csv file to the row which represents the string
        /// </summary>
        /// <param name="csv"></param>
        /// <param name="input"></param>
        private static void UpdateCsvTo(CsvReader csv, string input)
        {
            // Skip lines in file untill input string row
            while (csv.HeaderRecord.FirstOrDefault() != input && csv.HeaderRecord.FirstOrDefault() != "")
            {
                csv.Read();
                csv.ReadHeader();
            }
        }
    }
}
