using LineControl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Power_X1_iCT.Resources;
using System.Timers;

namespace StartStop
{
    [BindProperties]
    public class StartStopModel : PageModel
    {
        private ILogger mLog;
        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        private LotControl.Application.LotControl mLotControl = LotControl.Application.LotControl.Instance;

        public StartStopModel(ILogger<StartStopModel> logger, LineControlUpdater updater)
        {
            mLog = logger;
        }

        public JsonResult OnPostGetLineControlState()
        {
            return new JsonResult(new { state = ((StateBar.State)mLineControl.State).ToString() });
        }
        
        public void OnPostStart()
        {
            mLog.LogInformation("OnPostStart");

            if (mLineControl.State == (int)StateBar.State.Stopped)
            {
                mLineControl.Start();
            }
            else
            {
                mLineControl.Stop();
            }
        }

        public JsonResult OnPostGetDryRunState()
        {
            return new JsonResult(mLineControl.DryRunEnabled);
        }

        public JsonResult OnPostGetUPH()
        {
            return new JsonResult(mLotControl.UPH);
        }
    }
}
