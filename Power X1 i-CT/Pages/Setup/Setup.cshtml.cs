using LineControl;
using MagazineLoader;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PressModule.Updater;
using RobotToTrayUnloader.Updater;

namespace Power_X1_iCT.Pages
{
    [BindProperties]
    public class SetupModel : PageModel
    {
        private readonly LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;
        public bool NoAoi { get; set; }

        public bool SimulationEnabled { get; set; }

        public SetupModel(LineControlUpdater lineControlUpdater)
        {
        }


        public void OnPostChangeDryRunEnabled()
        {
            if (mLineControl.DryRunEnabled)
            {
                mLineControl.DisableDryRun();
            }
            else
            {
                mLineControl.EnableDryRun();
            }
        }

        public JsonResult OnPostGetDryRunEnabled()
        {
            return new JsonResult(mLineControl.DryRunEnabled);

        }
    }
}
