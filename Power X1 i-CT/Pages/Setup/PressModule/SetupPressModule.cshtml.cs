using MagazineLoader.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Power_X1_iCT.Model;
using Power_X1_iCT.Resources;
using PressModule.Updater;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]

    public class SetupPressModuleModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly PressModuleUpdater mUpdater;

        private PressModule.Application.PressModule mPressModule = PressModule.Application.PressModule.Instance;

        public PressModuleSettingsModel PressModuleSettingsModel { get; set; }
        public TrimmingSettingsModel TrimmingSettingsModel { get; set; }
        public FormingSettingsModel FormingSettingsModel { get; set; }
        public WasteHandlerSettingsModel WasteHandlerSettingsModel { get; set; }
        public bool SimulationEnabled { get; set; }
        public bool IsRunningProduction { get; set; }

        public SetupPressModuleModel(PressModuleUpdater pressModuleUpdater, ILogger<SetupPressModuleModel> logger)
        {
            mLog = logger;
            mUpdater = pressModuleUpdater;
            PressModuleSettingsModel = new PressModuleSettingsModel();
            TrimmingSettingsModel = new TrimmingSettingsModel();
            FormingSettingsModel = new FormingSettingsModel();
            WasteHandlerSettingsModel = new WasteHandlerSettingsModel();

            mPressModule.SimulationChanged += PressModule_SimulationChanged;
            PressModule_SimulationChanged(this, mPressModule.InSimulation);
        }

        private void PressModule_SimulationChanged(object sender, bool e)
        {
            SimulationEnabled = e;
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = ((StateBar.State)mPressModule.State).ToString() });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mPressModule.InSimulation);
        }

        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mPressModule.Initialize();
        }

        public void OnPostStart()
        {
            mLog.LogInformation("OnStart");
            mPressModule.Start();
        }

        public void OnPostStop()
        {
            mLog.LogInformation("OnStop");
            mPressModule.Stop();
        }

        public void OnPostTrim()
        {
            mLog.LogInformation("OnTrim");
            mPressModule.ExecuteTrim();
        }

        public void OnPostForm()
        {
            mLog.LogInformation("OnTrim");
            mPressModule.ExecuteForm();
        }

        public void OnPostDambar()
        {
            mLog.LogInformation("OnDambar");
            mPressModule.ExecuteDambar();
        }

        public void OnPostIndexerToPick()
        {
            mLog.LogInformation("OnIndexerToPick");
            mPressModule.ExecuteIndexerPick();
        }

        public void OnPostIndexerToPlace()
        {
            mLog.LogInformation("OnIndexerToPlace");
            mPressModule.ExecuteIndexerPlace();
        }

        public void OnPostDownloadTrimmingSettingsToPLC(TrimmingSettingsModel model)
        {
            model.Download();
        }

        public void OnPostDownloadFormingSettingsToPLC(FormingSettingsModel model)
        {
            model.Download();
        }

        public void OnPostDownloadWasteHandlerSettingsToPLC(WasteHandlerSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveTrimmingSettings(TrimmingSettingsModel model)
        {
            model.Save();
        }

        public void OnPostSaveWasteHandlerSettings(WasteHandlerSettingsModel model)
        {
            model.Save();
        }

        public void OnPostSaveFormingSettings(FormingSettingsModel model)
        {
            model.Save();
        }

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mPressModule.DisableSimulation();
            }
            else
            {
                mPressModule.EnableSimulation();
            }
        }
        public ActionResult OnPostSetVelocityPercentage(int percentage)
        {
            PressModuleSettingsModel.SetVelocityPercentage(percentage);

            return new JsonResult("Succes");
        }
    }
}

