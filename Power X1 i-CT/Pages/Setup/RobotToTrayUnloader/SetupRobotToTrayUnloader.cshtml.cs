using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Power_X1_iCT.Model;
using Power_X1_iCT.Resources;
using RobotToTrayUnloader.Updater;

namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]
    public class SetupRobotToTrayUnloaderModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly RobotToTrayUnloaderUpdater mUpdater;

        private RobotToTrayUnloader.Application.RobotToTrayUnloader mRobotToTrayUnloader = RobotToTrayUnloader.Application.RobotToTrayUnloader.Instance;

        public RobotToTrayUnloaderSettingsModel RobotToTrayUnloaderSettingsModel { get; set; }
        public InspectionSettingsModel InspectionSettingsModel { get; set; }
        public bool SimulationEnabled { get; set; }
        public bool IsRunningProduction { get; set; }
        public RobotSettingsModel RobotSettingsModel { get; set; } 
        public RobotPickSettingsModel RobotPickSettingsModel { get; set; }
        public RobotPlaceSettingsModel RobotPlaceSettingsModel { get; set; } 
        public RobotHomeSettingsModel RobotHomeSettingsModel { get; set; } 

        public SetupRobotToTrayUnloaderModel(RobotToTrayUnloaderUpdater robotToTrayUnloaderUpdater, ILogger<SetupRobotToTrayUnloaderModel> logger)
        {
            mLog = logger;
            mUpdater = robotToTrayUnloaderUpdater;
            RobotToTrayUnloaderSettingsModel = new RobotToTrayUnloaderSettingsModel();
            InspectionSettingsModel = new InspectionSettingsModel();
            RobotSettingsModel = new RobotSettingsModel();
            RobotPickSettingsModel = new RobotPickSettingsModel();
            RobotPlaceSettingsModel = new RobotPlaceSettingsModel();
            RobotHomeSettingsModel = new RobotHomeSettingsModel();
        }
        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = StateBar.ConvertToString(mRobotToTrayUnloader.State) });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mRobotToTrayUnloader.InSimulation);
        }

        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mRobotToTrayUnloader.Initialize();
        }

        public void OnPostStart()
        {
            mLog.LogInformation("OnStart");
            mRobotToTrayUnloader.Start();
        }

        public void OnPostStop()
        {
            mLog.LogInformation("OnStop");
            mRobotToTrayUnloader.Stop();
        }

        public void OnPostInspectionCommand()
        {
            mLog.LogInformation("OnInspection");
            mRobotToTrayUnloader.Inspect();
        }

        public void OnPostToInfeedCommand()
        {
            mLog.LogInformation("OnInfeed");
            mRobotToTrayUnloader.InspectToInfeed();
        }

        public void OnPostRobotPickCommand()
        {
            mLog.LogInformation("OnPick");
            mRobotToTrayUnloader.Pick();
        }
        public void OnPostRobotPlaceCommand()
        {
            mLog.LogInformation("OnPick");
            mRobotToTrayUnloader.Place();
        }
        public void OnPostRobotHomeCommand()
        {
            mLog.LogInformation("OnHome");
            mRobotToTrayUnloader.Home();
        }

        public void OnPostDownloadInspectionSettingsToPLC(InspectionSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveInspectionSettings(InspectionSettingsModel model)
        {
            model.Save();
        }

        public void OnPostDownloadRobotSettingsToPLC(RobotSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveRobotSettings(RobotSettingsModel model)
        {
            model.Save();
        }

        public void OnPostDownloadRobotPickSettingsToPLC(RobotPickSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveRobotPickSettings(RobotPickSettingsModel model)
        {
            model.Save();
        }

        public void OnPostDownloadRobotPlaceSettingsToPLC(RobotPlaceSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveRobotPlaceSettings(RobotPlaceSettingsModel model)
        {
            model.Save();
        }

        public void OnPostDownloadRobotHomeSettingsToPLC(RobotHomeSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveRobotHomeSettings(RobotHomeSettingsModel model)
        {
            model.Save();   
        }

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mRobotToTrayUnloader.DisableSimulation();
            }
            else
            {
                mRobotToTrayUnloader.EnableSimulation();
            }
        }
        public ActionResult OnPostSetVelocityPercentage(int percentage)
        {
            RobotToTrayUnloaderSettingsModel.SetVelocityPercentage(percentage);

            return new JsonResult("Succes");
        }
    }
}
