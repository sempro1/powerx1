using MagazineLoader;
using MagazineLoader.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Power_X1_iCT.Model;
using Power_X1_iCT.Resources;
using System.Threading.Tasks;

namespace Power_X1_iCT.Pages.Setup
{
    [BindProperties]
    public class SetupMagazineLoaderModel : PageModel
    {
        private readonly ILogger mLog;
        private readonly MagazineLoaderUpdater mUpdater;
        private MagazineLoaderModule mMagazineLoaderModule = MagazineLoaderModule.Instance;

        public MagazineLoaderSettingsModel MagazineLoaderSettingsModel { get; set; }

        public LoadMagazineSettingsModel LoadMagazineSettingsModel { get; set; }

        public ProvideMaterialSettingsModel ProvideMaterialSettingsModel { get; set; }

        public UnloadMagazineSettingsModel UnloadMagazineSettingsModel { get; set; }

        public bool SimulationEnabled { get; set; }

        public bool IsRunningProduction { get; set; }

        public SetupMagazineLoaderModel(MagazineLoaderUpdater magazineLoaderUpdater, ILogger<SetupMagazineLoaderModel> logger)
        {
            mLog = logger;
            mUpdater = magazineLoaderUpdater;
            MagazineLoaderSettingsModel = new MagazineLoaderSettingsModel();
            LoadMagazineSettingsModel = new LoadMagazineSettingsModel();
            ProvideMaterialSettingsModel = new ProvideMaterialSettingsModel();
            UnloadMagazineSettingsModel = new UnloadMagazineSettingsModel();
        }

        public JsonResult OnPostGetState()
        {
            return new JsonResult(new { state = StateBar.ConvertToString(mMagazineLoaderModule.State) });
        }

        public JsonResult OnPostGetStatusses()
        {
            return mUpdater.GetStatusses();
        }

        public JsonResult OnPostGetSimulationEnabled()
        {
            return new JsonResult(mMagazineLoaderModule.InSimulation);
        }

        public void OnPostInitialize()
        {
            mLog.LogInformation("OnInitialize");
            mMagazineLoaderModule.Initialize();
        }

        public void OnPostStart()
        {
            mMagazineLoaderModule.Start();
        }

        public void OnPostStop()
        {
            mMagazineLoaderModule.Stop();
        }

        public void OnPostLoadMagazine()
        {
            mMagazineLoaderModule.ExecuteLoadMagazine();
        }

        public void OnPostProvideMaterial()
        {
            mMagazineLoaderModule.ExecuteProvideMaterial();
        }

        public void OnPostUnloadMagazine()
        {
            mMagazineLoaderModule.ExecuteUnloadMagazine();
        }
        public void OnPostDownloadLoadMagazineSettingsToPLC(LoadMagazineSettingsModel model)
        {
            model.Download();
        }

        public IActionResult OnPostDownloadProvideMaterialSettingsToPLC()
        {
            ProvideMaterialSettingsModel.Download();

            // otherwise do some processing
            return RedirectToPage();
        }

        public void OnPostDownloadUnloadMagazineSettingsToPLC(UnloadMagazineSettingsModel model)
        {
            model.Download();
        }

        public void OnPostSaveLoadMagazineSettings()
        {
            LoadMagazineSettingsModel.Save();
        }
        public void OnPostSaveProvideMaterialSettings()
        {
            ProvideMaterialSettingsModel.Save();
        }

        public void OnPostSaveUnloadMagazineSettings()
        {
            UnloadMagazineSettingsModel.Save();
        }

        public IActionResult OnPostRefreshLoadMagazineSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public IActionResult OnPostRefreshProvideMaterialsSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public IActionResult OnPostRefreshUnloadMagazineSettingsFromRecipe()
        {
            // Should get last saved values from recipe

            return RedirectToPage();
        }

        public void OnPostSimulationChanged()
        {
            if (SimulationEnabled)
            {
                mMagazineLoaderModule.DisableSimulation();
            }
            else
            {
                mMagazineLoaderModule.EnableSimulation();
            }
        }

        public ActionResult OnPostSetVelocityPercentage(int percentage)
        {
            MagazineLoaderSettingsModel.SetVelocityPercentage(percentage);

            return new JsonResult("Succes");
        }
    }
}
