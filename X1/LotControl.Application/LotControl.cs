﻿using log4net;
using Logging;
using LotControl.Lib;
using LotControl.PLCInterfaces;
using MachineManager;
using MachineManager.Lib;
using MaterialTracking;
using MaterialTracking.Lib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LotControl.Application
{
    public class LotControl : SubscriptionBase
    {
        private int mProcessLeadFrameID;
        private int mDeviceProcessedID;

        private readonly TagModel mProcessLeadFrameIDTagModel = LotControlOpcUa.GetTagModel(x => x.LotControl.LotControlPcInterface.ToPc.ProcessLeadFrame.ID);
        private readonly TagModel mDeviceProcessedIDTagModel = LotControlOpcUa.GetTagModel(x => x.LotControl.LotControlPcInterface.ToPc.DeviceProcessed.ID);
        private readonly TagModel mCanLoadMaterialTagModel = LotControlOpcUa.GetTagModel(x => x.LotControl.LotControlPcInterface.ToPlc.CanLoadMaterial);

        private LineControl.Application.LineControl mLineControl = LineControl.Application.LineControl.Instance;

        public int UPH { get; set;}

        public int ProcessLeadFrameID
        {
            get => mProcessLeadFrameID;
            set
            {
                if (mProcessLeadFrameID != value)
                {
                    mProcessLeadFrameID = value;
                    NewLeadFrameArrived(mProcessLeadFrameID);                    
                }
            }
        }

        public int DeviceProcessedID
        {
            get => mDeviceProcessedID;
            set
            {
                if (mDeviceProcessedID != value)
                {
                    mDeviceProcessedID = value;
                    DeviceProcessed(mDeviceProcessedID);
                }
            }
        }

        private readonly ILog mLog = LogClient.Get();
        private static LotControl mLotControl;
        private MaterialTrackingApplication mMaterialTracking = MaterialTrackingApplication.Instance;
        private EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;

        public static LotControl Instance => mLotControl ??= new();

        public List<string> PlannedLots { get; set; } = new();

        public int CurrentLotId { get; set; }

        public LotControlMode Mode { get; set; } = LotControlMode.Continuous;

        public event EventHandler<string> LotAdded;
        public event EventHandler<string> LotRemoved;
        public event EventHandler<string> LotOpened;
        public event EventHandler<string> LotClosed;
        public event EventHandler<string> LotUpdated;

        public LotControl()
        {
            mMaterialTracking.DeviceUpdated += DeviceUpdated;
            Initialize();

            TagModelPathDictionary = new()
            {
                { mProcessLeadFrameIDTagModel, nameof(ProcessLeadFrameID) },
                { mDeviceProcessedIDTagModel,  nameof(DeviceProcessedID)}
            };

            AddSubscription();

            mPlcService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
        }

        private void Initialize()
        {
            mLog.Info("Initializing LotControl");

            var lots = mMaterialTracking.GetLots();
            if (lots.Any())
            {
                var newLotId = lots.Select(x => x.Id).Last();

                if (mMaterialTracking.GetLots().First(x => x.Id == newLotId).ProcessState == MaterialTracking.Lib.LotProcessState.Closed)
                    newLotId = 0;
                CurrentLotId = newLotId;
            }

            var currentLot = mMaterialTracking.GetLot(CurrentLotId);
            //if (currentLot.Virtual)
            {
                // When application is rebooted with a virtual lot, set all devices to removed
                // (as we dont have any information anymore)
                // Then a new lot will automatically be created and devices will be added to that new lot if still in machine
                foreach(var device in currentLot.Devices)
                {
                    if (device.ProcessState != DeviceProcessState.Processed)
                    {
                        mMaterialTracking.UpdateDevice(device.Id, CurrentLotId, DeviceProcessState.Removed);
                    }
                }
                // This should be replaced by a 'clear all material tracking data'.
                // Which clears all data in plc and pc of the current lot.
            }
            CheckCloseLot();
            CalculateUPH();
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            //Reconnected
            if (e == ConnectionState.Online)
            {
                mLog.Info("PLC reconnected: CheckCloseLot()");
                CheckCloseLot();
            }
        }

        private void NewLeadFrameArrived(int leadFrameID)
        {
            mLog.Info($"NewLeadFrameArrived: {leadFrameID}, lot id: {CurrentLotId}");

            if (CurrentLotId == 0)
            {
                OpenLot();
            }
            else
            {
                AddLeadFrameToLot(leadFrameID, CurrentLotId);
            }
        }

        private void AddLeadFrameToLot(int leadFrameID, int currentLotId)
        {
            mMaterialTracking.NewLeadFrame(leadFrameID, currentLotId);
        }

        private void DeviceUpdated(object sender, int e)
        {
            LotUpdated?.Invoke(this, mMaterialTracking.GetLotName(CurrentLotId));
        }

        public bool CanAddPlannedLot(string customerName)
        {
            return !PlannedLots.Any(x => x == customerName);
        }

        public void AddPlannedLot(string customerName)
        {
            if (CanAddPlannedLot(customerName))
                PlannedLots.Add(customerName);

            mEventManager.ResetEvent(LotControlEvents.NoPlannedLots.ID);

            LotAdded?.Invoke(this, customerName);
        }

        public void RemovePlannedLot(string customerName)
        {
            if (PlannedLots.Any(x => x == customerName))
            {
                PlannedLots.Remove(customerName);
                LotRemoved?.Invoke(this, customerName);
            }

            if (!PlannedLots.Any() && Mode == LotControlMode.Planned)
            {
                mEventManager.SetEvent(LotControlEvents.NoPlannedLots.ID);
            }
        }

        public void OpenLot()
        {
            var lotId = 1;
            if (mMaterialTracking.GetLots().Any())
            {
                lotId = mMaterialTracking.GetLots().Select(x => x.Id).Last();
                lotId += 1;
            }

            switch (Mode)
            {
                case LotControlMode.Continuous:
                    OpenLot(lotId);
                    break;
                case LotControlMode.Planned:
                    if (PlannedLots.Any())
                    {
                        var lotName = PlannedLots.First();
                        OpenLot(lotId, lotName);
                    }
                    break;
            }
        }

        private void OpenLot(int lotId, string customerName = "")
        {
            mLog.Info($"OpenLot: {lotId}, {customerName}");

            CurrentLotId = lotId;
            switch (Mode)
            {
                case LotControlMode.Continuous:
                    HandleContinuousOpenLot(lotId);
                    break;
                case LotControlMode.Planned:
                    HandlePlannedStripsOpenLot(lotId, customerName);
                    break;
            }
        }

        public void CloseLot()
        {
            mLog.Info($"CloseLot: {CurrentLotId}");
            mMaterialTracking.UpdateLot(CurrentLotId, LotProcessState.Closing);

            // Stop loading material
            mPlcService.WriteValue(mCanLoadMaterialTagModel, false);
        }

        public void DeviceProcessed(int deviceId)
        {
            mLog.Info($"DeviceProcessed: {deviceId}, lot id:{CurrentLotId}");

            mMaterialTracking.UpdateDevice(deviceId, CurrentLotId, DeviceProcessState.Processed);

            CalculateUPH();
            CheckCloseLot();
        }

        private void CalculateUPH()
        {
            //Only add lots which endtime is in the last hour or is still running
            var lots = mMaterialTracking.GetLots().ToList();
            var beginTime = DateTime.Now - TimeSpan.FromMinutes(60);
            lots.RemoveAll(x => (x.EndTime != DateTime.MinValue) && (x.EndTime < beginTime));

            var uph = 0;
            foreach (var lot in lots)
            {
                uph += lot.Devices.Where(x => x.EndTime > beginTime).Count();
            }
            UPH = uph;
        }

        private void CheckCloseLot()
        {
            var lot = mMaterialTracking.GetLot(CurrentLotId);
            if (lot.ProcessState == MaterialTracking.Lib.LotProcessState.Closing)
            {
                if (lot.Devices.All(x => x.ProcessState == DeviceProcessState.Processed || 
                x.ProcessState == DeviceProcessState.Removed || 
                x.ProcessState == DeviceProcessState.Rejected))
                {
                    mMaterialTracking.CloseLot(CurrentLotId);
                    LotClosed?.Invoke(this, lot.CustomerName);
                    CurrentLotId = 0;

                    mLog.Info($"Lot closed: {lot.CustomerName}: {lot.Id}");
                }
            }
            CheckCanLoadMaterial();
        }

        private void CheckCanLoadMaterial()
        {
            bool canLoadMaterial = false;
            var currentLot = mMaterialTracking.GetLot(CurrentLotId);

            switch (Mode)
            {
                case LotControlMode.Continuous:
                    canLoadMaterial = CurrentLotId == 0 ||  
                        currentLot.ProcessState != LotProcessState.Closing;
                    break;
                case LotControlMode.Planned:
                    canLoadMaterial = CurrentLotId != 0 || mMaterialTracking.GetLots().Any(x => x.ProcessState == MaterialTracking.Lib.LotProcessState.Planned);
                    break;
            }

            mPlcService.WriteValue(mCanLoadMaterialTagModel, canLoadMaterial);
        }

        private void HandleContinuousOpenLot(int lotId)
        {
            mLog.Info($"HandleContinuousOpenLot: {lotId}");
            var lotName = "Lot_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss");
            mMaterialTracking.OpenLot(lotId, lotName, mLineControl.DryRunEnabled);
            LotOpened?.Invoke(this, lotName);
        }

        private void HandlePlannedStripsOpenLot(int lotId, string customerName)
        {
            if (customerName != string.Empty)
            {
                if (PlannedLots.Any(x => x == customerName))
                {
                    mMaterialTracking.OpenLot(lotId, customerName);
                    RemovePlannedLot(customerName);
                }
                else
                {
                    mLog.Error("Cannot open lot as the lot name is not planned");
                }

            }
            else
            {
                if (PlannedLots.Any())
                {
                    string lotCustomerId = PlannedLots.First();
                    mMaterialTracking.OpenLot(lotId, lotCustomerId);
                    RemovePlannedLot(lotCustomerId);
                }
                else
                {
                    mLog.Error("Cannot open lot as there is no lot planned");
                }
            }
        }
    }
}
