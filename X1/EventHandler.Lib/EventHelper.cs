﻿namespace EventHandler.Lib
{
    public static class EventHelper
    {
        public static void SetEvent(this Event eventItem, IEventManager eventManager)
        {
            eventManager.SetEvent(eventItem.ID);
        }

        public static void ResetEvent(this Event eventItem, IEventManager eventManager)
        {
            eventManager.ResetEvent(eventItem.ID);
        }
    }
}
