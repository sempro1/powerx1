﻿using EventHandler.Lib;
using System;

namespace EventManager
{
    public class ExportEvent : IEquatable<ExportEvent>
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Occurences { get; set; }
        public EventType Type { get; set; }


        public ExportEvent(int id, string title, int occurences, EventType eventType)
        {
            ID = id;
            Title = title;
            Occurences = occurences;
            Type = eventType;
        }

        public bool Equals(ExportEvent other)
        {
            if (other is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (GetType() != other.GetType())
            {
                return false;
            }

            return (ID == other.ID);
        }
    }
}
