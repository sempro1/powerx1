﻿using EventHandler.Lib;
using System.Collections.Generic;
using System.Linq;


namespace EventManager
{
    public class TopOccurencesEvents
    {
        private List<ExportEvent> mEvents = new List<ExportEvent>();
        private List<ExportEvent> mFilteredEvents = new List<ExportEvent>();

        public TopOccurencesEvents(IEnumerable<ExportEvent> events = null)
        {
            if (events != null)
            {
                mEvents = events.ToList();
            }
        }

        public void AddEvent(ExportEvent addedEvent)
        {
            if (addedEvent != null)
                mEvents.Add(addedEvent);
        }

        public void AddEvent(int Id, string title, int occurences, EventType type)
        {
                mEvents.Add(new ExportEvent(Id, title, occurences, type));
        }

        public void RemoveEvent(ExportEvent removedEvent)
        {
            if (removedEvent != null)
                mEvents.Remove(removedEvent);
        }

        public void FilterEvents(IEnumerable<ExportEvent> filteredEvents)
        {
            if (filteredEvents != null)
                mFilteredEvents = filteredEvents.ToList();
        }

        public void ClearEvents()
        {
            mEvents.Clear();
        }

        public List<ExportEvent> GetTopOccurences(int amount)
        {
            var eventsToReport = new List<ExportEvent>();

            foreach (var eventInEvents in mEvents)
            {
                if (!mFilteredEvents.Contains(eventInEvents))
                    eventsToReport.Add(eventInEvents);
            }

            eventsToReport = eventsToReport.OrderByDescending(x => x.Occurences).ToList();

            return eventsToReport.Take(amount).ToList();
        }
    }
}
