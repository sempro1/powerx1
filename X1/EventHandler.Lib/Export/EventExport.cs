﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Layout.Element;
using iText.Layout.Properties;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.IO;
using CategoryAxis = OxyPlot.Axes.CategoryAxis;
using BarSeries = OxyPlot.Series.BarSeries;
using Image = iText.Layout.Element.Image;

namespace EventManager
{
    public static class EventExport
    {
        public static List<Paragraph> CreateEventParagraphs(List<ExportEvent> events)
        {
            List<Paragraph> output = new List<Paragraph>();

            output.Add(CreateTableParagraph(events));
            output.Add(CreateChartParagraph(events));
            
            return output;
        }

        private static Paragraph CreateTableParagraph(List<ExportEvent> events)
        {
            var tableParagraph = new Paragraph();
            // Create table of events
            float[] columnWidths = { 1, 2, 20 };

            Table table = new Table(UnitValue.CreatePercentArray(columnWidths));

            Cell[] headerCells =
                {
                    new Cell().SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(new DeviceRgb(250,164,43)).Add(new Paragraph("#")),
                    new Cell().SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(new DeviceRgb(250,164,43)).Add(new Paragraph("Count")),
                    new Cell().SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(new DeviceRgb(250,164,43)).Add(new Paragraph("Title"))
                };

            foreach (var cell in headerCells)
            {
                table.AddHeaderCell(cell);
            }

            Dictionary<int, int> points = new Dictionary<int, int>();

            int i = 0;
            foreach (var item in events)
            {
                points.Add(++i, item.Occurences);
                table.AddCell(new Cell().SetTextAlignment(TextAlignment.CENTER).SetFontColor(new DeviceRgb(0, 0, 0), 1).Add(new Paragraph(i.ToString())));
                table.AddCell(new Cell().SetTextAlignment(TextAlignment.CENTER).SetFontColor(new DeviceRgb(0, 0, 0), (float)0.7).Add(new Paragraph(item.Occurences.ToString())));
                table.AddCell(new Cell().SetTextAlignment(TextAlignment.LEFT).SetFontColor(new DeviceRgb(0, 0, 0), (float)0.7).Add(new Paragraph(item.Title)));
            }

            return tableParagraph.Add(table);
        }

        private static Paragraph CreateChartParagraph(List<ExportEvent> events)
        {
            var points = new Dictionary<int, int>();
            int i = 0;
            foreach (var item in events)
            {
                points.Add(++i, item.Occurences);
            }

            Paragraph paragraph;
            using (MemoryStream ms = new MemoryStream())
            {
                MemoryStream outputStream = new MemoryStream();
                GenerateChartToStream(points, outputStream);
                paragraph = new Paragraph().Add(new Image(ImageDataFactory.Create(outputStream.ToArray())));
            }

            return paragraph;
        }

        /// <summary>
        /// Generates the Chart
        /// </summary>
        /// <param name="input"></param>
        /// <param name="outputStream"></param>
        /// <param name="type"></param>
        private static void GenerateChartToStream(Dictionary<int,int> input, MemoryStream outputStream)
        {
            var model = new PlotModel();
            model.Axes.Add(new CategoryAxis());
            model.DefaultFont = "Verdana";

            var series = new BarSeries();
            series.FillColor = OxyColor.FromRgb(250, 164, 43);
            series.LabelPlacement = LabelPlacement.Middle;
            series.LabelFormatString = "{0}";

            model.Series.Add(series);

            foreach (var item in input)
            {
                var columnItem = new BarItem();
                columnItem.Value = item.Value;
                series.Items.Add(columnItem);
            }

            // Not yet supported in ASP.NET (only WPF and WinForms)
            //var pngExporter = new PngExporter { Width = 500, Height = 300 };
            //pngExporter.Export(model, outputStream);
        }
    }
}
