﻿using System;

namespace EventHandler.Lib
{
    public class Event : IEquatable<Event>
    {
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public string Title { get; set; }

        public string Process { get; set; }
        public string Module { get; set; }
        public EventType Type { get; set; }
        public string ObjectPath { get; set; }
        public string PageLocation { get; set; }

        /// <summary>
        /// Create new event
        /// </summary>
        /// <param name="id">ID of the event. Should be the same as the PLC event (if applicable)</param>
        /// <param name="title">Title that is shown in event</param>
        /// <param name="eventType">Type of event, this will set the coloring</param>
        /// <param name="source">PC/PLC who is the owner (and can reset the item) </param>
        /// <param name="module">In which module did the event occur </param>
        /// <param name="process">In what process did the event occur</param>
        public Event(int id, string title, EventType eventType, string process = "", string module = "", string objectPath = "", string pageLocation = "")
        {
            ID = id;
            Title = title;
            Type = eventType;
            Module = module;
            Process = process;
            ObjectPath = objectPath;
            PageLocation = pageLocation;
        }

        public bool Equals(Event other)
        {
            if (other is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (GetType() != other.GetType())
            {
                return false;
            }

            return (ID == other.ID);
        }
    }
}
