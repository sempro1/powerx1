﻿namespace EventHandler.Lib
{
    public interface IEventManager
    {
        void SetEvent(int eventId);
        void ResetEvent(int eventId);
    }
}