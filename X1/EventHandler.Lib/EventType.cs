﻿namespace EventHandler.Lib
{
    public enum EventType
    {
        Info,
        Assist,
        Warning,
        Error
    }
}
