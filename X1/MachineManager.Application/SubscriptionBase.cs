﻿using MachineManager.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MachineManager
{
    /// <summary>
    /// Used for all modules to add a subscription with multiple items to get updates from
    /// </summary>
    public class SubscriptionBase : ISubscription
    {
        protected PlcService mPlcService = PlcService.Instance;

        public Dictionary<ITagModel, string> TagModelPathDictionary { get; set; }

        /// <summary>
        /// Add the tagmodels to the plc subscription list
        /// Pre-condition: tagmodels must have been set prior
        /// </summary>
        public void AddSubscription()
        {
            if (TagModelPathDictionary != null)
            {
                mPlcService.AddSubscription(this, TagModelPathDictionary.Keys);
            }
        }

        public void OnSubscriptionValueChanged(ValueChangedModel e)
        {
            var propertyName = TagModelPathDictionary.FirstOrDefault(x => x.Key.ObjectPath == e.Name).Value;

            PlcService.SetProperty(this, propertyName, e.Value);
        }

        /// <summary>
        /// Returns the name of the property and its owner as a string.
        /// Example: '() => Class.Property' or '() => object.Property'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyLambda"></param>
        /// <returns>"Class.Property" or "Class.Property.Property"</returns>
        public static string GetNameOf<T>(Expression<Func<T>> propertyLambda)
        {
            var me = propertyLambda.Body as MemberExpression;

            if (me == null)
            {
                throw new ArgumentException("You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
            }
            // Get the main property with the use of reflection
            if (me.Expression.ToString().LastIndexOf(')') != 0)
            {
                var mainProperty = me.Expression.ToString().Substring(me.Expression.ToString().LastIndexOf(')') + 2);
                return mainProperty + "." + me.Member.Name;
            }
            return me.Member.Name;
        }

        public static string GetNameOf(string argument1, string argument2)
        {
            return argument1 + "." + argument2;
        }

        public void AddTagModelDictionaryOfProperty(string propertyName, Dictionary<ITagModel, string> tagModelPathDictionary)
        {
            foreach (var tagModel in tagModelPathDictionary)
            {
                TagModelPathDictionary.Add(tagModel.Key, GetNameOf(propertyName, tagModel.Value));
            }
        }
    }
}