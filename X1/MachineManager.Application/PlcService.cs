﻿using log4net;
using Logging;
using MachineManager.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MachineManager
{
    public enum OperationMode
    {
        Unknown = 0,
        Stopped = 4,
        Running = 8
    }

    public class PlcService : ISubscription
    {
        private readonly ILog mLog = LogClient.Get();
        private bool mIsRunningPLCSIM = false;
        private static PlcService mPlcService;
        private ConnectionState mConnectionState;
        private OperationMode mOperationMode;
        private string mSerialNumber;
        private OPCUA mOPCUAServer;
        private const string cDefaultIpAdress = "192.168.238.164";
        private const string cDefaultPortNumber = "4840";
        private Dictionary<string, ISubscription> mCallBackDictionary = new Dictionary<string, ISubscription>();
        private Dictionary<ISubscription, IEnumerable<ITagModel>> mCallBackSubscribtionsDictionary = new();
        private ITagModel mOperatingModeTagModel = new NamedTagModel("OperatingMode");
        private ITagModel mSerialNumberTagModel = new NamedTagModel("SerialNumber");

        public event EventHandler<ConnectionState> ConnectionStateChanged;

        public event EventHandler<OperationMode> OperationModeChanged;

        public event EventHandler<ValueChangedModel> SubscriptionValueChanged;

        public ConnectionState ConnectionState
        {
            get => mConnectionState;
            private set
            {
                if (mConnectionState != value)
                {
                    mConnectionState = value;
                    FireConnectionStateChanged();
                }
            }
        }

        public OperationMode OperationMode
        {
            get => mOperationMode;
            private set
            {
                if (mOperationMode != value)
                {
                    mOperationMode = value;
                    FireOperationModeChanged();
                }
            }
        }

        public bool IsRunningPLCSIM => mIsRunningPLCSIM;

        public string SerialNumber
        {
            get => mSerialNumber;
            private set
            {
                if (mSerialNumber != value)
                {
                    mSerialNumber = value;
                    FireOperationModeChanged();
                }
            }
        }

        /// <summary>
        /// PLC instance which controls all traffic to the PLC
        /// </summary>
        public static PlcService Instance
        {
            get
            {
                if (mPlcService == null)
                {
                    mPlcService = new PlcService();
                }
                return mPlcService;
            }
        }

        private PlcService()
        {
            mLog.Info($"> PlcService");

            mOPCUAServer = new OPCUA(cDefaultIpAdress, cDefaultPortNumber);
            mOPCUAServer.ValueChanged += MyOPCUAServer_ValueChanged;
            mOPCUAServer.ConnectionStateChanged += (sender, connection) => ConnectionState = connection;
            ConnectionState = mOPCUAServer.State;

            TagModelPathDictionary = new()
            {
                { mOperatingModeTagModel, nameof(OperationMode) },
                { mSerialNumberTagModel, nameof(SerialNumber) }
            };

            //AddSubscription();

            mLog.Info($"PlcService >");
        }
        public void Connect()
        {
            mOPCUAServer.Connect();

            if (mCallBackSubscribtionsDictionary.Any())
            {
                foreach (var tagModels in mCallBackSubscribtionsDictionary.Values)
                {
                    mOPCUAServer.AddSubscriptions(tagModels);
                }            
            }
        }

        public void Disconnect()
        {
            mOPCUAServer.Disconnect();
        }

        public void WriteValue(TagModel tagModel, object value)
        {
            mOPCUAServer.WriteValue(tagModel, value);
        }

        public async Task WriteValueAsync(TagModel tagModel, object value)
        {
            await mOPCUAServer.WriteValueAsync(tagModel, value);
        }

        public object ReadValue(TagModel tagModel)
        {
            return mOPCUAServer.ReadValue(tagModel);
        }

        public void AddSubscription(ISubscription sender, IEnumerable<ITagModel> tagModels)
        {
            if (!mCallBackSubscribtionsDictionary.Keys.Any(x => x == sender))
            {
                mCallBackSubscribtionsDictionary.Add(sender, tagModels);
            }

            mOPCUAServer.AddSubscriptions(tagModels);
        }
        private void MyOPCUAServer_ValueChanged(object sender, ValueChangedModel e)
        {
            if (mCallBackDictionary.Keys.Any(x => x == e.Name))
            {
                // When we know who wanted these updates -> only send them to him
                mCallBackDictionary[e.Name].OnSubscriptionValueChanged(e);
            }
            else if (mCallBackSubscribtionsDictionary.Values.Any(x => x.Any(y => y.ObjectPath == e.Name)))
            {
                // We have a subscribtion for this monitored item in our callback dictionary for subscribtions
                ISubscription subcribtor = mCallBackSubscribtionsDictionary.FirstOrDefault(x => x.Value.Any(y => y.ObjectPath == e.Name)).Key;
                subcribtor.OnSubscriptionValueChanged(e);
            }
            else
            {
                SubscriptionValueChanged?.Invoke(this, e);
            }
        }

        private void FireConnectionStateChanged()
        {
            mLog.Info($"Connection state has been changed to: {mConnectionState}");
            ConnectionStateChanged?.Invoke(this, mConnectionState);
        }

        private void FireOperationModeChanged()
        {
            OperationModeChanged?.Invoke(this, OperationMode);
        }

        public Dictionary<ITagModel, string> TagModelPathDictionary { get; set; }

        /// <summary>
        /// Add the tagmodels to the plc subscription list
        /// Pre-condition: tagmodels must have been set prior
        /// </summary>
        public void AddSubscription()
        {
            if (TagModelPathDictionary != null)
            {
                AddSubscription(this, TagModelPathDictionary.Keys);
            }
        }

        public void OnSubscriptionValueChanged(ValueChangedModel e)
        {
            var propertyName = TagModelPathDictionary.FirstOrDefault(x => x.Key.ObjectPath == e.Name).Value;

            SetProperty(this, propertyName, e.Value);
        }

        /// <summary>
        /// Helper method to set a value on a (nested) property
        /// </summary>
        /// <param name="target"></param>
        /// <param name="property"></param>
        /// <param name="setTo"></param>
        public static void SetProperty(object target, string property, object setTo)
        {
            var parts = property.Split('.');
            // if target object is List and target object no end target - 
            // we need cast to IList and get value by index
            if (target.GetType().Namespace == "System.Collections.Generic"
                && parts.Length != 1)
            {
                var targetList = (IList)target;
                var value = targetList[int.Parse(parts.First())];
                SetProperty(value, string.Join(".", parts.Skip(1)), setTo);
            }
            else
            {
                var prop = target.GetType().GetProperty(parts[0]);
                if (parts.Length == 1)
                {
                    // last property
                    prop.SetValue(target, setTo, null);
                }
                else
                {
                    // Not at the end, go recursive
                    var value = prop.GetValue(target);
                    SetProperty(value, string.Join(".", parts.Skip(1)), setTo);
                }
            }
        }
    }
}

