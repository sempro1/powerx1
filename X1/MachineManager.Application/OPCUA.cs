﻿using System;
using System.Collections.Generic;
using System.Threading;
using Opc.Ua.Client;
using Opc.Ua;
using Opc.Ua.Configuration;
using System.Linq;
using System.Threading.Tasks;
using MachineManager.Lib;
using Logging;
using log4net;

namespace MachineManager
{
    public class OPCUA
    {
        public event EventHandler<ValueChangedModel> ValueChanged;
        public string ServerAddress { get; set; }
        public string ServerPortNumber { get; set; }
        public bool SecurityEnabled { get; set; }
        public string MyApplicationName { get; set; }
        public static Session OPCSession { get; set; }
        public DateTime LastTimeSessionRenewed { get; set; }
        public DateTime LastTimeOPCServerFoundAlive { get; set; }
        public bool ClassDisposing { get; set; }
        public bool InitialisationCompleted { get; set; }

        public ConnectionState State
        {
            get => mState;
            set 
            {
                if (mState != value)
                {
                    mState = value;
                    ConnectionStateChanged?.Invoke(this, mState);
                }
            }
        }

        private readonly Dictionary<string, NodeId> mNodeIdCache = new();

        private static Dictionary<string, int> mNamespaceIndexes;
        private ConnectionState mState;
        private readonly ILog mLog = LogClient.Get();
        private const string cServerInterfaces = "ServerInterfaces";

        public event EventHandler<ConnectionState> ConnectionStateChanged;

        public OPCUA(string serverAddres, string serverport)
        {
            mLog.Debug($"> OPCUA: {serverAddres}, {serverport}");

            ServerAddress = serverAddres;
            ServerPortNumber = serverport;
            MyApplicationName = "MyApplication";
            State = ConnectionState.Offline;
            Connect();

            mLog.Debug($"OPCUA >");
        }

        public void Connect()
        {
            mLog.Debug($"> Connect");
            try
            {
                InitializeOPCUAClient();
                LastTimeOPCServerFoundAlive = DateTime.Now;
            }
            catch (Exception)
            {
                CloseSession();
                // No PLC Connection ?
                mLog.Error($"Failed to connect to OPC UA Server: No PLC connection?");
            }
            mLog.Debug($"Connect >");
        }

        public void Disconnect()
        {
            mLog.Info($"Disconnect");
            OPCSession.Close();
        }

        public static int GetNameSpaceForModule(string moduleName)
        {
            if (mNamespaceIndexes == null)
                mNamespaceIndexes = GenerateNamespaces();

            if (mNamespaceIndexes.Any(x => x.Key == moduleName))
            {
                return mNamespaceIndexes[moduleName];
            }
            return 0;
        }

        public void InitializeOPCUAClient()
        {
            //mLog.Error("Step 1 - Create application configuration and certificate.");
            var config = new ApplicationConfiguration()
            {
                ApplicationName = MyApplicationName,
                ApplicationUri = Utils.Format(@"urn:{0}:" + MyApplicationName + "", ServerAddress),
                ApplicationType = ApplicationType.Client,
                SecurityConfiguration = new SecurityConfiguration
                {
                    ApplicationCertificate = new CertificateIdentifier { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\MachineDefault", SubjectName = Utils.Format(@"CN={0}, DC={1}", MyApplicationName, ServerAddress) },
                    TrustedIssuerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Certificate Authorities" },
                    TrustedPeerCertificates = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\UA Applications" },
                    RejectedCertificateStore = new CertificateTrustList { StoreType = @"Directory", StorePath = @"%CommonApplicationData%\OPC Foundation\CertificateStores\RejectedCertificates" },
                    AutoAcceptUntrustedCertificates = true,
                    AddAppCertToTrustedStore = true
                },
                ServerConfiguration = new ServerConfiguration { MaxSubscriptionCount = 50 },
                TransportConfigurations = new TransportConfigurationCollection(),
                TransportQuotas = new TransportQuotas { OperationTimeout = 15000 },
                ClientConfiguration = new ClientConfiguration { DefaultSessionTimeout = 60000 },
                TraceConfiguration = new TraceConfiguration()
            };
            config.Validate(ApplicationType.Client).GetAwaiter().GetResult();
            if (config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
            {
                config.CertificateValidator.CertificateValidation += (s, e) => { e.Accept = (e.Error.StatusCode == StatusCodes.BadCertificateUntrusted); };
            }

            var application = new ApplicationInstance
            {
                ApplicationName = MyApplicationName,
                ApplicationType = ApplicationType.Client,
                ApplicationConfiguration = config
            };
            application.CheckApplicationInstanceCertificate(false, 2048).GetAwaiter().GetResult();

            var selectedEndpoint = CoreClientUtils.SelectEndpoint("opc.tcp://" + ServerAddress + ":" + ServerPortNumber + "", useSecurity: SecurityEnabled);

            // mLog.Error($"Step 2 - Create a session with your server: {selectedEndpoint.EndpointUrl} ");
            OPCSession = Session.Create(config, new ConfiguredEndpoint(null, selectedEndpoint, EndpointConfiguration.Create(config)), false, "", 60000, null, null).GetAwaiter().GetResult();
            OPCSession.KeepAlive += OPCSession_KeepAlive;
            OPCSession.KeepAliveInterval = 1000;
        }

        private async void OPCSession_KeepAlive(Session session, KeepAliveEventArgs e)
        {
            if (session.KeepAliveStopped)
                CloseSession();
            else
            {

                if (State == ConnectionState.Offline && State != ConnectionState.Connecting)
                {
                    State = ConnectionState.Connecting;
                    // Delay stating it is online for 3 seconds, because afetr this time it is ready to receive the values.
                    await Task.Delay(3000);
                    State = ConnectionState.Online;
                    mLog.Info($"ConnectionState: Online");
                }
            }
        }

        private static Dictionary<string, int> GenerateNamespaces()
        {
            var mLog = LogClient.Get();
            var result = new Dictionary<string, int>();

            NodeId nodeId = Objects.ObjectsFolder;
            ReferenceDescriptionCollection nextRefs;
            byte[] nextCp;

            try
            {
                //Get main ServerInterfaces
                ResponseHeader rh =
                    OPCSession.Browse(null,
                        null,
                        nodeId,
                        0u,
                        BrowseDirection.Forward,
                        ReferenceTypeIds.HierarchicalReferences,
                        true,
                        (uint)NodeClass.Variable | (uint)NodeClass.Object | (uint)NodeClass.Method,
                        out nextCp,
                        out nextRefs);

                ReferenceDescription serverInterface = nextRefs.FirstOrDefault(x => x.NodeId.Identifier.ToString() == cServerInterfaces);

                // Get nodes of ServerInterfaces
                OPCSession.Browse(
                        null,
                        null,
                        ExpandedNodeId.ToNodeId(serverInterface.NodeId, OPCSession.NamespaceUris),
                        0u,
                        BrowseDirection.Forward,
                        ReferenceTypeIds.HierarchicalReferences,
                        true,
                        (uint)NodeClass.Variable | (uint)NodeClass.Object | (uint)NodeClass.Method,
                        out nextCp,
                        out nextRefs);

                foreach (var module in nextRefs)
                {
                    result.Add(module.DisplayName.Text, module.NodeId.NamespaceIndex);
                }
            }
            catch(Exception)
            {
                mLog.Error("Unable to generate namespaces");
            }

            return result;
        }

        //class destructor
        ~OPCUA()
        {
            mLog.Error("Disposing OPC UA");
            ClassDisposing = true;
            try
            {

                OPCSession.Close();
                OPCSession.Dispose();
                OPCSession = null;
            }
            catch { }

        }

        public object ReadValue(TagModel tagHelper)
        {
            try
            {
                var readValue = new ReadValueId();
                readValue.NodeId = GetNode(tagHelper);
                readValue.AttributeId = Attributes.Value;

                var list = new List<ReadValueId>();
                list.Add(readValue);
                ReadValueIdCollection nodesToRead = new(list);

                OPCSession.Read(null, 100.0, new TimestampsToReturn(), nodesToRead,
                            out var results,
                            out var diagnosticInfos);

                if (results.Any())
                    return results.FirstOrDefault().Value;
            }
            catch(Exception)
            {
                mLog.Error("Unable to get data from plc for tag:" + tagHelper.ObjectPath);
                CloseSession();
            }

            return null;
        }

        public void WriteValue(TagModel tagHelper, object value)
        {
            WriteValueCollection nodesToWrite = new(new List<WriteValue>
            {
                new WriteValue
                {
                    NodeId = GetNode(tagHelper),
                    AttributeId = Attributes.Value,
                    Value = new DataValue { Value = value }
                }
            });

            try
            {
                OPCSession.Write(null, nodesToWrite,
                        out var resultsWrite,
                        out var diagnosticInfos);

                if (resultsWrite.Any() && StatusCode.IsBad(resultsWrite[0]))
                {
                    mLog.Error($"Software error: BadTypeMismatch? Item: {tagHelper.ObjectPath}, Value: {value}");
                }
            }
            catch (Exception)
            {
                CloseSession();
            }
        }

        private void CloseSession()
        {
            if (OPCSession != null)
                OPCSession.Close();

            State = ConnectionState.Offline;
        }

        public async Task<WriteResponse> WriteValueAsync(TagModel tagHelper, object value)
        {
            WriteResponse result = new();

            try
            {
                WriteValueCollection nodesToWrite = new WriteValueCollection(new List<WriteValue>
                {
                    new WriteValue
                    {
                        NodeId = GetNode(tagHelper),
                        AttributeId = Attributes.Value,
                        Value = new DataValue { Value = value }
                    }
                });

                result = await OPCSession.WriteAsync(null, nodesToWrite, new CancellationToken());
            }
            catch (Exception)
            {
                mLog.Error($"Failed to write value: {tagHelper.ObjectPath}, {value}");
                CloseSession();
                return result;
            }

            if (StatusCode.IsNotGood(result.Results[0].Code))
            {
                //throw new InvalidCastException();
                mLog.Error($"Invalid cast: make sure the PLC and PC items are equal: {tagHelper.ObjectPath}");
                // Check if the types of PC and PLC are equal
            }

            return result;
        }

        public NodeId GetNode(ITagModel tagHelper)
        {
            NodeId output = tagHelper.GetObjectPathOPCUa();
            BrowsePathCollection browsePaths = new();
            try
            {
                if (mNodeIdCache.Keys.Any(x => x.Contains(tagHelper.ObjectPath)))
                {
                    return mNodeIdCache[tagHelper.ObjectPath];
                }

                var startNodeId = new NodeId(Objects.ObjectsFolder);
                browsePaths = new BrowsePathCollection
                            {
                                new BrowsePath
                                {
                                    RelativePath = RelativePath.Parse(tagHelper.GetObjectPathOPCUa(), OPCSession.TypeTree, OPCSession.NamespaceUris, OPCSession.NamespaceUris),
                                    StartingNode = startNodeId
                                }
                            };
            }
            catch (Exception e)
            {
                CloseSession();
                mLog.Error("Failed to create browse path collection: " + e);
            }
            try
            {
                OPCSession.TranslateBrowsePathsToNodeIds(
                    null,
                    browsePaths,
                    out var results,
                    out var diagnosticInfos);

                output = "ns=" + results[0].Targets[0].TargetId.NamespaceIndex + ";i=" + results[0].Targets[0].TargetId.Identifier + "";

                mNodeIdCache.Add(tagHelper.ObjectPath, output);
            }
            catch (Exception e)
            {
                CloseSession();
                mLog.Error("Failed to translate path to a node id: " + e);
            }

            return output;
        }

        public void RemoveSubscribtion(string objectPath)
        {
            try
            {
                if (OPCSession.Subscriptions.Any(x => x.DisplayName == objectPath))
                {
                    OPCSession.RemoveSubscription(OPCSession.Subscriptions.FirstOrDefault(x => x.DisplayName == objectPath));
                }
            }
            catch (Exception e)
            {
                CloseSession();
                mLog.Error(e);
            }
        }

        public void AddSubscriptions(IEnumerable<ITagModel> tagModels)
        {
            try
            {
                //mLog.Debug("Step 4 - Create a subscription. Set a faster publishing interval if you wish.");
                var subscription = new Subscription(OPCSession.DefaultSubscription) { PublishingInterval = 100 };

                foreach (var item in tagModels)
                {
                    // In EventTagModels/NamedTagModels the identifier is the objectpath, so we dont need to get the node identifier from OPC UA Server
                    var startNodeId = (item is EventTagModel || item is NamedTagModel) ? item.GetObjectPathOPCUa() : GetNode(item);
                    var monitoritem = new MonitoredItem(subscription.DefaultItem) { DisplayName = item.ObjectPath, StartNodeId = startNodeId };

                    monitoritem.Notification += OnTagValueChange;
                    subscription.AddItem(monitoritem);
                }
                //mLog.Debug("Step 6 - Add the subscription to the session.");
                OPCSession.AddSubscription(subscription);
                if (OPCSession.SubscriptionCount < 20)
                {
                    subscription.Create();
                }
                else
                {
                    throw new Exception("Too many subscriptions");
                }
            }
            catch (Exception e)
            {
                CloseSession();
                mLog.Error($"Could not find part of path " + e);
            }
        }     

        public void OnTagValueChange(MonitoredItem item, MonitoredItemNotificationEventArgs e)
        {
            foreach (var value in item.DequeueValues())
            {
                if (value.Value != null)
                    mLog.DebugFormat("{0}: {1}, {2}, {3}", item.DisplayName, value.Value.ToString(), value.SourceTimestamp.ToLocalTime(), value.StatusCode);
                else
                    mLog.DebugFormat("{0}: {1}, {2}, {3}", item.DisplayName, "Null Value", value.SourceTimestamp, value.StatusCode);

                if (value.Value != null)
                {
                    FireValueChanged(item.DisplayName, value.Value);
                }
            }
            InitialisationCompleted = true;
        }

        private void FireValueChanged(string nameOfTag, object value)
        {
            ValueChanged?.Invoke(this, new ValueChangedModel(nameOfTag, value));
        }
    }
}
