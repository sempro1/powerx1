﻿using log4net.Core;
using log4net.Layout.Pattern;
using System;
using System.IO;

namespace Logging.Lib
{
    public class SemproPatternConverter : PatternLayoutConverter
    {
        private long nextSecond;
        private string cachedTimeStamp;
        protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
        {
            if (loggingEvent.TimeStamp.Ticks > nextSecond)
            {
                cachedTimeStamp = loggingEvent.TimeStamp.ToString("yyyy-MM-dd hh:mm:ss tt");
                nextSecond = loggingEvent.TimeStamp.Ticks + TimeSpan.TicksPerSecond; // ???
            }

            writer.Write(cachedTimeStamp);
        }
    }
}
