﻿using log4net;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]

namespace Logging
{
    public static class LogClient
    {
        public static ILog Get([CallerFilePath] string filename = "")
        {
            return LogManager.GetLogger(Path.GetFileNameWithoutExtension(filename));
        }
    }
}
