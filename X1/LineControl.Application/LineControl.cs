﻿using LineControl.Lib;
using LineControl.PLCInterfaces;
using MachineManager;
using MachineManager.Lib;
using System;

namespace LineControl.Application
{
    public class LineControl: SubscriptionBase
    {
        public event EventHandler<LineControlDataModel> DataChanged;

        private readonly TagModel mStateTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.State);
        private readonly TagModel mStatusTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.Status);
        private readonly TagModel mHasErrorTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.HasError);
        private readonly TagModel mDryRunEnabledTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.DryRunEnabled);
        private readonly TagModel mCoversLockedTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.CoversLocked);
        private readonly TagModel mStartCmdResultTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.StartCmdResult);
        private readonly TagModel mStopCmdResultTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.StopCmdResult);
        private readonly TagModel mLockCmdResultTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.LockCmdResult);
        private readonly TagModel mUnlockCmdResultTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPc.UnlockCmdResult);

        private readonly TagModel mStartCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.StartCommand);
        private readonly TagModel mStopCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.StopCommand);
        private readonly TagModel mLockCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.LockCommand);
        private readonly TagModel mUnlockCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.UnlockCommand);
        private readonly TagModel mEnableDryRunCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.EnableDryRunCommand);
        private readonly TagModel mDisableDryRunCommandTagModel = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.DisableDryRunCommand);
        private readonly TagModel mMasterReset = LineControlOpcUa.GetTagModel(x => x.LineControl.LineControlProcessPcInterface.ToPlc.MasterReset);      
        private LineControlDataModel mData;
        private EventManager.Application.EventManager mEventManager = EventManager.Application.EventManager.Instance;
        private static LineControl mLineControl;

        public static LineControl Instance => mLineControl ??= new LineControl();

        public LineControlDataModel Data => mData;
        

        public int State
        {
            get => mData.State;
            set 
            {
                if (mData.State != value)
                {
                    mData.State = value;                   
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public int Status
        {
            get => Data.Status;
            set
            {
                if (mData.Status != value)
                {
                    mData.Status = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public bool HasError
        {
            get => Data.HasError;
            set
            {
                if (mData.HasError != value)
                {
                    mData.HasError = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public bool DryRunEnabled
        {
            get => Data.DryRunEnabled;
            set
            {
                if (mData.DryRunEnabled != value)
                {
                    mData.DryRunEnabled = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public bool CoversLocked
        {
            get => Data.CoversLocked;
            set
            {
                if (mData.CoversLocked != value)
                {
                    mData.CoversLocked = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public int StartCmdResult
        {
            get => Data.StartCmdResult;
            set
            {
                if (mData.StartCmdResult != value)
                {
                    mData.StartCmdResult = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public int StopCmdResult
        {
            get => Data.StopCmdResult;
            set
            {
                if (mData.StopCmdResult != value)
                {
                    mData.StopCmdResult = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public int LockCmdResult
        {
            get => Data.LockCmdResult;
            set
            {
                if (mData.LockCmdResult != value)
                {
                    mData.LockCmdResult = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }

        public int UnlockCmdResult
        {
            get => Data.UnlockCmdResult;
            set
            {
                if (mData.UnlockCmdResult != value)
                {
                    mData.UnlockCmdResult = value;
                    DataChanged?.Invoke(this, mData);
                }
            }
        }


        public void Start()
        {
            PlcService.Instance.WriteValue(mStartCommandTagModel, true);
        }

        public void Stop()
        {
            PlcService.Instance.WriteValue(mStopCommandTagModel, true);
        }

        public void Lock()
        {
            PlcService.Instance.WriteValue(mLockCommandTagModel, true);
        }

        public void Unlock()
        {
            PlcService.Instance.WriteValue(mUnlockCommandTagModel, true);   
        }

        public void EnableDryRun()
        {
            PlcService.Instance.WriteValue(mEnableDryRunCommandTagModel, true);
        }

        public void DisableDryRun()
        {
            PlcService.Instance.WriteValue(mDisableDryRunCommandTagModel, true);
        }

        public void MasterReset()
        {
            PlcService.Instance.WriteValue(mMasterReset, true);
        }

        public LineControl()
        {
            mData = new LineControlDataModel();
            mEventManager.RegisterEvents(LineControlEvents.Events);

            mPlcService.ConnectionStateChanged += PlcService_ConnectionStateChanged;
            PlcService_ConnectionStateChanged(this, mPlcService.ConnectionState);

            TagModelPathDictionary = new()
            {
                { mStateTagModel, nameof(State) },
                { mStatusTagModel, nameof(Status) },
                { mHasErrorTagModel, nameof(HasError) },
                { mDryRunEnabledTagModel, nameof(DryRunEnabled) },
                { mCoversLockedTagModel, nameof(CoversLocked) },
                { mStartCmdResultTagModel, nameof(StartCmdResult) },
                { mStopCmdResultTagModel, nameof(StopCmdResult) },
                { mLockCmdResultTagModel, nameof(LockCmdResult) },
                { mUnlockCmdResultTagModel, nameof(UnlockCmdResult) }
            };

            AddSubscription();
        }

        private void PlcService_ConnectionStateChanged(object sender, ConnectionState e)
        {
            switch (e)
            {
                case ConnectionState.Offline:
                    mEventManager.SetEvent(LineControlEvents.NoConnectionToThePLC.ID);
                    mEventManager.ResetEvent(LineControlEvents.ConnectingToThePLC.ID);
                    break;
                case ConnectionState.Connecting:
                    mEventManager.ResetEvent(LineControlEvents.NoConnectionToThePLC.ID);
                    mEventManager.SetEvent(LineControlEvents.ConnectingToThePLC.ID);
                    break;
                case ConnectionState.Online:
                    mEventManager.ResetEvent(LineControlEvents.NoConnectionToThePLC.ID);
                    mEventManager.ResetEvent(LineControlEvents.ConnectingToThePLC.ID);
                    if (DryRunEnabled)
                        EnableDryRun();
                    break;
                default:
                    mEventManager.SetEvent(LineControlEvents.NoConnectionToThePLC.ID);
                    mEventManager.ResetEvent(LineControlEvents.ConnectingToThePLC.ID);
                    break;
            }
        }
    }
}
