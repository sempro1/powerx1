﻿namespace LineControl.Application
{
   public class LineControlDataModel
    {
        public int State { get; set; }
        public int Status { get; set; }
        public bool HasError { get; set; }
        public bool DryRunEnabled { get; set; }
        public bool CoversLocked{ get; set; }
        public int StartCmdResult { get; set; }
        public int StopCmdResult { get; set; }
        public int LockCmdResult { get; set; }
        public int UnlockCmdResult { get; set; }
    }
}
