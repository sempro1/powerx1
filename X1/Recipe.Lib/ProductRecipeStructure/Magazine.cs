﻿namespace X1.Recipe.Lib.Product
{
   public class Magazine
    {
        public int SlotCount { get; set; }
        public double MagazineWidth { get; set; }
        public double SlotOffset { get; set; }
        public double BottomSlotOffset { get; set; }
        public int DevicesPerSlot { get; set; }
    }
}
