﻿using System;
using System.Xml.Serialization;
using X1.Recipe.Lib.Product;

namespace X1.Recipe.Lib
{
    public class ProductRecipe
    {
        public DateTime DateOfRevision { get; set; } = DateTime.Now;
        [XmlIgnore]
        public int Revision { get; set; }
        public string CustomerName { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public string LastModifiedBy { get; set; } = "Unknown";
        public bool Released { get; set; } = false;
        public Device Device { get; set; } = new();
        public Magazine Magazine { get; set; } = new();
        public Tray Tray { get; set; } = new(); 
        public Trim Trim { get; set; } = new ();
        public Form Form { get; set; } = new();
        public WasteHandlerBin WasteHandlerBin { get; set; } = new();
        public Inspection Inspection { get; set; } = new();
        public Robot Robot { get; set; } = new();
        public RobotProcess RobotProcess { get; set; } = new();

        public ProductRecipe Clone()
        {
            var result = new ProductRecipe
            {
                Created = DateTime.Now,
                Revision = 0,
                Device = Device ?? new(),
                Magazine = Magazine ?? new(),
                Tray = Tray ?? new(),
                Trim = Trim ?? new(),
                Form = Form ?? new(),
                WasteHandlerBin = WasteHandlerBin ?? new(),
                Inspection = Inspection ?? new(),
                Robot = Robot ?? new(),
                RobotProcess = RobotProcess ?? new()
            };

            return result;
        }
    }
}
