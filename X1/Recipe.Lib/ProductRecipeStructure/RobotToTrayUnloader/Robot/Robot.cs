﻿using System.Collections.Generic;

namespace X1.Recipe.Lib.Product

{
    public class Robot
    {
        public int BaseCount { get; set; }
        public List<OutfeedLocationType> Locations { get; set; } = new();
        public int DefaultVelocityPercentage { get; set; }
    }
}
