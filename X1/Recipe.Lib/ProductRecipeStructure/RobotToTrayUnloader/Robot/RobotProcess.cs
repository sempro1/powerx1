﻿using X1.Recipe.Lib.Product;

namespace X1.Recipe.Lib
{
    public class RobotProcess
    {
        public Pick Pick { get; set; } = new();
        public Place Place { get; set; } = new();
    }
}