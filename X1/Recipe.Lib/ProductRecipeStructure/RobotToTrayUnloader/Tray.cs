﻿namespace X1.Recipe.Lib.Product
{
    public class Tray
    {
        public double PocketOffsetX { get; set; }
        public double PocketOffsetY { get; set; }
        public double TrayHeight { get; set; }
        public double PocketDepth { get; set; }
        public double TrayToFirstPocketOffsetX { get; set; }
        public double TrayToFirstPocketOffsetY { get; set; }
    }
}
