﻿namespace X1.Recipe.Lib.Machine
{
   public class LoadMagazine
    {
        public double ConveyorUpperZPosition { get; set; }
        public double StartPickOffsetZ { get; set; }
        public double EndPickOffsetZ { get; set; }
        public double PickY { get; set; }
        public double MagazineLiftSafePositionY { get; set; }
    }
}
