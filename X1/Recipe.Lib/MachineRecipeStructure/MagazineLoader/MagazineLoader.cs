﻿namespace X1.Recipe.Lib.Machine
{
   public class MagazineLoader
    {
        public LoadMagazine LoadMagazine { get; set; } = new();
        public ProvideMaterial ProvideMaterial { get; set; } = new();
        public UnloadMagazine UnloadMagazine { get; set; } = new();
    }
}
