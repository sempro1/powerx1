﻿namespace X1.Recipe.Lib.Machine
{
    public class UnloadMagazine
    {              
        public double LowerConveyorZPosition { get; set; }
        public double StartPlaceOffsetZPosition { get; set; }
        public double PlaceYPosition { get; set; }
        public double MagazineLiftSafePositionY { get; set; }

    }
}
