﻿using System;
using System.Xml.Serialization;

namespace X1.Recipe.Lib.Machine
{
    public class MachineRecipe
    {
        public DateTime DateOfRevision { get; set; }

        [XmlIgnore]
        public int Revision { get; set; }

        public MagazineLoader MagazineLoader { get; set; } = new();

        public RobotTrayUnloader RobotTrayUnloader {get; set; } = new();
    }
}
