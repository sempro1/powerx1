﻿using RobotToTrayUnloader.PLCInterface.Robot;

namespace X1.Recipe.Lib.Machine
{
    public class Home
    {
        public Position Position { get; set; } = new();
    }
}
