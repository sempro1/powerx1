﻿using EventHandler.Lib;
using System.Collections.Generic;

namespace LotControl.Lib
{
    public class LotControlEvents
    {
        private const string cModuleName = "LotControl";
        private const string cLineProcess = "System";

        public static Event NoPlannedLots = new(50, "No planned lots", EventType.Info, cLineProcess, cModuleName);

        public static IEnumerable<Event> Events = new List<Event>
        {
            NoPlannedLots
        };
    }
}
