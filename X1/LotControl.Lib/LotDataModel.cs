﻿using MaterialTracking.Lib;
using System;

namespace LotControl.Lib
{
    public class LotDataModel
    {
        public string CustomerName { get; set; }

        public string State { get; set; }

        public LotDataModel(string customerName, LotProcessState state)
        {
            CustomerName = customerName;
            State = state.ToString();
        }
    }
}
