﻿using System;

namespace LotControl.Lib
{
    public enum LotControlMode
    {
        Continuous,
        Planned
    }
}
