﻿using MachineManager.Lib;
using MachineManager.OpcUa;
using System;
using System.Linq.Expressions;

namespace LotControl.PLCInterfaces
{
    public class LotControlOpcUa
    {
        public static string GetPath<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return OpcUaPathCreator<ServerInterface>.GetPath(expression);
        }

        public static TagModel GetTagModel<TProperty>(Expression<Func<ServerInterface, TProperty>> expression)
        {
            return new TagModel(ServerInterface.ModuleId, GetPath(expression));
        }
    }
}
