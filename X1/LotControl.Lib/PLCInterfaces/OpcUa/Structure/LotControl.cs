﻿using MachineManager.Lib;

namespace LotControl.PLCInterfaces
{
    public class LotControl
    {
        [Name("lotControlPcInterface")]
        public LotControlPcInterface LotControlPcInterface { get; set; }

    }
}
