﻿using MachineManager;

namespace LotControl.PLCInterfaces
{
    public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceForModule(nameof(LotControl));
        public LotControl LotControl { get; set; }
    }
}