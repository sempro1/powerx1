﻿using MachineManager.Lib;

namespace LotControl.PLCInterfaces.OpcUa.LotControl
{
    public class ToPlc
    {
        [Name("canLoadMaterial")]
        public bool CanLoadMaterial {get; set;}
    }
}
