﻿using LotControl.PLCInterfaces.OpcUa.LotControl;
using MachineManager.Lib;

namespace LotControl.PLCInterfaces
{
   public class LotControlPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
