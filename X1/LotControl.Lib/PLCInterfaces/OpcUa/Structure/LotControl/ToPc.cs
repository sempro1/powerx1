﻿using MachineManager.Lib;

namespace LotControl.PLCInterfaces.OpcUa.LotControl
{ 
   public class ToPc
    {
        [Name("processLeadFrame")]
        public ProcessLeadFrame ProcessLeadFrame { get; set; }

        [Name("deviceProcessed")]
        public DeviceProcessed DeviceProcessed { get; set; }
    }
}
