﻿using System;
using System.Collections.Generic;
using EventHandler.Lib;
using EventManager;

namespace LineControl.Lib
{
    public interface IModule
    {
        event EventHandler<bool> HasErrorChanged;

        event EventHandler<int> StateChanged;

        void Start();

        void Stop();

        IEnumerable<Event> GetEvents();
    }
}
