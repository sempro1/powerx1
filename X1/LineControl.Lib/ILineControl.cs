﻿using System;

namespace LineControl
{
    public interface ILineControl
    {
        event EventHandler<int> StatusChanged;
        event EventHandler<int> StateChanged;

        bool AreCoversLocked { get; }

        bool AreCoversUnlocked { get; }

        void Start();

        void Stop();

        void LockCovers();

        void UnlockCovers();
        void MasterReset();
        void EnableDryRun();
        void DisableDryRun();
    }
}
