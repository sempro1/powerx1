﻿using EventHandler.Lib;
using System.Collections.Generic;

namespace LineControl.Lib
{
    public class LineControlEvents
    {
        private const string cModuleName = "LineControl";
        private const string cLineProcess = "System";

        public static Event NoConnectionToThePLC = new(1, "Connection to the PLC is lost", EventType.Error, cLineProcess, cModuleName, string.Empty, "Event/Events/NoConnectionToThePLC");
        public static Event ConnectingToThePLC = new(2, "Connecting to the PLC", EventType.Info, cLineProcess, cModuleName);

        public static IEnumerable<Event> Events = new List<Event>
        {
            NoConnectionToThePLC,
            ConnectingToThePLC
        };
    }
}
