﻿using MachineManager.Lib;

namespace LineControl.PLCInterfaces
{
    public class LineControl
    {
        [Name("lineControlProcessPcInterface")]
        public LineControlProcessPcInterface LineControlProcessPcInterface { get; set; }

    }
}
