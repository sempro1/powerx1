﻿using MachineManager;

namespace LineControl.PLCInterfaces
{
    public class ServerInterface
    {
        public static int ModuleId = OPCUA.GetNameSpaceForModule(nameof(LineControl));
        public LineControl LineControl { get; set; }
    }
}
