﻿using MachineManager.Lib;

namespace LineControl.PLCInterfaces.OpcUa
{
   public class ToPc
    {
        [Name("state")]
        public int State { get; set; }

        [Name("status")]
        public int Status { get; set; }

        [Name("hasError")]
        public int HasError { get; set; }

        [Name("dryRunEnabled")]
        public int DryRunEnabled { get; set; }

        [Name("coversLocked")]
        public int CoversLocked { get; set; }

        public int StartCmdResult { get; set; }
                
        public int StopCmdResult { get; set; }
            
        public int LockCmdResult { get; set; }

        public int UnlockCmdResult { get; set; }
    }
}
