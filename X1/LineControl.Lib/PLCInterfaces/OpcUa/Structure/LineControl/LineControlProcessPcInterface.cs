﻿using LineControl.PLCInterfaces.OpcUa;
using MachineManager.Lib;

namespace LineControl.PLCInterfaces
{
    public class LineControlProcessPcInterface
    {
        [Name("toPc")]
        public ToPc ToPc { get; set; }

        [Name("toPlc")]
        public ToPlc ToPlc { get; set; }
    }
}
