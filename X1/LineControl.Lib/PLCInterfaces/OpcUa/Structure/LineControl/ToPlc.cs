﻿using MachineManager.Lib;

namespace LineControl.PLCInterfaces.OpcUa
{
    public class ToPlc
    {
        [Name("startCommand")]
        public int StartCommand { get; set; }

        [Name("stopCommand")]
        public int StopCommand { get; set; }

        [Name("lockCommand")]
        public int LockCommand { get; set; }

        [Name("unlockCommand")]
        public int UnlockCommand { get; set; }

        [Name("enableDryRunCommand")]
        public int EnableDryRunCommand { get; set; }

        [Name("disableDryRunCommand")]
        public int DisableDryRunCommand { get; set; }

        [Name("masterReset")]
        public int MasterReset { get; set; }

    }
}
