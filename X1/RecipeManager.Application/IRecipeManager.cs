﻿using System;
using System.Collections.Generic;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace RecipeManager
{
    public interface IRecipeManager
    {
        event EventHandler<MachineRecipe> MachineRecipeLoaded;

        string ProductRecipeName { get; }

        event EventHandler<ProductRecipe> ProductRecipeChanged;

        event EventHandler<string> ProductRecipeAdded;

        event EventHandler<string> ProductRecipeRemoved;

        void LoadRecipe(string recipeName);
        bool CanLoadRecipe(string recipeName);
        void CopyRecipe(string fromRecipeName, string toRecipeName);
        void DeleteRecipe(string recipeName);
        ProductRecipe GetRecipe(string recipeName);
        void SaveRecipe(ProductRecipe recipe);
        void SaveRecipe(MachineRecipe recipe);
        IEnumerable<string> GetProductRecipeNames();
        MachineRecipe GetMachineRecipe();
        ProductRecipe GetCurrentRecipe();
        IEnumerable<ProductRecipe> GetAllRevisionsOfRecipe(string recipeName);
        void RenameRecipe(string fromName, string toName);
    }
}
