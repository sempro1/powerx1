﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using X1.Recipe.Lib;
using X1.Recipe.Lib.Machine;

namespace RecipeManager
{
    public class RecipeManager : IRecipeManager
    {
        private static IRecipeManager mRecipeManager;

        private string mProductRecipeName;
        //private readonly DataBaseAccess mDatabase = DataBaseAccess.Instance;
        private readonly string mRecipePath = Directory.GetCurrentDirectory() + @"\bin" + @"\Recipe";
        private const string cMachineRecipeName = "Machine";
        private readonly XmlSerializer mProductSerializer;
        private readonly XmlSerializer mMachineSerializer;
        readonly Dictionary<string, ProductRecipe> mProductRecipes = new();
        private MachineRecipe mMachineRecipe;
        public static IRecipeManager Instance => mRecipeManager ??= new RecipeManager();

        public string ProductRecipeName
        {
            get
            {
                return mProductRecipeName;
            }
            set
            {
                if (mProductRecipeName != value)
                {
                    mProductRecipeName = value;
                    ProductRecipeChanged?.Invoke(this, GetCurrentRecipe());
                }
            }
        }

        public event EventHandler<ProductRecipe> ProductRecipeChanged;
        public event EventHandler<string> ProductRecipeAdded;
        public event EventHandler<string> ProductRecipeRemoved;
        public event EventHandler<MachineRecipe> MachineRecipeLoaded;

        public RecipeManager()
        {
            mProductSerializer = new XmlSerializer(typeof(ProductRecipe));
            mMachineSerializer = new XmlSerializer(typeof(MachineRecipe));

            if (!Directory.Exists(mRecipePath))
                Directory.CreateDirectory(mRecipePath);

            GetProductRecipesFromDisk();           
            CleanOldRecipeFilesExceptLastRevision();
            LoadProductRecipe();
            LoadMachineRecipe();
        }

        private void LoadMachineRecipe()
        {
            var filenames = Directory.GetFiles(mRecipePath);

            if (!filenames.Any(x => x.Contains(cMachineRecipeName)))
            {
                mMachineRecipe = new MachineRecipe();
                SaveRecipe(mMachineRecipe);
            }
            else
            {
                Dictionary<string, int> filenameRevisionDictionary = new();

                foreach(var fileName in filenames.Where(x => x.Contains(cMachineRecipeName)))
                {
                    if (Path.GetExtension(fileName) == ".xml")
                    {
                        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                        var revision = Convert.ToInt32(fileNameWithoutExtension.Substring(fileNameWithoutExtension.LastIndexOf('_') + 1));
                        filenameRevisionDictionary.Add(fileName, revision);
                    }
                }

                var fileNameOfLatestMachineRecipe = filenameRevisionDictionary.OrderByDescending(x => x.Value).First();
                using (FileStream fileStream = new(fileNameOfLatestMachineRecipe.Key, FileMode.Open))
                {
                    var recipe = (MachineRecipe)mMachineSerializer.Deserialize(fileStream);
                    recipe.Revision = fileNameOfLatestMachineRecipe.Value;
                    mMachineRecipe = recipe;
                }
            }
            MachineRecipeLoaded?.Invoke(this, mMachineRecipe);
        }

        private void LoadProductRecipe()
        {
            //Restart with the last loaded recipe
            //var id = mDatabase.GetCurrentProductId();
            //ProductRecipeName = mDatabase.GetDescriptionFromProductId(id);
            // As we don't have a database yet -> use ProductA
            mProductRecipeName = "ProductA";
            if (!mProductRecipes.Keys.Any(x => x == mProductRecipeName))
            {
                // recipe does not exist on disk -> create it
                var productRecipe = new ProductRecipe();
                productRecipe.CustomerName = mProductRecipeName;
                SaveRecipe(productRecipe);
            }

            LoadRecipe(ProductRecipeName);
        }

        private void GetProductRecipesFromDisk()
        {
            Dictionary<string, int> filenameRevisionDictionary = GetFileNameRevisionDictionary();

            var latestRevisionsDictionary = new Dictionary<string, KeyValuePair<string, int>>();
            foreach (var productRecipeFileName in filenameRevisionDictionary)
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(productRecipeFileName.Key);
                var fileNameWithoutRevision = fileNameWithoutExtension.Substring(0, fileNameWithoutExtension.IndexOf('_'));

                if (latestRevisionsDictionary.ContainsKey(fileNameWithoutRevision))
                {
                    // When the new recipe has a higher revision than the previous recipe with this name
                    if (Math.Abs(productRecipeFileName.Value) > Math.Abs(latestRevisionsDictionary[fileNameWithoutRevision].Value))
                    {
                        latestRevisionsDictionary[fileNameWithoutRevision] = productRecipeFileName;
                    }
                }
                else
                {
                    latestRevisionsDictionary.Add(fileNameWithoutRevision, productRecipeFileName);
                }
            }

            // Only add productrecipes where the latest revision is a positive number
            foreach (var productRecipeFile in latestRevisionsDictionary.Where(x => x.Value.Value > 0))
            {
                var fileName = productRecipeFile.Value.Key;
                using (FileStream fileStream = new(fileName, FileMode.Open))
                {
                    var recipe = (ProductRecipe)mProductSerializer.Deserialize(fileStream);
                    recipe.Revision = productRecipeFile.Value.Value;

                    if (!mProductRecipes.Keys.Any(x => x == recipe.CustomerName))
                    {
                        mProductRecipes.Add(recipe.CustomerName, recipe);
                    }
                }
            }
        }

        private Dictionary<string, int> GetFileNameRevisionDictionary()
        {
            Dictionary<string, int> filenameRevisionDictionary = new Dictionary<string, int>();
            var filenames = Directory.GetFiles(mRecipePath);
            foreach (var fileName in filenames.Where(x => !x.Contains(cMachineRecipeName)))
            {
                if (Path.GetExtension(fileName) == ".xml")
                {
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                    var revision = Convert.ToInt32(fileNameWithoutExtension.Substring(fileNameWithoutExtension.LastIndexOf('_') + 1));
                    filenameRevisionDictionary.Add(fileName, revision);
                }
            }

            return filenameRevisionDictionary;
        }

        private void CleanOldRecipeFilesExceptLastRevision()
        {
            string[] files = Directory.GetFiles(mRecipePath);
            var deleteBefore = DateTime.Now.AddMonths(-6);

            Dictionary<string, List<string>> filesPerProduct = new Dictionary<string, List<string>>(); 
            foreach (string fileName in files)
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                var productName = fileNameWithoutExtension.Remove(fileNameWithoutExtension.LastIndexOf('_')); 
                if (!filesPerProduct.ContainsKey(productName))
                {
                    filesPerProduct.Add(productName, new List<string>());
                }
                filesPerProduct[productName].Add(fileName);
            }

            foreach (var productFiles in filesPerProduct)
            {
                if (productFiles.Value.Count > 1)
                {
                    for (int i = 0; i < productFiles.Value.Count - 1; i++)
                    {
                        var fileName = productFiles.Value[i];
                        FileInfo fileInfo = new FileInfo(fileName);
                        if (fileInfo.LastWriteTime < deleteBefore)
                            fileInfo.Delete();
                    }
                }
            }
        }

        public bool CanLoadRecipe(string recipeName)
        {
            return true;
        }

        public void LoadRecipe(string recipeName)
        {
            //mDatabase.SetCurrentProductId(mDatabase.GetProductIdFromDescription(recipeName));
            ProductRecipeName = recipeName;
        }

        public void CopyRecipe(string fromRecipeName, string toRecipeName)
        {
            var recipe = mProductRecipes[fromRecipeName];

            var productRecipe = recipe.Clone();
            productRecipe.CustomerName = toRecipeName;
            productRecipe.Released = false;
            productRecipe.LastModifiedBy = "Ewald Peters";
            SaveRecipe(productRecipe);
        }

        public void DeleteRecipe(string recipeName)
        {
            if (recipeName != null)
            {
                //mDatabase.DeleteProductId(mDatabase.GetProductIdFromDescription(recipeName));

                // Set revision to - to indicate it is removed
                if (mProductRecipes.Keys.Any(X => X == recipeName))
                {
                    var recipe = mProductRecipes[recipeName];
                    var fileName = Path.Combine(mRecipePath, $"{recipe.CustomerName}_-{++recipe.Revision}.xml");

                    using (StreamWriter outputFile = new(Path.Combine(mRecipePath, fileName)))
                    {
                        mProductSerializer.Serialize(outputFile, recipe);
                    }

                    mProductRecipes.Remove(recipe.CustomerName);
                }

                ProductRecipeRemoved?.Invoke(this, recipeName);
            }
        }

        public void SaveRecipe(ProductRecipe recipe)
        {
            if (!Directory.Exists(mRecipePath))
            {
                Directory.CreateDirectory(mRecipePath);
            }

            recipe.DateOfRevision = DateTime.Now;

            // When we create a recipe that is already on the disk, it has been created before
            // and could be removed in a later revision, thus should contain the highest revision number
            string filename;
            string filename_removed;
            do
            {
                recipe.Revision++;
                filename = $"{recipe.CustomerName}_{recipe.Revision}.xml";
                filename_removed = $"{recipe.CustomerName}_-{recipe.Revision}.xml";
            }
            while (File.Exists(Path.Combine(mRecipePath, filename)) ||
                    File.Exists(Path.Combine(mRecipePath, filename_removed)));

            using (StreamWriter outputFile = new(Path.Combine(mRecipePath, filename)))
            {
                mProductSerializer.Serialize(outputFile, recipe);
            }

            // When current active recipe is saved -> reload the values
            if (recipe.CustomerName == ProductRecipeName)
            {
                ProductRecipeChanged?.Invoke(this, GetCurrentRecipe());
                mProductRecipes[recipe.CustomerName] = recipe;
            }
            else if (!GetProductRecipeNames().Any(x => x == recipe.CustomerName))
            {
                // new product
                //mDatabase.CreateNewProduct(recipe.CustomerName, "");
                mProductRecipes.Add(recipe.CustomerName, recipe);
                ProductRecipeAdded?.Invoke(this, recipe.CustomerName);
            }          
            else if(!mProductRecipes.Keys.Any(x => x == recipe.CustomerName))
            {
                mProductRecipes.Add(recipe.CustomerName, recipe);
                ProductRecipeAdded?.Invoke(this, recipe.CustomerName);
            }
            else
            {
                mProductRecipes[recipe.CustomerName] = recipe;
            }
        }

        public void SaveRecipe(MachineRecipe recipe)
        {
            if (!Directory.Exists(mRecipePath))
            {
                Directory.CreateDirectory(mRecipePath);
            }

            recipe.DateOfRevision = DateTime.Now;
            recipe.Revision++;

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(mRecipePath, $"{cMachineRecipeName}_{recipe.Revision}.xml")))
            {
                mMachineSerializer.Serialize(outputFile, recipe);
            }
        }

        public ProductRecipe GetRecipe(string recipeName)
        {
            if (mProductRecipes.ContainsKey(recipeName))
            return mProductRecipes[recipeName];

            return null;
        }

        public IEnumerable<ProductRecipe> GetAllRevisionsOfRecipe(string recipeName)
        {
            var output = new List<ProductRecipe>();
            var fileNameRevisionDictionary = GetFileNameRevisionDictionary();

            foreach (var productRecipeFile in fileNameRevisionDictionary)
            {
                var fileName = productRecipeFile.Key;

                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
                var productRecipeName = fileNameWithoutExtension.Remove(fileNameWithoutExtension.LastIndexOf('_'));

                if (productRecipeName != recipeName)
                    continue;

                using (FileStream fileStream = new(fileName, FileMode.Open))
                {
                    var recipe = (ProductRecipe)mProductSerializer.Deserialize(fileStream);
                    recipe.Revision = productRecipeFile.Value;
                    output.Add(recipe);
                }
            }

            return output;
        }

        public IEnumerable<string> GetProductRecipeNames()
        {
            return mProductRecipes.Keys;
        }

        public ProductRecipe GetCurrentRecipe()
        {
            return GetRecipe(mProductRecipeName);
        }

        public void RenameRecipe(string fromName, string toName)
        {
            CopyRecipe(fromName, toName);
            DeleteRecipe(fromName);
        }

        MachineRecipe IRecipeManager.GetMachineRecipe()
        {
                return mMachineRecipe;
        }
    }
}
