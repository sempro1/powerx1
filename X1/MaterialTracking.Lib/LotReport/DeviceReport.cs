﻿using System;

namespace MaterialTracking.Lib.LotReport
{
    public class DeviceReport
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime EndTime { get; set; }
        public DeviceProcessState ProcessState { get; set; }
        public ValidationResults InspectionDambarResult { get; set; }
        public ValidationResults InspectionBottomResult { get; set; }
        public ValidationResults InspectionTopResult { get; set; }
        public ValidationResults Inspection3DResult { get; set; }
    }
}