﻿using System;
using System.Collections.Generic;

namespace MaterialTracking.Lib.LotReport
{
    public class LotReport
    {
        public int Id { get; set; }
        public string CustomerName { get; set; } = string.Empty;
        public bool Virtual { get; set; } = new();
        public DateTime CreatedTime { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public LotProcessState ProcessState { get; set; }
        public List<LeadFrameReport> LeadFrames  { get; set; } = new();
        public List<DeviceReport> Devices { get; set; } = new();

    }
}
