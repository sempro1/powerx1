﻿using System;
using System.Collections.Generic;

namespace MaterialTracking.Lib.LotReport
{
    public class LeadFrameReport
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime EndTime { get; set; }
        public LeadFrameProcessState ProcessState { get; set; }
        public List<int> Devices { get; set; } = new();
    }
}