﻿namespace MaterialTracking.Lib
{
    public enum LotProcessState
    {
        Planned,
        Opened,
        Closing,
        Closed
    }
}
