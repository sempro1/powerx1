﻿namespace MaterialTracking.Lib
{
    public enum DeviceProcessState
    {
        New,
        Loaded,
        Trimmed,
        Formed,       
        InspectedDambar,
        Singulated,
        InspectedFinal,
        Processed,
        Rejected,
        Removed,
    }
}
