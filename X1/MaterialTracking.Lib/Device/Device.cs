﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
    public class Device
    {
        public int ID { get; set; }

        [Name("processedState")]
        public int ProcessedState { get; set; }

        [Name("deviceResult")]
        public int DeviceResult { get; set; }

        [Name("inspectionResult")]
        public int InspectionResult { get; set; }

        [Name("dambarInspectionResult")]
        public int DambarInspectionResult { get; set; }

        

    }
}
