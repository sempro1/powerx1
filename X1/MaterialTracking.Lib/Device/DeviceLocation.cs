﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
    public class DeviceLocation
    {
        [Name("hasMaterial")]
        public bool HasMaterial {get; set;}

        [Name("device")]
        public Device Device { get; set; }
    }
}
