﻿namespace MaterialTracking
{
    public enum DeviceProcessTypeResult
    {
        InspectionDambarResult,
        InspectionBottomResult,
        InspectionTopResult,
        Inspection3DResult,
    }
}