﻿namespace MaterialTracking.Lib
{
    public enum LeadFrameProcessState
    {
        Planned,
        Loaded,
        Rejected,
        Processed
    }
}
