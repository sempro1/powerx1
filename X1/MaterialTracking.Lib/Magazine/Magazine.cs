﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
    public class Magazine
    {
        public int ID { get; set; }

        [Name("isEmpty")]
        public bool IsEmpty { get; set; }

        [Name("slotCount")]
        public int SlotCount { get; set; }

        [Name("slot")]
        public Slot Slot { get; set; }
    }
}