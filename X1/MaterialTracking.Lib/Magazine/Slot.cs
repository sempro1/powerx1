﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
    public class Slot
    {
        [Name("[0]")]
        public bool Slot0 { get; set; }
        [Name("[1]")]
        public bool Slot1 { get; set; }
        [Name("[2]")]
        public bool Slot2 { get; set; }
        [Name("[3]")]
        public bool Slot3 { get; set; }
        [Name("[4]")]
        public bool Slot4 { get; set; }
        [Name("[5]")]
        public bool Slot5 { get; set; }
        [Name("[6]")]
        public bool Slot6 { get; set; }
        [Name("[7]")]
        public bool Slot7 { get; set; }
        [Name("[8]")]
        public bool Slot8 { get; set; }
        [Name("[9]")]
        public bool Slot9 { get; set; }
        [Name("[10]")]
        public bool Slot10 { get; set; }
        [Name("[11]")]
        public bool Slot11 { get; set; }
        [Name("[12]")]
        public bool Slot12 { get; set; }
        [Name("[13]")]
        public bool Slot13 { get; set; }
        [Name("[14]")]
        public bool Slot14 { get; set; }
        [Name("[15]")]
        public bool Slot15 { get; set; }
        [Name("[16]")]
        public bool Slot16 { get; set; }
        [Name("[17]")]
        public bool Slot17 { get; set; }
    }
}