﻿
using MachineManager.Lib;

namespace MaterialTracking.Lib
{
    public class Tray
    {
        public int ID { get; set; }

        [Name("type")]
        public int Type { get; set; }

        [Name("isFull")]
        public bool IsFull { get; set; }

        [Name("position")]
        public int Position { get; set; }

        [Name("orientation")]
        public int Orientation { get; set; }

        [Name("rowCount")]
        public int RowCount { get; set; }

        [Name("columnCount")]
        public int ColumnCount { get; set; }

        [Name("devicePositions")]
        public TrayDeviceLocations TrayDeviceLocations { get; set; }

    }
}
