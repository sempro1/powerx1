﻿namespace MaterialTracking.Lib
{
    public enum TrayPosition
    {
        Tray1 = 1,
        Tray2,
        Tray3,
        Tray4,
        Tray5,
        Tray6
    }
}
