﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
   public class TrayLocation
    {
        [Name("hasTray")]
        public bool HasTray { get; set; }

        [Name("orientation")]
        public int Orientation { get; set; }

        [Name("type")]
        public int Type { get; set; }

        [Name("tray")]
        public Tray Tray { get; set; }

    }
}
