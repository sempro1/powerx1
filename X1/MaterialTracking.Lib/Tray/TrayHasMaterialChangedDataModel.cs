﻿namespace MaterialTracking.Lib
{
    public class TrayHasMaterialChangedDataModel
    {
        public TrayHasMaterialChangedDataModel(TrayPosition trayPosition, int deviceId, bool trayPositionHasMaterial, int trayDeviceCount)
        {
            TrayPosition = trayPosition;
            DeviceId = deviceId;
            TrayPositionHasMaterial = trayPositionHasMaterial;
            TrayDeviceCount = trayDeviceCount;
        }

        public TrayPosition TrayPosition { get; set; }
        public int DeviceId { get; set; }
        public bool TrayPositionHasMaterial { get; set; }
        public int TrayDeviceCount { get; set; }
    }
}
