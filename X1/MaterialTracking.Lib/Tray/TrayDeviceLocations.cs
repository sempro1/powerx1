﻿using MachineManager.Lib;

namespace MaterialTracking.Lib
{
   public class TrayDeviceLocations
    {
        [Name("[1,1]")]
        public DeviceLocation DeviceLocation1 {get; set;}

        [Name("[1,2]")]
        public DeviceLocation DeviceLocation2 { get; set; }

        [Name("[1,3]")]
        public DeviceLocation DeviceLocation3 { get; set; }

        [Name("[1,4]")]
        public DeviceLocation DeviceLocation4 { get; set; }

        [Name("[2,1]")]
        public DeviceLocation DeviceLocation5 { get; set; }

        [Name("[2,2]")]
        public DeviceLocation DeviceLocation6 { get; set; }

        [Name("[2,3]")]
        public DeviceLocation DeviceLocation7 { get; set; }

        [Name("[2,4]")]
        public DeviceLocation DeviceLocation8 { get; set; }

        [Name("[3,1]")]
        public DeviceLocation DeviceLocation9 { get; set; }

        [Name("[3,2]")]
        public DeviceLocation DeviceLocation10 { get; set; }

        [Name("[3,3]")]
        public DeviceLocation DeviceLocation11 { get; set; }

        [Name("[3,4]")]
        public DeviceLocation DeviceLocation12 { get; set; }
    }
}
