﻿namespace MaterialTracking.Lib
{
    public class HasTrayChangedDataModel
    {
        public HasTrayChangedDataModel(TrayPosition trayPosition, int trayId)
        {
            TrayPosition = trayPosition;
            TrayId = trayId;

            HasMaterial = trayId != 0;
        }

        public TrayPosition TrayPosition { get; set; }
        public int TrayId { get; set; }
        public bool HasMaterial { get; set; }
    }
}
