﻿using log4net;
using Logging;
using MaterialTracking.Lib;
using MaterialTracking.Lib.LotReport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MaterialTracking
{
    public class MaterialTrackingApplication
    {
        private static MaterialTrackingApplication mMaterialTracking;
        private readonly ILog mLog = LogClient.Get();
        private readonly string mLotReportPath = Directory.GetCurrentDirectory() + @"\bin" + @"\LotReports";
        private readonly XmlSerializer mLotReportSerializer;
        private Dictionary<int, LotReport> mLotReports = new();
        private object mSerializeLockObject = new();
        private object mLotListLockObject = new();

        public static MaterialTrackingApplication Instance => mMaterialTracking ??= new MaterialTrackingApplication();
        public EventHandler<int> DeviceUpdated;

        public MaterialTrackingApplication()
        {
            mLotReportSerializer = new XmlSerializer(typeof(LotReport));

            if (!Directory.Exists(mLotReportPath))
                Directory.CreateDirectory(mLotReportPath);

            GetLotReportsFromDisk();
            CleanOldLotReports();
        }

        private void GetLotReportsFromDisk()
        {
            var filenames = Directory.GetFiles(mLotReportPath);

            foreach (var fileName in filenames)
            {
                if (Path.GetExtension(fileName) == ".xml")
                {
                    using (FileStream fileStream = new(fileName, FileMode.Open))
                    {
                        lock (mSerializeLockObject)
                        {
                            var lotReport = (LotReport)mLotReportSerializer.Deserialize(fileStream);

                            lock (mLotListLockObject)
                            {
                                if (!mLotReports.Any(x => x.Key == lotReport.Id))
                                {
                                    mLotReports.Add(lotReport.Id, lotReport);
                                }
                            }
                        }
                    }
                }
            }          
        }

        public IEnumerable<LotReport> GetLots()
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Any())
                    return mLotReports.Values;
            }
            return new List<LotReport>();
        }

        private void CleanOldLotReports()
        {
            string[] files = Directory.GetFiles(mLotReportPath);
            var deleteBefore = DateTime.Now.AddMonths(-6);

            foreach (string fileName in files)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                if (fileInfo.LastWriteTime < deleteBefore)
                    fileInfo.Delete();
            }
        }

        public LotReport GetLot(string lotName)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Values.Any(x => x.CustomerName == lotName))
                {
                    return mLotReports.Values.First(x => x.CustomerName == lotName);
                }
                else
                {
                    mLog.Error("Unable to get lot report: lotName is unknown");
                }
            }
            return new LotReport();
        }

        public LotReport GetLot(int lotId)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Keys.Any(x => x == lotId))
                {
                    return mLotReports.First(x => x.Key == lotId).Value;
                }
                else
                {
                    mLog.Error("Unable to get lot report: lotName is unknown");
                }
            }
            return new LotReport();
        }

        public bool NewLot(int id, string name, bool virtualLot)
        {
            bool output = false;
            try
            {
                var lotReport = new LotReport
                {
                    Id = id,
                    CustomerName = name,
                    Virtual = virtualLot,
                    ProcessState = LotProcessState.Opened,
                    CreatedTime = DateTime.Now
                };

                lock (mLotListLockObject)
                {
                    if (!mLotReports.Any(x => x.Key == id))
                    {
                        mLotReports.Add(id, lotReport);
                    }
                    else
                    {
                        mLotReports[id] = lotReport;
                    }
                }

                SaveLotReport(id);

                output = true;
            }
            catch(Exception e)
            {
                mLog.Error($"Failed to update planned lot: {e}");
            }
            return output;
        }

        public DeviceReport GetDevice(int id, int lotId)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == lotId))
                {
                    var lotReport = mLotReports[lotId];

                    if (lotReport.Devices.Any(x => x.Id == id))
                    {
                        return lotReport.Devices.First(x => x.Id == id);
                    }
                }
            }

            return new DeviceReport();
        }
        /// <summary>
        /// Should only be used if lot report is unknown as this cycles through all known lot reports
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lotId"></param>
        /// <returns></returns>
        public DeviceReport GetDevice(int id)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Values.Any())
                {
                    foreach (var lotReport in mLotReports.Values)
                    {
                        if (lotReport.Devices.Any())
                        {
                            foreach (var device in lotReport.Devices)
                            {
                                if (device.Id == id)
                                {
                                    return device;
                                }
                            }
                        }
                    }
                }
            }

            return new DeviceReport();
        }

        private void SaveLotReport(int id)
        {
            if (!Directory.Exists(mLotReportPath))
            {
                Directory.CreateDirectory(mLotReportPath);
            }

            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == id))
                {
                    var lotReport = mLotReports[id];
                    using (StreamWriter outputFile = new(Path.Combine(mLotReportPath, $"{lotReport.CustomerName}.xml")))
                    {
                        lock (mSerializeLockObject)
                        {
                            mLotReportSerializer.Serialize(outputFile, lotReport);
                        }
                    }
                }
                else
                {
                    mLog.Error("Unable to save lot report to disk: Id is unknown");
                }
            }
        }

        public string GetLotName(int id)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == id))
                {
                    return mLotReports[id].CustomerName;
                }
                else
                {
                    mLog.Error("Unable to get lot report: Id is unknown");
                }
            }
            return string.Empty;
        }

        public bool UpdateLot(int id, LotProcessState state)
        {
            bool output = false;

            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == id))
                {
                    var lotReport = mLotReports[id];
                    lotReport.ProcessState = state;

                    if (state == LotProcessState.Opened)
                        lotReport.StartTime = DateTime.Now;

                    if (state == LotProcessState.Closed)
                        lotReport.EndTime = DateTime.Now;

                    mLog.Info($"Lot updated: {id}, {state}");
                }
                else
                {
                    mLog.Error("Unable to save lot report to disk: Id is unknown");
                }
            }

            SaveLotReport(id);

            return output;
        }
        public bool NewLeadFrame(int id, int lotId, string customerName = "")
        {
            bool output = false;

            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == lotId))
                {
                    var lotReport = mLotReports[lotId];

                    if (!lotReport.LeadFrames.Any(x => x.Id == id))
                    {
                        var leadframeReport = new LeadFrameReport
                        {
                            Id = id,
                            CreatedTime = DateTime.Now,
                            ProcessState = LeadFrameProcessState.Loaded
                        };
                        lotReport.LeadFrames.Add(leadframeReport);
                    }
                }
            }
            return output;
        }

        public bool UpdateLeadFrame(int id, int lotId, LeadFrameProcessState state)
        {
            bool output = false;

            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == lotId))
                {
                    var lotReport = mLotReports[lotId];

                    if (!lotReport.LeadFrames.Any(x => x.Id == id))
                    {
                        NewLeadFrame(id, lotId);
                    }
                    else
                    {
                        var LeadFrameReport = lotReport.LeadFrames.First(x => x.Id == id);
                        LeadFrameReport.ProcessState = state;

                        if (state == LeadFrameProcessState.Processed)
                        {
                            LeadFrameReport.EndTime = DateTime.Now;
                        }
                    }
                }
            }

            return output;
        }

        public bool NewDevice(int id, int leadframeId, int lotId)
        {
            bool output = false;

            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == lotId))
                {
                    var lotReport = mLotReports[lotId];
                    LeadFrameReport leadFrame;

                    if (lotReport.LeadFrames.Any(x => x.Id == leadframeId))
                    {
                        leadFrame = lotReport.LeadFrames.First(x => x.Id == leadframeId);
                        leadFrame.Devices.Add(id);
                    }
                    else
                    {
                        NewLeadFrame(leadframeId, lotId);

                        if (lotReport.LeadFrames.Any(x => x.Id == leadframeId))
                        {
                            leadFrame = lotReport.LeadFrames.First(x => x.Id == leadframeId);
                            leadFrame.Devices.Add(id);
                        }
                    }

                    if (!lotReport.Devices.Any(x => x.Id == id))
                    {
                        var deviceReport = new DeviceReport
                        {
                            Id = id,
                            CreatedTime = DateTime.Now,
                            ProcessState = DeviceProcessState.Loaded
                        };
                        lotReport.Devices.Add(deviceReport);

                        DeviceUpdated?.Invoke(this, id);
                    }
                }
                else
                {
                    // No lot found;
                }
            }
            return output;
        }

        public bool UpdateDevice(int id, int lotId, DeviceProcessState state)
        {
            bool output = false;

            var deviceReport = GetDeviceReport(id, lotId);
            deviceReport.ProcessState = state;

            if (state == DeviceProcessState.Processed)
            {
                deviceReport.EndTime = DateTime.Now;
                var lotReport = GetLot(lotId);
                // Check if all devices in the lead frame are processed to update the leadframe state
                var leadFrameReport = lotReport.LeadFrames.FirstOrDefault(x => x.Devices.Any(x => x == id));

                if (leadFrameReport != null)
                {
                    var devicesInLeadFrame = leadFrameReport.Devices.Count;
                    var devicesProcessed = 0;
                    foreach (var device in leadFrameReport.Devices)
                    {
                        var deviceReportFromLeadFrame = lotReport.Devices.First(x => x.Id == device);
                        if (deviceReportFromLeadFrame.ProcessState == DeviceProcessState.Processed)
                        {
                            devicesProcessed++;
                            continue;
                        }
                        break;
                    }

                    if (devicesInLeadFrame == devicesProcessed)
                    {
                        leadFrameReport.ProcessState = LeadFrameProcessState.Processed;
                    }
                }
            }
            DeviceUpdated?.Invoke(this, id);

            mLog.Info($"Device updated: {id}, lot id: {lotId}, state: {state}");

            return output;
        }

        public void UpdateDevice(int id, int lotId, DeviceProcessTypeResult deviceProcessTypeResult, ValidationResults result)
        {
            lock (mLotListLockObject)
            {
                if (mLotReports.Any(x => x.Key == lotId))
                {
                    var deviceReport = GetDeviceReport(id, lotId);

                    switch (deviceProcessTypeResult)
                    {
                        case DeviceProcessTypeResult.InspectionDambarResult:
                            deviceReport.InspectionDambarResult = result;
                            break;
                        case DeviceProcessTypeResult.InspectionBottomResult:
                            deviceReport.InspectionBottomResult = result;
                            break;
                        case DeviceProcessTypeResult.InspectionTopResult:
                            deviceReport.InspectionTopResult = result;
                            break;
                        case DeviceProcessTypeResult.Inspection3DResult:
                            deviceReport.Inspection3DResult = result;
                            break;
                    }
                }
            }

            mLog.Info($"Device updated: {id}, lot id: {lotId}, state: {deviceProcessTypeResult}, validation: {result}");

            DeviceUpdated?.Invoke(this, id);
        }

        private DeviceReport GetDeviceReport(int id, int lotId)
        {
            DeviceReport deviceReport;
            lock (mLotListLockObject)
            {
                var lotReport = mLotReports[lotId];

                if (!lotReport.Devices.Any(x => x.Id == id))
                {
                    deviceReport = new DeviceReport
                    {
                        Id = id,
                        CreatedTime = DateTime.Now,
                    };
                    lotReport.Devices.Add(deviceReport);
                }
                else
                {
                    deviceReport = lotReport.Devices.First(x => x.Id == id);
                }
            }
            return deviceReport;
        }

        public void OpenLot(int id, string customerName, bool virtualLot = false)
        {
            lock (mLotListLockObject)
            {
                if (!mLotReports.Any(x => x.Key == id))
                {
                    NewLot(id, customerName, virtualLot);
                }
            }

            UpdateLot(id, LotProcessState.Opened);
        }

        public void CloseLot(int id)
        {
            UpdateLot(id, LotProcessState.Closed);
        }
    }
}
