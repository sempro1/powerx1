﻿using MachineManager;
using System.Collections.Generic;
using System;
using System.Linq;
using EventHandler.Lib;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace EventManager.Application
{
   public class EventManager: SubscriptionBase, IEventManager
    {
        public static EventManager mEventManager;
        public event EventHandler<Event> EventAdded;
        public event EventHandler<Event> EventRemoved;

        private readonly List<Event> mActiveEvents = new();
        private readonly List<Event> mEvents = new();


        //private PlcService mPlcService = PlcService.Instance;
        //private IEventHandler mEventHandler = EventHandler.Instance;

        public static EventManager Instance => mEventManager ??= new();

        public IEnumerable<Event> ActiveEvents
        {
            get => mActiveEvents;
        }

        /// <summary>
        /// Method to register a list of events for PC
        /// </summary>
        /// <param name="Events">List of all possible events</param>
        public void RegisterEvents(IEnumerable<Event> events)
        {
            mEvents.AddRange(events.ToList());
        }

        /// <summary>
        /// Remove events from list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="source"></param>
        private void RemoveEventFromActiveEvents(int id)
        {
            if (mActiveEvents.Any(x => x.ID == id) &&
                                    mEvents.Any(x => x.ID == id))
            {
                var removedEvent = mEvents.First(x => x.ID == id);
                mActiveEvents.Remove(removedEvent);
                FireEventRemoved(removedEvent);
            }
        }

        private void AddEventToActiveEvents(int id)
        {
            if (!IsEventSet(id))
            {
                if (mEvents.Any(x => x.ID == id))
                {
                    var addedEvent = mEvents.First(x => x.ID == id);
                    addedEvent.Created = DateTime.Now;
                    mActiveEvents.Add(addedEvent);
                    FireEventAdded(addedEvent);
                }
                else
                {
                    //Event is set, but ID is unknown?
                }
            }
        }

        public void SetEvent(int eventId)
        {
            AddEventToActiveEvents(eventId);
        }

        public void ResetEvent(int eventId)
        {
            RemoveEventFromActiveEvents(eventId);
        }

        private void FireEventRemoved(Event removedEvent)
        {
            EventRemoved?.Invoke(this, removedEvent);
        }

        private void FireEventAdded(Event addedEvent)
        {
            EventAdded?.Invoke(this, addedEvent);
}
        public bool IsEventSet(int id)
        {
            return mActiveEvents.Any(x => x.ID == id);
        }
    }
}
