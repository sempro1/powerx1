﻿using EventHandler.Lib;
using MachineManager;
using MachineManager.Lib;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EventHandler.Application
{
    public class EventHandlerSubScriptionBase: SubscriptionBase
    {
        private readonly IEventManager mEventManager = EventManager.Application.EventManager.Instance;

        /// <summary>
        /// Add all events to this dictionary and they will be registered at OPC UA
        /// </summary>
        protected Dictionary<string, Event> mEventDictionary = new();

        /// <summary>
        /// Needs to be called to register the tagmodels in the mEventDictionary
        /// </summary>
        public void RegisterTagModelsInDictionary()
        {
            TagModelPathDictionary = new();
            foreach (var eventItem in mEventDictionary)
            {
                TagModelPathDictionary.Add(new EventTagModel(eventItem.Value.ObjectPath), eventItem.Key);
            }
        }

        /// <summary>
        /// Helper method so every property can call this method and set its event
        /// </summary>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        protected void SetEventPropertyValue(bool value, [CallerMemberName] string propertyName = "")
        {
            if (propertyName == string.Empty)
                return;

            if (value)
            {
                mEventDictionary[propertyName].SetEvent(mEventManager);
            }
            else
            {
                mEventDictionary[propertyName].ResetEvent(mEventManager);
            }
        }
    }
}
