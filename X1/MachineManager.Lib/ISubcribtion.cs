﻿namespace MachineManager.Lib
{
    public interface ISubscription
    {
        void OnSubscriptionValueChanged(ValueChangedModel e);
    }
}
