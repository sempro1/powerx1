﻿namespace MachineManager
{
    public class ValueChangedModel
    {
        public string Name { get; set; }
        public object Value { get; set; }

        public ValueChangedModel(string name, object value)
        {
            Name = name;
            Value = value;
        }
    }
}
