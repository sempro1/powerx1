﻿namespace MachineManager.Lib
{
    public class TagModel: ITagModel
    {
       
        public int ModuleId { get; }
        public string ObjectPath { get; }

        public TagModel(int moduleId, string objectPath)
        {
            ModuleId = moduleId;
            ObjectPath = objectPath;
        }

        /// <summary>
        /// Returns path which is used by OPC UA
        /// </summary>
        public string GetObjectPathOPCUa()
        {
            string output = string.Empty;

            if (ObjectPath != null)
            {
                output = ObjectPath.Replace("/", $"/{ModuleId}:");
                output = "/3:ServerInterfaces" + $"/{ModuleId}:" + output;
            }
            return output;
        }
    }
}
