﻿using System;

namespace MachineManager.Lib
{
    [AttributeUsage(AttributeTargets.Class|
                       AttributeTargets.Property | AttributeTargets.Interface,
                       AllowMultiple = false)]
    public class NameAttribute : Attribute
    {
        public string Name { get; private set; }

        public NameAttribute(string name)
        {
            Name = name;
        }

        public NameAttribute()
        {
        }
    }
}
