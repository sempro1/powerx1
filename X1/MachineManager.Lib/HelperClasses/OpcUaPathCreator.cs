﻿using MachineManager.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MachineManager.OpcUa
{
    public class OpcUaPathCreator<T>
    {
        public static string GetPath<TProperty>(Expression<Func<T, TProperty>> expression)
        {
            var body = expression.Body as MemberExpression;

            if (body == null)
            {
                body = ((UnaryExpression)expression.Body).Operand as MemberExpression;
            }

            return string.Join("/", GetPropertyNames(body).Reverse());
        }

        private static IEnumerable<string> GetPropertyNames(MemberExpression body)
        {
            while (body != null)
            {
                var name = GetNameAttributeValue(body);

                yield return name;
                var inner = body.Expression;
                switch (inner.NodeType)
                {
                    case ExpressionType.MemberAccess:
                        body = inner as MemberExpression;
                        break;
                    default:
                        body = null;
                        break;

                }
            }
        }

        private static string GetNameAttributeValue(MemberExpression body)
        {
            var name = body.Member.Name;
            //Check if it has a name attribute
            var attributes = Attribute.GetCustomAttributes(body.Member);
            if (attributes.Any(x => x.GetType() == typeof(NameAttribute)))
            {
                name = ((NameAttribute)attributes.First(x => x.GetType() == typeof(NameAttribute))).Name;
            }

            return name;
        }
    }
}
