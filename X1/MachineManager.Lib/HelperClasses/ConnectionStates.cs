﻿namespace MachineManager.Lib
{
    public enum ConnectionState
    {
        Offline,
        Connecting,
        Online
    }
}
