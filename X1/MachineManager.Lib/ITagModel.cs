﻿namespace MachineManager.Lib
{
    public interface ITagModel
    {
        public string ObjectPath { get; }

        public string GetObjectPathOPCUa();
    }
}