﻿using System.Reflection;

namespace MachineManager.Lib
{
    public class EventTagModel : ITagModel
    {
        /// <summary>
        /// For events this should be: "instLoadMagazineEvents.instMagazineOrientationNotOkWarning"
        /// </summary>
        public string ObjectPath { get; set; }

        public string GetObjectPathOPCUa()
        {
            // "ns=3;\"instLoadMagazineEvents\".\"instMagazineOrientationNotOkWarning\".\"IsSet\""
            var objectStrings = ObjectPath.Split('.');
            var instance = objectStrings[0];
            var eventName = objectStrings[1];

            //Instance objects are always in namespace 3
            return $"ns=3;\"{instance}\".\"{eventName}\".\"IsSet\"";
        }

        public EventTagModel(string objectPath)
        {
            ObjectPath = objectPath;
        }
    }
}
