﻿namespace MachineManager.Lib
{
    public class NamedTagModel: ITagModel
    {
        public string Name { get; }

        public string ObjectPath => Name;

        public NamedTagModel(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Returns path which is used by OPC UA
        /// </summary>
        public string GetObjectPathOPCUa()
        {

            return $"ns=3;s={Name}";
        }
    }
}
