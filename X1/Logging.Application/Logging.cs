﻿using Logging.PLCInterfaces.OpcUa;
using MachineManager;
using MachineManager.Lib;

namespace Logging.Application
{
    public class Logging
    {
        private PlcService mPLCService = PlcService.Instance;
        private static Logging mLogging;

        private TagModel mLoggingTagModel = new TagModel(ServerInterface.ModuleId, "Logging/LogDB");
        public static Logging Instance
        {
            get
            {
                return mLogging ??= new Logging();
            }
        }

        public Logging()
        {
            // Start communication with the PLC
            //var value = mPLCService.ReadValue(mLoggingTagModel);

        }
    }
}
